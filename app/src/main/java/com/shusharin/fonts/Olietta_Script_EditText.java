package com.shusharin.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.example.czh.R;

public class Olietta_Script_EditText extends AppCompatEditText {

    public Olietta_Script_EditText(Context context) {
        super(context, null);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Olietta script.ttf"));
    }

    public Olietta_Script_EditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Olietta script.ttf"));
        this.setTextSize(25);
        this.setHintTextColor(getResources().getColor(R.color.text_color_edit_text));
    }

    public Olietta_Script_EditText(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Olietta script.ttf"));
    }
}
