package com.shusharin.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class Olietta_Script_TextView extends AppCompatTextView {

    public Olietta_Script_TextView(Context context) {
        super(context, null);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Olietta script.ttf"));
    }

    @SuppressLint("RestrictedApi")
    public Olietta_Script_TextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Olietta script.ttf"));
        this.setAutoSizeTextTypeUniformWithConfiguration(1,30,1,1);
    }


    public Olietta_Script_TextView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Olietta script.ttf"));
    }
}
