package com.shusharin.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class PragmaticaShadowC_TextView extends AppCompatTextView {

    public PragmaticaShadowC_TextView(Context context) {
        super(context, null);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/PragmaticaShadowC-Bold.otf"));
    }

    public PragmaticaShadowC_TextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/PragmaticaShadowC-Bold.otf"));
    }

    public PragmaticaShadowC_TextView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/PragmaticaShadowC-Bold.otf"));
    }
}
