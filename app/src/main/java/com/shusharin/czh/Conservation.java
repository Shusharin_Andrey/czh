package com.shusharin.czh;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.czh.R;

import static com.shusharin.czh.MainActivity.myResources;

public class Conservation implements Parcelable {

    public static final String TAG = "CONSERVATION";
    public static final String NUMBER_CONSERVATION = "NUMBER_CONSERVATION";
    public static final String SIZE = "SIZE";
    public static final String NUMBER_NAME = "NUMBER_NAME";
    public static final String ONE_PLAYER = "ONE_PLAYER";
    public static final String TWO_PLAYER = "TWO_PLAYER";
    public static final String THREE_PLAYER = "THREE_PLAYER";
    public static final String NUMBER_PLAYER = "NUMBER_PLAYER";
    public static final String NUMBER_FIELD = "NUMBER_FIELD";
    public static final String IS_FINISHED = "IS_FINISHED";
    public static final String IS_FIRST = "IS_FIRST";
    public static final String IS_LOOSE = "IS_LOOSE";
    private String name;
    private String nameOnePlayer = "";
    private String nameTwoPlayer = "";
    private String nameThreePlayer = "";
    private int numberPlayer;
    private int numberField;
    private int numberName;
    private boolean isFinished;
    private boolean isLoose = true;
    private boolean isFirst = true;

    private Conservation(Parcel in) {
        name = in.readString();
        nameOnePlayer = in.readString();
        nameTwoPlayer = in.readString();
        nameThreePlayer = in.readString();
        numberPlayer = in.readInt();
        numberField = in.readInt();
        numberName = in.readInt();
        isFinished = in.readByte() != 0;
        isLoose = in.readByte() != 0;
        isFirst = in.readByte() != 0;
    }

    public static final Creator<Conservation> CREATOR = new Creator<Conservation>() {
        @Override
        public Conservation createFromParcel(Parcel in) {
            return new Conservation(in);
        }

        @Override
        public Conservation[] newArray(int size) {
            return new Conservation[size];
        }
    };

    public String getNameOnePlayer() {
        return nameOnePlayer;
    }

    public void setNameOnePlayer(String nameOnePlayer) {
        this.nameOnePlayer = nameOnePlayer;
    }

    public String getNameTwoPlayer() {
        return nameTwoPlayer;
    }

    public void setNameTwoPlayer(String nameTwoPlayer) {
        this.nameTwoPlayer = nameTwoPlayer;
    }

    public String getNameThreePlayer() {
        return nameThreePlayer;
    }

    public void setNameThreePlayer(String nameThreePlayer) {
        this.nameThreePlayer = nameThreePlayer;
    }

    public boolean isLoose() {
        return isLoose;
    }

    public void setLoose(boolean loose) {
        isLoose = loose;
    }

    public static String getTAG() {
        return TAG;
    }

    public static String getNumberConservation() {
        return NUMBER_CONSERVATION;
    }

    public static String getSIZE() {
        return SIZE;
    }

    public static String getOnePlayer() {
        return ONE_PLAYER;
    }

    public static String getTwoPlayer() {
        return TWO_PLAYER;
    }

    public static String getThreePlayer() {
        return THREE_PLAYER;
    }

    public static String getIsFinished() {
        return IS_FINISHED;
    }

    public static String getIsFirst() {
        return IS_FIRST;
    }

    public static String getIsLoose() {
        return IS_LOOSE;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Creator<Conservation> getCREATOR() {
        return CREATOR;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public Conservation(int numberPlayer, int numberField, String name, int numberName) {
        this.name = name;
        this.numberField = numberField;
        this.numberPlayer = numberPlayer;
        this.numberName = numberName;
    }

    public Conservation(int numberPlayer, int numberField, String name) {
        this.name = name;
        this.numberField = numberField;
        this.numberPlayer = numberPlayer;
    }

    public Conservation() {
    }

    public String getName() {
        if (numberName == 0) {
            return this.name;
        } else {
            return this.name + numberName;
        }
    }

    public String getNameNoNumber() {
        return this.name;
    }

    public void setNameNoNumber(String name) {
        this.name = name;
    }

    public String getNumberPlayerString() {
        if (numberPlayer == 1) {
            return myResources.getString(R.string.onePlayer);
        } else if (numberPlayer == 2) {
            return myResources.getString(R.string.Two_player);
        } else {
            return myResources.getString(R.string.Three_player);
        }
    }

    public String getNumberFieldString() {
        if (numberField == 1) {
            return myResources.getString(R.string.One_field);
        } else {
            return myResources.getString(R.string.Three_faild);
        }
    }

    public int getNumberPlayer() {
        return numberPlayer;
    }

    public int getNumberField() {
        return numberField;
    }

    public int getNumberName() {
        return numberName;
    }

    public void setNumberPlayer(int numberPlayer) {
        this.numberPlayer = numberPlayer;
    }

    public void setNumberField(int numberField) {
        this.numberField = numberField;
    }

    public void setNumberName(int numberName) {
        this.numberName = numberName;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(nameOnePlayer);
        dest.writeString(nameTwoPlayer);
        dest.writeString(nameThreePlayer);
        dest.writeInt(numberPlayer);
        dest.writeInt(numberField);
        dest.writeInt(numberName);
        dest.writeByte((byte) (isFinished ? 1 : 0));
        dest.writeByte((byte) (isLoose ? 1 : 0));
        dest.writeByte((byte) (isFirst ? 1 : 0));
    }
}