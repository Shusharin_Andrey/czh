package com.shusharin.czh.Field;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.czh.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shusharin.czh.MainActivity;

import static com.shusharin.czh.Field.Field.whoOpen;
import static com.shusharin.czh.MainActivity.myResources;
import static com.shusharin.czh.MainActivity.saveName;

class TableOnline extends Table {
    private static final String TAG = "TableOnline";
    private DatabaseReference DATABASE_SMALL_SQUARE_ALL;
    private DatabaseReference DATABASE_BIG_SQUARE_ALL;
    private DatabaseReference DATABASE_IS_FIRST;
    private DatabaseReference DATABASE_IS_FINISHED;
    private DatabaseReference DATABASE_IS_CANCEL;
    private DatabaseReference DATABASE_CANCEL_I;
    private DatabaseReference DATABASE_CANCEL_J;
    private DatabaseReference DATABASE_HOW_SHOW;
    private DatabaseReference DATABASE_WHERE_TO_POKE;
    private DatabaseReference DATABASE_CANCEL_WHERE_TO_POKE;
    private DatabaseReference DATABASE_WHO_WIN_INT;
    private DatabaseReference DATABASE_WHO_WIN;
    private DatabaseReference DATABASE_IS_MAKE_HIS_MOVE;
    private DatabaseReference DATABASE_PREVIOUS;
    private DatabaseReference DATABASE_CANCEL_BIG_SQUARE;
    private DatabaseReference DATABASE_CANCEL_PREVIOUS;

    DatabaseReference readReferenceTable;
    DatabaseReference readReferenceName;

    private ValueEventListener[][] LISTENER_SMALL_SQUARE = new ValueEventListener[9][9];
    private ValueEventListener[] LISTENER_BIG_SQUARE = new ValueEventListener[9];
    private ValueEventListener LISTENER_IS_FIRST;
    private ValueEventListener LISTENER_IS_FINISHED;
    private ValueEventListener LISTENER_IS_CANCEL;
    private ValueEventListener LISTENER_CANCEL_I;
    private ValueEventListener LISTENER_CANCEL_J;
    private ValueEventListener LISTENER_HOW_SHOW;
    private ValueEventListener LISTENER_WHERE_TO_POKE;
    private ValueEventListener LISTENER_CANCEL_WHERE_TO_POKE;
    private ValueEventListener LISTENER_WHO_WIN_INT;
    private ValueEventListener LISTENER_WHO_WIN;
    private ValueEventListener LISTENER_IS_MAKE_HIS_MOVE;
    private ValueEventListener LISTENER_PREVIOUS;
    private ValueEventListener LISTENER_CANCEL_BIG_SQUARE;
    private ValueEventListener LISTENER_CANCEL_PREVIOUS;

    private ValueEventListener LISTENER_FIRST_PLAYER;
    private ValueEventListener LISTENER_SECOND_PLAYER;
    private ValueEventListener LISTENER_THIRD_PLAYER;

    private int numberTable;
    private boolean isStatistic = false;

    private Activity context;

    TableOnline(String table, int tableint, Activity activity,Boolean isStatistic){
        this.isStatistic = isStatistic;
        create(table,tableint,activity);
    }

    TableOnline(String table, int tableint, Activity activity) {
        create(table,tableint,activity);
    }

    private void create(String table, int tableint, Activity activity){
        context = activity;
        numberTable = tableint;
        DatabaseReference database = FirebaseDatabase.getInstance().getReference(myResources.getString(R.string.Games));
        readReferenceTable = database.child(saveName).child(myResources.getString(R.string.Field)).child(table);
        DATABASE_SMALL_SQUARE_ALL = readReferenceTable.child(APP_SAVE_SMALL_SQUARE);
        DATABASE_BIG_SQUARE_ALL = readReferenceTable.child(APP_SAVE_BIG_SQUARE_INT);
        for (int i = 0; i < 9; i++) {
            LISTENER_BIG_SQUARE[i] = update(i);
            for (int j = 0; j < 9; j++) {
                LISTENER_SMALL_SQUARE[i][j] = update(i, j);
            }
        }

        readReferenceName = FirebaseDatabase.getInstance().getReference(myResources.getString(R.string.name_game)).child(saveName);
        LISTENER_FIRST_PLAYER = update(readReferenceName.child(myResources.getString(R.string.name_one_player)), 1, 2);
        LISTENER_SECOND_PLAYER = update(readReferenceName.child(myResources.getString(R.string.name_two_player)), 2, 2);
        LISTENER_THIRD_PLAYER = update(readReferenceName.child(myResources.getString(R.string.name_three_player)), 3, 2);

        DATABASE_IS_FIRST = readReferenceTable.child(APP_SAVE_IS_FIRST);
        LISTENER_IS_FIRST = update(DATABASE_IS_FIRST, 1, 1);

        DATABASE_IS_FINISHED = readReferenceTable.child(APP_SAVE_IS_FINISHED);
        LISTENER_IS_FINISHED = update(DATABASE_IS_FINISHED, 3, 1);

        DATABASE_IS_CANCEL = readReferenceTable.child(APP_SAVE_IS_CANCEL);
        LISTENER_IS_CANCEL = update(DATABASE_IS_CANCEL, 0, 1);

        DATABASE_CANCEL_I = readReferenceTable.child(APP_SAVE_CANCEL_I);
        LISTENER_CANCEL_I = update(DATABASE_CANCEL_I, 0, 0);

        DATABASE_CANCEL_J = readReferenceTable.child(APP_SAVE_CANCEL_J);
        LISTENER_CANCEL_J = update(DATABASE_CANCEL_J, 1, 0);

        DATABASE_HOW_SHOW = readReferenceTable.child(APP_SAVE_HOW_SHOW);
        LISTENER_HOW_SHOW = update(DATABASE_HOW_SHOW, 2, 0);

        DATABASE_WHERE_TO_POKE = readReferenceTable.child(APP_SAVE_WHERE_TO_POKE);
        LISTENER_WHERE_TO_POKE = update(DATABASE_WHERE_TO_POKE, 3, 0);

        DATABASE_CANCEL_WHERE_TO_POKE = readReferenceTable.child(APP_SAVE_CANCEL_WHERE_TO_POKE);
        LISTENER_CANCEL_WHERE_TO_POKE = update(DATABASE_CANCEL_WHERE_TO_POKE, 5, 0);

        DATABASE_WHO_WIN_INT = readReferenceTable.child(APP_SAVE_WHO_WIN_INT);
        LISTENER_WHO_WIN_INT = update(DATABASE_WHO_WIN_INT, 7, 0);

        DATABASE_WHO_WIN = readReferenceTable.child(APP_SAVE_WHO_WIN);
        LISTENER_WHO_WIN = update(DATABASE_WHO_WIN, 0, 2);

        DATABASE_IS_MAKE_HIS_MOVE = readReferenceTable.child(APP_SAVE_IS_MAKE_HIS_MOVE);
        LISTENER_IS_MAKE_HIS_MOVE = update(DATABASE_IS_MAKE_HIS_MOVE, 4, 1);

        DATABASE_PREVIOUS = readReferenceTable.child(APP_SAVE_PREVIOUS);
        LISTENER_PREVIOUS = update(DATABASE_PREVIOUS, 8, 0);

        DATABASE_CANCEL_BIG_SQUARE = readReferenceTable.child(APP_SAVE_CANCEL_BIG_SQUARE);
        LISTENER_CANCEL_BIG_SQUARE = update(DATABASE_CANCEL_BIG_SQUARE, 9, 0);

        DATABASE_CANCEL_PREVIOUS = readReferenceTable.child(APP_SAVE_CANCEL_PREVIOUS);
        LISTENER_CANCEL_PREVIOUS = update(DATABASE_CANCEL_PREVIOUS, 10, 0);
    }

    private ValueEventListener update(int i) {
        DatabaseReference BIG_SQUARE = DATABASE_BIG_SQUARE_ALL.child(String.valueOf(i));
        return BIG_SQUARE.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (dataSnapshot.getValue(Integer.class) != null) {
                    bigSquareInt[i] = dataSnapshot.getValue(Integer.class);
                    if (whoOpen == numberTable) {
                        Field.bigSquare[i].setBackgroundResource(ThreePlayer.getBackgroundResourceThick(getBigSquareInt(i)));
                        if (bigSquareInt[i] != 0 || Field.conservation.isFinished()) {
                            Field.bigSquare[i].setForeground(null);
                            if (whereToPoke == i && !isStatistic) {
                                context.findViewById(R.id.frame).setVisibility(View.VISIBLE);
                            }
                        }
                        if (!isShow()) {
                            for (int n = 0; n < 3; n++) {
                                Field.bigLine[i][n].setVisibility(View.GONE);
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, myResources.getString(R.string.fail_read), error.toException());
            }
        });
    }

    private ValueEventListener update(int i, int j) {
        DatabaseReference SMALL_SQUARE = DATABASE_SMALL_SQUARE_ALL.child(String.valueOf(i)).child(String.valueOf(j));
        return SMALL_SQUARE.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (dataSnapshot.getValue(Integer.class) != null) {
                    smallSquare[i][j] = dataSnapshot.getValue(Integer.class);
                    if (whoOpen == numberTable) {
                        Field.smallSquareView[i][j].setBackgroundResource(ThreePlayer.getBackgroundResourceThin(getSmallSquare(i, j)));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, myResources.getString(R.string.fail_read), error.toException());
            }
        });
    }

    private ValueEventListener update(DatabaseReference databaseReference, int numberMethod, int index) {
        return databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                choiceOfMethod(dataSnapshot, numberMethod, index);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, myResources.getString(R.string.fail_read), error.toException());
            }
        });
    }

    private void method(int numberMethod, boolean value) {
        switch (numberMethod) {
            case 0:
                isCancel = value;
                if (whoOpen == numberTable) {
                    if (isCancel()) {
                        Field.cancel.setBackgroundResource(R.drawable.button);
                    } else {
                        Field.cancel.setBackgroundResource(R.drawable.button_pressed);
                    }
                }
                break;
            case 1:
                isFirst = (value);
                if(!isStatistic) {
                    TextView motionText = context.findViewById(R.id.motionText);
                    View motion = context.findViewById(R.id.motion);
                    Field.showMotion(motion, motionText, this);
                }
                break;
            case 3:
                isFinished = (value);
                break;
            case 4:
                isMakeHisMove = (value);
                break;
        }
    }

    private void method(int numberMethod, int value) {
        switch (numberMethod) {
            case 0:
                cancelI = (value);
                break;
            case 1:
                cancelJ = (value);
                break;
            case 2:
                howShow = (value);
                if (whoOpen == numberTable) {
                    if (getHowShow() != 0) {
                        Field.show.setBackgroundResource(R.drawable.button);
                    } else {
                        Field.show.setBackgroundResource(R.drawable.button_pressed);
                    }
                }
                break;
            case 3:
                if (whoOpen == numberTable) {
                    if (Field.bigSquare[getWhereToPoke()] != null) {
                        if (getBigSquareInt(getWhereToPoke()) == ID_EMPTY) {
                            Field.bigSquare[getWhereToPoke()].setForeground(null);
                        } else if(!isStatistic){
                            context.findViewById(R.id.frame).setVisibility(View.GONE);
                        }
                    }
                }
                whereToPoke = (value);
                if (whoOpen == numberTable) {
                    if (Field.bigSquare[getWhereToPoke()] != null) {
                        if (!isMakeHisMove) {
                            if (getBigSquareInt(getWhereToPoke()) == ID_EMPTY) {
                                Field.bigSquare[getWhereToPoke()].setForeground(myResources.getDrawable(R.drawable.gold_frame));
                            } else if(!isStatistic){
                                context.findViewById(R.id.frame).setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
                break;
            case 5:
                cancelWhereToPoke = (value);
                break;
            case 7:
                whoWinInt = (value);
                if (whoOpen == 0) {
                    DatabaseReference databaseTable = FirebaseDatabase.getInstance().getReference(myResources.getString(R.string.Games)).child(saveName).child(myResources.getString(R.string.Field));

                    DatabaseReference WIN_ONE = databaseTable.child(Field.WIN_ONE);
                    DatabaseReference WIN_TWO = databaseTable.child(Field.WIN_TWO);

                    if (value == ID_BLUE) {
                        WIN_ONE.setValue(10);
                        WIN_TWO.setValue(0);
                    } else if (value == ID_RED) {
                        WIN_ONE.setValue(0);
                        WIN_TWO.setValue(10);
                    } else {
                        WIN_ONE.setValue(5);
                        WIN_TWO.setValue(5);
                    }
                }
                break;
            case 8:
                if (whoOpen == numberTable) {
                    if (getPrevious() != null) {
                        getPrevious().setForeground(null);
                    }
                }
                if(!isStatistic) {
                    previous = (context.findViewById(value));
                    if (whoOpen == numberTable) {
                        if (getPrevious() != null) {
                            getPrevious().setForeground(myResources.getDrawable(R.drawable.green_thin));
                        }
                    }
                }
                break;
            case 9:
                if(!isStatistic) {
                    cancelBigSquare = (context.findViewById(value));
                }
                break;
            case 10:
                if(!isStatistic) {
                    cancelPrevious = (context.findViewById(value));
                }
                break;
        }
    }

    private void method(int numberMethod, String value) {
        switch (numberMethod) {
            case 0:
                whoWin = (value);
                break;
            case 1:
                Field.conservation.setNameOnePlayer(value);
                break;
            case 2:
                Field.conservation.setNameTwoPlayer(value);
                break;
            case 3:
                Field.conservation.setNameThreePlayer(value);
                break;
        }
    }

    private void choiceOfMethod(DataSnapshot dataSnapshot, int numberMethod, int index) {
        switch (index) {
            case 0:
                if (dataSnapshot.getValue(Integer.class) != null) {
                    method(numberMethod, dataSnapshot.getValue(Integer.class));
                }
                break;
            case 1:
                if (dataSnapshot.getValue(boolean.class) != null) {
                    method(numberMethod, dataSnapshot.getValue(boolean.class));
                }
                break;
            case 2:
                if (dataSnapshot.getValue(String.class) != null) {
                    method(numberMethod, dataSnapshot.getValue(String.class));
                }
                break;
        }
    }

    @Override
    void setPrevious(View previous) {
        if (getPrevious() != previous) {
            super.setPrevious(previous);
            if (previous != null) {
                DATABASE_PREVIOUS.setValue(previous.getId());
            } else {
                DATABASE_PREVIOUS.setValue(null);
            }
        }
    }

    @Override
    void setCancelBigSquare(View cancelBigSquare) {
        if (getCancelBigSquare() != cancelBigSquare) {
            super.setCancelBigSquare(cancelBigSquare);
            DATABASE_CANCEL_BIG_SQUARE.setValue(cancelBigSquare.getId());
        }
    }

    @Override
    void setCancelPrevious(View cancelPrevious) {
        if (getCancelPrevious() != cancelPrevious) {
            super.setCancelPrevious(cancelPrevious);
            DATABASE_CANCEL_PREVIOUS.setValue(cancelPrevious.getId());
        }
    }

    @Override
    void setWhoWin(String whoWin) {
        if (!getWhoWin().equals(whoWin)) {
            super.setWhoWin(whoWin);
            DATABASE_WHO_WIN.setValue(whoWin);
        }
    }

    @Override
    void setMakeHisMove(boolean makeHisMove) {
        if (isMakeHisMove() != makeHisMove) {
            super.setMakeHisMove(makeHisMove);
            DATABASE_IS_MAKE_HIS_MOVE.setValue(makeHisMove);
        }
    }

    @Override
    void setWhoWinInt(int whoWinInt) {
        if (getWhoWinInt() != whoWinInt) {
            super.setWhoWinInt(whoWinInt);
            DATABASE_WHO_WIN_INT.setValue(whoWinInt);
        }
    }

    @Override
    void setFirst(boolean First) {
        if (isFirst() != First) {
            super.setFirst(First);
            DATABASE_IS_FIRST.setValue(First);
        }
    }

    @Override
    void setFinished(Boolean Finished) {
        if (isFinished() != Finished) {
            super.setFinished(Finished);
            DATABASE_IS_FINISHED.setValue(Finished);
        }
    }

    @Override
    void setCancel(boolean cancel) {
        if (isCancel() != cancel) {
            super.setCancel(cancel);
            DATABASE_IS_CANCEL.setValue(cancel);
        }
    }

    void setSmallSquare(int i, int j, int k) {
        if (getSmallSquare(i, j) != k) {
            super.setSmallSquare(i, j, k);
            DATABASE_SMALL_SQUARE_ALL.child(String.valueOf(i)).child(String.valueOf(j)).setValue(k);
            MainActivity.startPlay(Field.smallSquareSong);
        }
    }

    @Override
    void setBigSquareInt(int i, int k) {
        if (getBigSquareInt(i) != k) {
            super.setBigSquareInt(i, k);
            DATABASE_BIG_SQUARE_ALL.child(String.valueOf(i)).setValue(k);
            MainActivity.startPlay(Field.bigSquareSong);
        }
    }

    @Override
    void setCancelI(int cancelI) {
        if (getCancelI() != cancelI) {
            super.setCancelI(cancelI);
            DATABASE_CANCEL_I.setValue(cancelI);
        }
    }

    @Override
    void setCancelJ(int cancelJ) {
        if (getCancelJ() != cancelJ) {
            super.setCancelJ(cancelJ);
            DATABASE_CANCEL_J.setValue(cancelJ);
        }
    }

    @Override
    void setWhereToPoke(int whereToPoke) {
        if (getWhereToPoke() != whereToPoke) {
            super.setWhereToPoke(whereToPoke);
            DATABASE_WHERE_TO_POKE.setValue(whereToPoke);
        }
    }

    @Override
    void setCancelWhereToPoke(int cancelWhereToPoke) {
        if (getCancelWhereToPoke() != cancelWhereToPoke) {
            super.setCancelWhereToPoke(cancelWhereToPoke);
            DATABASE_CANCEL_WHERE_TO_POKE.setValue(cancelWhereToPoke);
        }
    }

    @Override
    void setHowShow(int anInt) {
        if (getHowShow() != anInt) {
            super.setHowShow(anInt);
            DATABASE_HOW_SHOW.setValue(anInt);
        }
    }

    void stop() {
        if (readReferenceTable != null) {
            for (int i = 0; i < 9; i++) {
                if (LISTENER_BIG_SQUARE[i] != null) {
                    DATABASE_BIG_SQUARE_ALL.removeEventListener(LISTENER_BIG_SQUARE[i]);
                }
                for (int j = 0; j < 9; j++) {
                    if (LISTENER_SMALL_SQUARE[i][j] != null) {
                        DATABASE_SMALL_SQUARE_ALL.removeEventListener(LISTENER_SMALL_SQUARE[i][j]);
                    }
                }
            }
            if (LISTENER_IS_FIRST != null) {
                DATABASE_IS_FIRST.removeEventListener(LISTENER_IS_FIRST);
            }
            if (LISTENER_IS_FINISHED != null) {
                DATABASE_IS_FINISHED.removeEventListener(LISTENER_IS_FINISHED);
            }
            if (LISTENER_IS_CANCEL != null) {
                DATABASE_IS_CANCEL.removeEventListener(LISTENER_IS_CANCEL);
            }
            if (LISTENER_CANCEL_I != null) {
                DATABASE_CANCEL_I.removeEventListener(LISTENER_CANCEL_I);
            }
            if (LISTENER_CANCEL_J != null) {
                DATABASE_CANCEL_J.removeEventListener(LISTENER_CANCEL_J);
            }
            if (LISTENER_HOW_SHOW != null) {
                DATABASE_HOW_SHOW.removeEventListener(LISTENER_HOW_SHOW);
            }
            if (LISTENER_WHERE_TO_POKE != null) {
                DATABASE_WHERE_TO_POKE.removeEventListener(LISTENER_WHERE_TO_POKE);
            }
            if (LISTENER_CANCEL_WHERE_TO_POKE != null) {
                DATABASE_CANCEL_WHERE_TO_POKE.removeEventListener(LISTENER_CANCEL_WHERE_TO_POKE);
            }
            if (LISTENER_WHO_WIN_INT != null) {
                DATABASE_WHO_WIN_INT.removeEventListener(LISTENER_WHO_WIN_INT);
            }
            if (LISTENER_WHO_WIN != null) {
                DATABASE_WHO_WIN.removeEventListener(LISTENER_WHO_WIN);
            }
            if (LISTENER_IS_MAKE_HIS_MOVE != null) {
                DATABASE_IS_MAKE_HIS_MOVE.removeEventListener(LISTENER_IS_MAKE_HIS_MOVE);
            }
            if (LISTENER_PREVIOUS != null) {
                DATABASE_PREVIOUS.removeEventListener(LISTENER_PREVIOUS);
            }
            if (LISTENER_CANCEL_BIG_SQUARE != null) {
                DATABASE_CANCEL_BIG_SQUARE.removeEventListener(LISTENER_CANCEL_BIG_SQUARE);
            }
            if (LISTENER_CANCEL_PREVIOUS != null) {
                DATABASE_CANCEL_PREVIOUS.removeEventListener(LISTENER_CANCEL_PREVIOUS);
            }
        }

        if (readReferenceName != null) {
            if (LISTENER_FIRST_PLAYER != null) {
                readReferenceName.removeEventListener(LISTENER_FIRST_PLAYER);
            }
            if (LISTENER_SECOND_PLAYER != null) {
                readReferenceName.removeEventListener(LISTENER_SECOND_PLAYER);
            }
            if (LISTENER_THIRD_PLAYER != null) {
                readReferenceName.removeEventListener(LISTENER_THIRD_PLAYER);
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        stop();
    }
}
