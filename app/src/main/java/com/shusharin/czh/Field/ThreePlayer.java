package com.shusharin.czh.Field;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.czh.R;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.MainActivity;
import com.shusharin.czh.SelectionGames.Continue;

import static com.shusharin.czh.MainActivity.buttonCloseSong;
import static com.shusharin.czh.MainActivity.buttonSong;
import static com.shusharin.czh.MainActivity.myResources;
import static com.shusharin.czh.MainActivity.saveName;

public class ThreePlayer extends FieldThree {
    protected static final String APP_SAVE_WIN_THREE = "WIN_THREE";
    public static final String APP_SAVE_ID_THREE_PLAYER = "ID_THREE_PLAYER";
    protected int winThree;
    protected static int idNow = 1;
    protected static boolean isThreePlayer;

    public void setWinThree(int winThree) {
        this.winThree = winThree;
    }

    public void setIdNow(int idNow) {
        ThreePlayer.idNow = idNow;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isThreePlayer = true;

        cancel.setVisibility(View.GONE);

        MainActivity.isNeedHideNavigationBar = true;
        MainActivity.hideNavigationBar(getWindow());
        Three.setVisibility(View.GONE);
        isPressed = false;
        if (savedInstanceState != null) {
            winThree = savedInstanceState.getInt(APP_SAVE_WIN_THREE);
            idNow = savedInstanceState.getInt(APP_SAVE_ID_THREE_PLAYER);
        }
        if (idNow == 1) {
            One.setBackgroundResource(R.drawable.button_fist_with_third);
            Two.setBackgroundResource(R.drawable.button_fist_with_second);
        }
    }

    void nextMove() {
    }

    @Override
    protected void saveData() {
        super.saveData();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(APP_SAVE_WIN_THREE, winThree);
        editor.putInt(APP_SAVE_ID_THREE_PLAYER, idNow);
        editor.apply();
    }

    @Override
    void getData() {
        if (preferences.contains(saveName)) {
            super.getData();
            saveName = preferences.getString(saveName, "");
            winThree = preferences.getInt(APP_SAVE_WIN_THREE, winThree);
            idNow = preferences.getInt(APP_SAVE_ID_THREE_PLAYER, idNow);
            activitySave();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(APP_SAVE_WIN_THREE, winThree);
        outState.putInt(APP_SAVE_ID_THREE_PLAYER, idNow);
    }

    @Override
    protected void CheckIsFinishedAll() {
        table1.setCancel(false);
        table2.setCancel(false);
        table3.setCancel(false);
        table.setWhoWin("");
        setWhoWin(View.GONE);
        giveFrame();
        if (table.getWhoWinInt() == 1) {
            setWinOne(winOne + 10);
        } else if (table.getWhoWinInt() == 2) {
            setWinTwo(winTwo + 10);
        } else if (table.getWhoWinInt() == 5) {
            setWinThree(winThree + 10);
        } else {
            if (whoOpen == 1) {
                setWinOne(winOne + 5);
                setWinThree(winThree + 5);
            } else if (whoOpen == 2) {
                setWinOne(winOne + 5);
                setWinTwo(winTwo + 5);
            } else {
                setWinTwo(winTwo + 5);
                setWinThree(winThree + 5);
            }
        }
        if (winOne + winTwo +winThree == 30) {
            WinText();
        }
    }

    @Override
    protected void WinText() {
        if (!isWin) {
            isWin = true;
            Continue.conservations.get(index).setFinished(true);
            table1.setFinished();
            table2.setFinished();
            table3.setFinished();
            setWhoWin(View.VISIBLE);
            Intent intent = new Intent(this, Statistics.class);
            intent.putExtra(Conservation.NUMBER_FIELD, 3);
            intent.putExtra(Conservation.NUMBER_PLAYER, 3);
            intent.putExtra(WIN_ONE, winOne);
            intent.putExtra(WIN_TWO, winTwo);
            intent.putExtra(APP_SAVE_WIN_THREE, winThree);
            intent.putExtra(getString(R.string.table) + 1, table1);
            intent.putExtra(getString(R.string.table) + 2, table2);
            intent.putExtra(getString(R.string.table) + 3, table3);
            intent.putExtra(IS_LOCAL, true);
            intent.putExtra(Conservation.TAG, conservation);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
    }

    @Override
    protected void activitySave() {
        super.activitySave();
        bigSquare[4].setForeground(null);
        changeTable();
        if (idNow == 1) {
            One.setBackgroundResource(R.drawable.button_fist_with_third);
            Two.setBackgroundResource(R.drawable.button_fist_with_second);
            if (whoOpen != 1) {
                ChangePressed(Two);
            }
        } else if (idNow == 2) {
            One.setBackgroundResource(R.drawable.button_fist_with_second);
            Two.setBackgroundResource(R.drawable.button_second_with_third);
            if (whoOpen != 2) {
                ChangePressed(Two);
            }
        } else {
            One.setBackgroundResource(R.drawable.button_fist_with_third);
            Two.setBackgroundResource(R.drawable.button_second_with_third);
            if (whoOpen != 1) {
                ChangePressed(Two);
            }
        }
        One.setSelected(true);
        checkFrameTable();
    }

    public void onClickButton(View view, int whoNeedOpen) {
        if (((idNow == Table.ID_BLUE && whoNeedOpen != whoOpen)
                || (idNow == Table.ID_RED && ((whoNeedOpen == 1 && whoOpen == 3) || (whoNeedOpen == 2 && whoOpen == 2)))
                || (idNow == Table.ID_ORANGE && ((whoNeedOpen == 1 && whoOpen == 3) || (whoNeedOpen == 2 && whoOpen == 1))))
                && !isPressed) {
            isPressed = true;
            if (whoNeedOpen == 1) {
                if (idNow == 2) {
                    whoOpen = 2;
                } else {
                    whoOpen = 1;
                }
            } else {
                if (idNow == 1) {
                    whoOpen = 2;
                } else {
                    whoOpen = 3;
                }
            }
            MainActivity.startPlay(buttonSong);
            ChangePressed(view);
            changeTable();
            new Handler().postDelayed(() -> isPressed = false, 100);
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    protected void check(int i, int j, View view) {
        if (!table.isMakeHisMove() && !isPressed) {
            if (table.getSmallSquare(i, j) == 0 && (table.getWhereToPoke() == Table.whereDidPokeIt(i, j) || table.getBigSquareInt(table.getWhereToPoke()) != 0) && !table.isFinished()) {
                checkMethod(i, j, view);
                if (table.isFinished()) {
                    CheckIsFinishedAll();
                }
                table.setMakeHisMove(true);
                bigSquare[table.getWhereToPoke()].setForeground(null);
                frame.setVisibility(View.GONE);
                if (idNow == 1) {
                    if (whoOpen == 1) {
                        whoOpen = 2;
                    } else {
                        whoOpen = 1;
                    }
                } else if (idNow == 2) {
                    if (whoOpen == 2) {
                        whoOpen = 3;
                    } else {
                        whoOpen = 2;
                    }
                } else {
                    if (whoOpen == 1) {
                        whoOpen = 3;
                    } else {
                        whoOpen = 1;
                    }
                }
                if ((whoOpen == 1 && !table1.isFinished()) || (whoOpen == 2 && !table2.isFinished()) || (whoOpen == 3 && !table3.isFinished())) {
                    isPressed = true;
                    new Handler().postDelayed(() -> {
                        changeTable();
                        if (whoOpen == 1) {
                            ChangePressed(One);
                        } else if (whoOpen == 2) {
                            if (idNow == 1) {
                                ChangePressed(Two);
                            } else {
                                ChangePressed(One);
                            }
                        } else {
                            ChangePressed(Two);
                        }
                        new Handler().postDelayed(() -> {
                            randomMotion(table);
                            table.setMakeHisMove(true);
                            checkMethod(randomI, randomJ, smallSquareView[randomI][randomJ]);
                            if (table.isFinished()) {
                                CheckIsFinishedAll();
                            }
                            isPressed = false;
                            nextTurn();
                        }, 250);
                    }, 250);
                } else {
                    nextTurn();
                }
            }
        }
    }

    protected void nextId() {
        nextMoveThreeField();
        if (idNow == Table.ID_BLUE) {
            setIdNow(Table.ID_RED);
        } else if (idNow == Table.ID_RED) {
            setIdNow(Table.ID_ORANGE);
        } else {
            setIdNow(Table.ID_BLUE);
        }
    }

    protected void paintOver(int i, int j, View view) {
        table.setSmallSquare(i, j, idNow);
        table.getPrevious().setForeground(myResources.getDrawable(R.drawable.green_thin));
        view.setBackgroundResource(getBackgroundResourceThin(idNow));
    }

    public void nextTurn() {
        new Handler().postDelayed(this::NextTurn, 250);
    }

    public void NextTurn() {
        if (!isPressed) {
            isPressed = true;
            if (isMakeMove() && (!table1.isFinished() || !table2.isFinished() || !table3.isFinished())) {
                MainActivity.startPlay(buttonSong);
                nextId();
                int whatDo;
                if (idNow == 1) {
                    One.setBackgroundResource(R.drawable.button_fist_with_third);
                    Two.setBackgroundResource(R.drawable.button_fist_with_second);
                } else if (idNow == 2) {
                    One.setBackgroundResource(R.drawable.button_fist_with_second);
                    Two.setBackgroundResource(R.drawable.button_second_with_third);
                } else {
                    One.setBackgroundResource(R.drawable.button_fist_with_third);
                    Two.setBackgroundResource(R.drawable.button_second_with_third);
                }
                if (idNow == 2) {
                    if (!table2.isFinished()) {
                        whoOpen = 2;
                        whatDo = 1;
                    } else if (!table3.isFinished()) {
                        whoOpen = 3;
                        whatDo = 2;
                    } else {
                        whatDo = 3;
                    }
                } else {
                    if (!table1.isFinished()) {
                        whoOpen = 1;
                        whatDo = 1;
                    } else if (idNow == 1) {
                        if (!table2.isFinished()) {
                            whoOpen = 2;
                            whatDo = 2;
                        } else {
                            whatDo = 3;
                        }
                    } else {
                        if (!table3.isFinished()) {
                            whoOpen = 3;
                            whatDo = 2;
                        } else {
                            whatDo = 3;
                        }
                    }
                }
                Button.setSelected(false);
                if (whatDo == 1) {
                    One.setSelected(true);
                    Button = One;
                    giveFrame();
                    changeTable();
                    new Handler().postDelayed(() -> isPressed = false, 100);
                } else if (whatDo == 2) {
                    Two.setSelected(true);
                    Button = Two;
                    giveFrame();
                    changeTable();
                    new Handler().postDelayed(() -> isPressed = false, 100);
                } else {
                    new Handler().postDelayed(() -> {
                        isPressed = false;
                        NextTurn();
                    }, 100);
                }
            } else if (table1.isFinished() && table2.isFinished() && table3.isFinished()) {
                MainActivity.startPlay(buttonSong);
                nextId();
                if (idNow == 2) {
                    whoOpen = 2;
                } else {
                    whoOpen = 1;
                }
                One.setSelected(true);
                Button = One;
                changeTable();
                giveFrame();
            } else {
                MainActivity.startPlay(buttonCloseSong);
            }
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    protected boolean isMakeMove() {
        if (idNow == 1) {
            if (table1.isMakeHisMove() && table2.isMakeHisMove()) {
                if (!table1.isFinished()) {
                    table1.setMakeHisMove(false);
                }
                if (!table2.isFinished()) {
                    table2.setMakeHisMove(false);
                }
                return true;
            }
        } else if (idNow == 2) {
            if (table3.isMakeHisMove() && table2.isMakeHisMove()) {
                if (!table3.isFinished()) {
                    table3.setMakeHisMove(false);
                }
                if (!table2.isFinished()) {
                    table2.setMakeHisMove(false);
                }
                return true;
            }
        } else {
            if (table1.isMakeHisMove() && table3.isMakeHisMove()) {
                if (!table1.isFinished()) {
                    table1.setMakeHisMove(false);
                }
                if (!table3.isFinished()) {
                    table3.setMakeHisMove(false);
                }
                return true;
            }
        }
        isPressed = false;
        return false;
    }

    protected void giveFrame() {
        table1.setCancel(false);
        table2.setCancel(false);
        table3.setCancel(false);
        if (idNow == 1) {
            One.setForeground(getFrameId(table1.getWhoWinInt()));
            Two.setForeground(getFrameId(table2.getWhoWinInt()));
        } else if (idNow == 2) {
            One.setForeground(getFrameId(table2.getWhoWinInt()));
            Two.setForeground(getFrameId(table3.getWhoWinInt()));
        } else {
            One.setForeground(getFrameId(table1.getWhoWinInt()));
            Two.setForeground(getFrameId(table3.getWhoWinInt()));
        }
    }

    @Override
    protected Drawable getFrameId(int i) {
        switch (i) {
            case 1:
                return myResources.getDrawable(R.drawable.blue_button);
            case 2:
                return myResources.getDrawable(R.drawable.red_button);
            case 3:
                return myResources.getDrawable(R.drawable.violet_button);
            case 5:
                return myResources.getDrawable(R.drawable.orange_button);
            default:
                return myResources.getDrawable(R.drawable.transperent_thick);
        }
    }

    @Override
    public void Cancel(View view) {
        if (!table.isFinished() && table.isCancel() && !isPressed) {
            if (idNow == 1) {
                cancel(table1);
                cancel(table2);
                if (whoOpen == 1) {
                    deletePreviusTwoTable(table2);
                } else {
                    deletePreviusTwoTable(table1);
                }
                motion.setBackgroundResource(R.drawable.blue_thin);
            } else if (idNow == 2) {
                cancel(table2);
                cancel(table3);
                if (whoOpen == 2) {
                    deletePreviusTwoTable(table3);
                } else {
                    deletePreviusTwoTable(table2);
                }
                motion.setBackgroundResource(R.drawable.red_thin);
            } else {
                cancel(table1);
                cancel(table3);
                if (whoOpen == 1) {
                    deletePreviusTwoTable(table3);
                } else {
                    deletePreviusTwoTable(table1);
                }
                motion.setBackgroundResource(R.drawable.color_three_thin);
            }
            checkFrame();
            changeTable();
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    public void cancel(Table table) {
        super.cancel(table);
        cancelTableThree(table);
        table.setMakeHisMove(false);
    }

    private void deletePreviusTwoTable(Table table) {
        bigSquare[table.getWhereToPoke()].setForeground(null);
        if (table.getPrevious() != null) {
            table.getPrevious().setForeground(null);
        }
    }

    protected void isFinishedContinue() {
        if (isFinished) {
            WinText();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        idNow = 1;
        isThreePlayer = false;
    }
}
