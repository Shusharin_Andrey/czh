package com.shusharin.czh.Field;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.czh.R;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.Interface.Help;
import com.shusharin.czh.MainActivity;
import com.shusharin.czh.SelectionGames.Continue;

import java.util.Objects;

import static com.shusharin.czh.MainActivity.buttonCloseSong;
import static com.shusharin.czh.MainActivity.buttonSong;
import static com.shusharin.czh.MainActivity.displayName;
import static com.shusharin.czh.MainActivity.myResources;
import static com.shusharin.czh.MainActivity.numberDesign;
import static com.shusharin.czh.MainActivity.saveName;
import static com.shusharin.czh.MainActivity.squareDesigns;


public class Field extends AppCompatActivity {

    private static final String TAG = "Field";

    public static final String IS_LOCAL = "IS_LOCAL";
    public static final String IS_LOAD = "IS_LOAD";
    public static final String WIN_ONE = "WIN_ONE";
    public static final String WIN_TWO = "WIN_TWO";
    public static final String NUMBER_STROKE = "NUMBER_STROKE";

    static MediaPlayer smallSquareSong;
    static MediaPlayer bigSquareSong;

    public static int whoOpen;

    protected static int idPlayer;
    protected static int idNotification = 1;
    protected static int index;

    protected static boolean isWin;

    protected View motion;
    protected View frame;
    protected static TableLayout[] bigSquare = new TableLayout[9];
    protected static TableRow[][] bigLine = new TableRow[9][3];
    protected static View[][] smallSquareView = new View[9][9];
    protected TextView motionText;
    protected static Button show;
    protected static Button cancel;

    protected static MediaPlayer winSong;
    protected static MediaPlayer winSmallSong;

    protected Table table;
    protected static Conservation conservation;

    protected boolean isPressed;
    protected boolean isFinished;

    protected View Button;

    protected Button One;
    protected Button Two;
    protected Button Three;

    protected TextView whoWin;
    protected TextView whoWin2;
    protected TextView whoWin4;
    protected TextView whoWin3;

    protected SharedPreferences preferences;

    static int numberStroke = 1;
    TableLayout TableL;

    protected void setHasSentNotification() {
    }

    protected void setNumberStroke() {
        numberStroke++;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThreePlayer.isThreePlayer = false;
        whoOpen = 0;
        isWin = false;
        index = getIntent().getIntExtra(Conservation.NUMBER_CONSERVATION, 0);
        conservation = getIntent().getParcelableExtra(Conservation.TAG);
        if (conservation != null) {
            isFinished = conservation.isFinished();
        } else {
            isFinished = false;
        }
        setContentView(R.layout.activity_field);
        createTable();
        motion = findViewById(R.id.motion);
        motionText = findViewById(R.id.motionText);
        show = findViewById(R.id.show);
        cancel = findViewById(R.id.cancel);
        whoWin = findViewById(R.id.whoWin);
        whoWin2 = findViewById(R.id.whoWin2);
        whoWin3 = findViewById(R.id.whoWin3);
        whoWin4 = findViewById(R.id.whoWin4);
        frame = findViewById(R.id.frame);

        bigSquare[0] = findViewById(R.id.table0);
        bigLine[0][0] = findViewById(R.id.row00);
        bigLine[0][1] = findViewById(R.id.row01);
        bigLine[0][2] = findViewById(R.id.row02);

        bigSquare[1] = findViewById(R.id.table1);
        bigLine[1][0] = findViewById(R.id.row10);
        bigLine[1][1] = findViewById(R.id.row11);
        bigLine[1][2] = findViewById(R.id.row12);

        bigSquare[2] = findViewById(R.id.table2);
        bigLine[2][0] = findViewById(R.id.row20);
        bigLine[2][1] = findViewById(R.id.row21);
        bigLine[2][2] = findViewById(R.id.row22);

        bigSquare[3] = findViewById(R.id.table3);
        bigLine[3][0] = findViewById(R.id.row30);
        bigLine[3][1] = findViewById(R.id.row31);
        bigLine[3][2] = findViewById(R.id.row32);

        bigSquare[4] = findViewById(R.id.table4);
        bigLine[4][0] = findViewById(R.id.row40);
        bigLine[4][1] = findViewById(R.id.row41);
        bigLine[4][2] = findViewById(R.id.row42);

        bigSquare[5] = findViewById(R.id.table5);
        bigLine[5][0] = findViewById(R.id.row50);
        bigLine[5][1] = findViewById(R.id.row51);
        bigLine[5][2] = findViewById(R.id.row52);

        bigSquare[6] = findViewById(R.id.table6);
        bigLine[6][0] = findViewById(R.id.row60);
        bigLine[6][1] = findViewById(R.id.row61);
        bigLine[6][2] = findViewById(R.id.row62);

        bigSquare[7] = findViewById(R.id.table7);
        bigLine[7][0] = findViewById(R.id.row70);
        bigLine[7][1] = findViewById(R.id.row71);
        bigLine[7][2] = findViewById(R.id.row72);

        bigSquare[8] = findViewById(R.id.table8);
        bigLine[8][0] = findViewById(R.id.row80);
        bigLine[8][1] = findViewById(R.id.row81);
        bigLine[8][2] = findViewById(R.id.row82);

        smallSquareView[0][0] = findViewById(R.id.square00);
        smallSquareView[0][1] = findViewById(R.id.square01);
        smallSquareView[0][2] = findViewById(R.id.square02);
        smallSquareView[0][3] = findViewById(R.id.square03);
        smallSquareView[0][4] = findViewById(R.id.square04);
        smallSquareView[0][5] = findViewById(R.id.square05);
        smallSquareView[0][6] = findViewById(R.id.square06);
        smallSquareView[0][7] = findViewById(R.id.square07);
        smallSquareView[0][8] = findViewById(R.id.square08);
        smallSquareView[1][0] = findViewById(R.id.square10);
        smallSquareView[1][1] = findViewById(R.id.square11);
        smallSquareView[1][2] = findViewById(R.id.square12);
        smallSquareView[1][3] = findViewById(R.id.square13);
        smallSquareView[1][4] = findViewById(R.id.square14);
        smallSquareView[1][5] = findViewById(R.id.square15);
        smallSquareView[1][6] = findViewById(R.id.square16);
        smallSquareView[1][7] = findViewById(R.id.square17);
        smallSquareView[1][8] = findViewById(R.id.square18);
        smallSquareView[2][0] = findViewById(R.id.square20);
        smallSquareView[2][1] = findViewById(R.id.square21);
        smallSquareView[2][2] = findViewById(R.id.square22);
        smallSquareView[2][3] = findViewById(R.id.square23);
        smallSquareView[2][4] = findViewById(R.id.square24);
        smallSquareView[2][5] = findViewById(R.id.square25);
        smallSquareView[2][6] = findViewById(R.id.square26);
        smallSquareView[2][7] = findViewById(R.id.square27);
        smallSquareView[2][8] = findViewById(R.id.square28);
        smallSquareView[3][0] = findViewById(R.id.square30);
        smallSquareView[3][1] = findViewById(R.id.square31);
        smallSquareView[3][2] = findViewById(R.id.square32);
        smallSquareView[3][3] = findViewById(R.id.square33);
        smallSquareView[3][4] = findViewById(R.id.square34);
        smallSquareView[3][5] = findViewById(R.id.square35);
        smallSquareView[3][6] = findViewById(R.id.square36);
        smallSquareView[3][7] = findViewById(R.id.square37);
        smallSquareView[3][8] = findViewById(R.id.square38);
        smallSquareView[4][0] = findViewById(R.id.square40);
        smallSquareView[4][1] = findViewById(R.id.square41);
        smallSquareView[4][2] = findViewById(R.id.square42);
        smallSquareView[4][3] = findViewById(R.id.square43);
        smallSquareView[4][4] = findViewById(R.id.square44);
        smallSquareView[4][5] = findViewById(R.id.square45);
        smallSquareView[4][6] = findViewById(R.id.square46);
        smallSquareView[4][7] = findViewById(R.id.square47);
        smallSquareView[4][8] = findViewById(R.id.square48);
        smallSquareView[5][0] = findViewById(R.id.square50);
        smallSquareView[5][1] = findViewById(R.id.square51);
        smallSquareView[5][2] = findViewById(R.id.square52);
        smallSquareView[5][3] = findViewById(R.id.square53);
        smallSquareView[5][4] = findViewById(R.id.square54);
        smallSquareView[5][5] = findViewById(R.id.square55);
        smallSquareView[5][6] = findViewById(R.id.square56);
        smallSquareView[5][7] = findViewById(R.id.square57);
        smallSquareView[5][8] = findViewById(R.id.square58);
        smallSquareView[6][0] = findViewById(R.id.square60);
        smallSquareView[6][1] = findViewById(R.id.square61);
        smallSquareView[6][2] = findViewById(R.id.square62);
        smallSquareView[6][3] = findViewById(R.id.square63);
        smallSquareView[6][4] = findViewById(R.id.square64);
        smallSquareView[6][5] = findViewById(R.id.square65);
        smallSquareView[6][6] = findViewById(R.id.square66);
        smallSquareView[6][7] = findViewById(R.id.square67);
        smallSquareView[6][8] = findViewById(R.id.square68);
        smallSquareView[7][0] = findViewById(R.id.square70);
        smallSquareView[7][1] = findViewById(R.id.square71);
        smallSquareView[7][2] = findViewById(R.id.square72);
        smallSquareView[7][3] = findViewById(R.id.square73);
        smallSquareView[7][4] = findViewById(R.id.square74);
        smallSquareView[7][5] = findViewById(R.id.square75);
        smallSquareView[7][6] = findViewById(R.id.square76);
        smallSquareView[7][7] = findViewById(R.id.square77);
        smallSquareView[7][8] = findViewById(R.id.square78);
        smallSquareView[8][0] = findViewById(R.id.square80);
        smallSquareView[8][1] = findViewById(R.id.square81);
        smallSquareView[8][2] = findViewById(R.id.square82);
        smallSquareView[8][3] = findViewById(R.id.square83);
        smallSquareView[8][4] = findViewById(R.id.square84);
        smallSquareView[8][5] = findViewById(R.id.square85);
        smallSquareView[8][6] = findViewById(R.id.square86);
        smallSquareView[8][7] = findViewById(R.id.square87);
        smallSquareView[8][8] = findViewById(R.id.square88);
        cancel.setBackgroundResource(R.drawable.button_pressed);
        show.setBackgroundResource(R.drawable.button_pressed);
        Two = findViewById(R.id.Two);
        Three = findViewById(R.id.Three);
        One = findViewById(R.id.One);
        Button = One;
        if (savedInstanceState != null) {
            table = savedInstanceState.getParcelable(getString(R.string.table) + 0);
            activitySave();
        }
        MainActivity.isNeedHideNavigationBar=true;
        MainActivity.hideNavigationBar(getWindow());
        preferences = getSharedPreferences(saveName, Context.MODE_PRIVATE);
        smallSquareSong = MediaPlayer.create(this, R.raw.paint_small);
        bigSquareSong = MediaPlayer.create(this, R.raw.what);
        winSong = MediaPlayer.create(this, R.raw.win);
        winSmallSong = MediaPlayer.create(this, R.raw.success);
        smallSquareSong.setOnCompletionListener(mp -> stopPlay(smallSquareSong));
        bigSquareSong.setOnCompletionListener(mp -> stopPlay(bigSquareSong));
        winSong.setOnCompletionListener(mp -> stopPlay(winSong));
        winSmallSong.setOnCompletionListener(mp -> stopPlay(winSmallSong));
        if (MainActivity.isContinue) {
            getData();
        }
        showMotion();
    }

    protected void isFinishedContinue(){
        if (conservation.isFinished()) {
            winSmallSong();
        }
    }

    private void stopPlay(MediaPlayer mediaPlayer) {
        mediaPlayer.stop();
        try {
            mediaPlayer.prepare();
            mediaPlayer.seekTo(0);
        } catch (Throwable t) {
            showMessage(t.getMessage());
        }
    }

    protected void activitySave() {
        bigSquare[4].setForeground(null);
        if (table.isCancel()) {
            cancel.setBackgroundResource(R.drawable.button);
        }
        if (table.getHowShow() != 0) {
            show.setBackgroundResource(R.drawable.button);
        }
    }

    protected void saveData() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(saveName, saveName);
        editor.putInt(NUMBER_STROKE,numberStroke);
        saveDataTable(editor, table, 0);
        editor.apply();
    }

    void saveDataTable(SharedPreferences.Editor edit, Table table, int numberTable) {
        edit.putBoolean(Table.APP_SAVE_IS_FIRST + numberTable, table.isFirst());
        edit.putBoolean(Table.APP_SAVE_IS_SHOW + numberTable, table.isShow());
        edit.putBoolean(Table.APP_SAVE_IS_FINISHED + numberTable, table.isFinished());
        edit.putBoolean(Table.APP_SAVE_IS_CANCEL + numberTable, table.isCancel());
        for (int i = 0; i < 9; i++) {
            edit.putInt(Table.APP_SAVE_BIG_SQUARE_INT + numberTable + i, table.getBigSquareInt(i));
            for (int j = 0; j < 9; j++) {
                edit.putInt(Table.APP_SAVE_SMALL_SQUARE + numberTable + i + j, table.getSmallSquare(i, j));
            }
        }
        edit.putInt(Table.APP_SAVE_POKE_IT + numberTable, table.getPokeIt());
        edit.putInt(Table.APP_SAVE_CANCEL_I + numberTable, table.getCancelI());
        edit.putInt(Table.APP_SAVE_CANCEL_J + numberTable, table.getCancelJ());
        edit.putInt(Table.APP_SAVE_HOW_SHOW + numberTable, table.getHowShow());
        edit.putInt(Table.APP_SAVE_WHERE_TO_POKE + numberTable, table.getWhereToPoke());
        edit.putInt(Table.APP_SAVE_CANCEL_WHERE_TO_POKE + numberTable, table.getCancelWhereToPoke());
        edit.putInt(Table.APP_SAVE_WHAT_TO_PAINT_OVER_BIG + numberTable, table.getWhatToPaintOverBig());
        edit.putInt(Table.APP_SAVE_WHO_WIN_INT + numberTable, table.getWhoWinInt());
        edit.putString(Table.APP_SAVE_WHO_WIN + numberTable, table.getWhoWin());
        edit.putBoolean(Table.APP_SAVE_IS_MAKE_HIS_MOVE + numberTable, table.isMakeHisMove());
        if (table.getPrevious() != null) {
            edit.putInt(Table.APP_SAVE_PREVIOUS + numberTable, table.getPrevious().getId());
        }
        if (table.getCancelBigSquare() != null) {
            edit.putInt(Table.APP_SAVE_CANCEL_BIG_SQUARE + numberTable, table.getCancelBigSquare().getId());
        }
        if (table.getCancelPrevious() != null) {
            edit.putInt(Table.APP_SAVE_CANCEL_PREVIOUS + numberTable, table.getCancelPrevious().getId());
        }
    }

    void getData() {
        if (preferences.contains(saveName)) {
            saveName = preferences.getString(saveName, "");
            numberStroke = preferences.getInt(NUMBER_STROKE,1);
            getDataTable(table, 0);
            activitySave();
        }
    }

    void getDataTable(Table table, int numberTable) {
        table.setFirst(preferences.getBoolean(Table.APP_SAVE_IS_FIRST + numberTable, table.isFinished()));
        table.setShow(preferences.getBoolean(Table.APP_SAVE_IS_SHOW + numberTable, table.isShow()));
        table.setFinished(preferences.getBoolean(Table.APP_SAVE_IS_FINISHED + numberTable, table.isFinished()));
        table.setCancel(preferences.getBoolean(Table.APP_SAVE_IS_CANCEL + numberTable, table.isCancel()));
        for (int i = 0; i < 9; i++) {
            table.setBigSquareInt(i, preferences.getInt(Table.APP_SAVE_BIG_SQUARE_INT + numberTable + i, 0));
            for (int j = 0; j < 9; j++) {
                table.setSmallSquare(i, j, preferences.getInt(Table.APP_SAVE_SMALL_SQUARE + numberTable + i + j, table.getSmallSquare(i, j)));
            }
        }
        table.setPokeIt(preferences.getInt(Table.APP_SAVE_POKE_IT + numberTable, table.getPokeIt()));
        table.setCancelI(preferences.getInt(Table.APP_SAVE_CANCEL_I + numberTable, table.getCancelI()));
        table.setCancelJ(preferences.getInt(Table.APP_SAVE_CANCEL_J + numberTable, table.getCancelJ()));
        table.setHowShow(preferences.getInt(Table.APP_SAVE_HOW_SHOW + numberTable, table.getHowShow()));
        table.setWhereToPoke(preferences.getInt(Table.APP_SAVE_WHERE_TO_POKE + numberTable, table.getWhereToPoke()));
        table.setCancelWhereToPoke(preferences.getInt(Table.APP_SAVE_CANCEL_WHERE_TO_POKE + numberTable, table.getCancelWhereToPoke()));
        table.setWhatToPaintOverBig(preferences.getInt(Table.APP_SAVE_WHAT_TO_PAINT_OVER_BIG + numberTable, table.getWhatToPaintOverBig()));
        table.setWhoWinInt(preferences.getInt(Table.APP_SAVE_WHO_WIN_INT + numberTable, table.getWhoWinInt()));
        table.setWhoWin(preferences.getString(Table.APP_SAVE_WHO_WIN + numberTable, table.getWhoWin()));
        table.setMakeHisMove(preferences.getBoolean(Table.APP_SAVE_IS_MAKE_HIS_MOVE + numberTable, table.isMakeHisMove()));
        if (preferences.contains(Table.APP_SAVE_PREVIOUS + numberTable)) {
            table.setPrevious(findViewById(preferences.getInt(Table.APP_SAVE_PREVIOUS + numberTable, 0)));
        }
        if (preferences.contains(Table.APP_SAVE_CANCEL_BIG_SQUARE + numberTable)) {
            table.setCancelBigSquare(findViewById(preferences.getInt(Table.APP_SAVE_CANCEL_BIG_SQUARE + numberTable, 0)));
        }
        if (preferences.contains(Table.APP_SAVE_CANCEL_PREVIOUS + numberTable)) {
            table.setCancelPrevious(findViewById(preferences.getInt(Table.APP_SAVE_CANCEL_PREVIOUS + numberTable, 0)));
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(getString(R.string.table) + 0, table);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fillPaint();
        isFinishedContinue();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    protected void createTable() {
        table = new Table();
    }

    public void Menu(View view) {
        if (!isPressed) {
            isPressed = true;
            MainActivity.startPlay(buttonSong);
            finish();
            overridePendingTransition(0, 0);
            new Handler().postDelayed(() -> isPressed = false, 250);
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    public void Cancel(View view) {
        if(!isPressed) {
            isPressed = true;
            cancel(table);
            new Handler().postDelayed(() -> isPressed = false, 100);
        }else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    public void cancel(Table table) {
        if (table.isCancel() && !table.isFinished()) {
            table.setCancel(false);
            MainActivity.startPlay(buttonSong);
            smallSquareView[table.getCancelI()][table.getCancelJ()].setForeground(null);
            bigSquare[table.getWhereToPoke()].setForeground(null);
            if (table.getCancelPrevious() != null) {
                table.getCancelPrevious().setForeground(myResources.getDrawable(R.drawable.green_thin));
            }
            table.getCancelBigSquare().setForeground(myResources.getDrawable(R.drawable.gold_frame));
            smallSquareView[table.getCancelI()][table.getCancelJ()].setBackgroundResource(R.drawable.white_thin);
            if (table.eraseOrFillBigSquare(table.getCancelI(), table.getCancelJ(), false)) {
                if (table.getHowShow() == 0) {
                    show.setBackgroundResource(R.drawable.button_pressed);
                    table.setShow(false);
                    show.setText(R.string.Show);
                }
                bigSquare[table.getWhereToPoke()].setBackgroundResource(R.drawable.transperent_thick);
                for (int l = 0; l < 3; l++) {
                    bigLine[table.getPokeIt()][l].setVisibility(View.VISIBLE);
                }
            }
            showMotion();
            cancel.setBackgroundResource(R.drawable.button_pressed);
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    protected void checkMethod(int i, int j, View view) {
        if (table.getSmallSquare(i, j) == Table.ID_EMPTY && (table.getWhereToPoke() == Table.whereDidPokeIt(i, j) || table.getBigSquareInt(table.getWhereToPoke()) != Table.ID_EMPTY) && !table.isFinished()) {
            MainActivity.startPlay(smallSquareSong);
            frame.setVisibility(View.GONE);
            if (!table.isCancel()) {
                table.setCancel(true);
                cancel.setBackgroundResource(R.drawable.button);
            }
            bigSquare[table.getWhereToPoke()].setForeground(null);
            if (table.getPrevious() != null) {
                table.getPrevious().setForeground(null);
            }
            table.setCancelI(i);
            table.setCancelJ(j);
            table.setCancelBigSquare(bigSquare[table.getWhereToPoke()]);
            table.setCancelPrevious(table.getPrevious());
            table.setCancelWhereToPoke(table.getWhereToPoke());
            table.setPrevious(view);
            nextMove();
            paintOver(i, j, view);
            if (table.eraseOrFillBigSquare(i, j, true)) {
                MainActivity.startPlay(bigSquareSong);
                if (table.getHowShow() == 1) {
                    show.setBackgroundResource(R.drawable.button);
                }
                bigSquare[table.getPokeIt()].setBackgroundResource(getBackgroundResourceThick(table.getWhatToPaintOverBig()));
                if (!table.isShow()) {
                    for (int l = 0; l < 3; l++) {
                        bigLine[table.getPokeIt()][l].setVisibility(View.INVISIBLE);
                    }
                }
                if (table.beFinal()) {
                    table.setShow(false);
                    show.setText(R.string.Show);
                    motion.setVisibility(View.GONE);
                    motionText.setVisibility(View.GONE);
                    if (table.isShow()) {
                        buttonCheck();
                    }
                    table.setFinished();
                    table.setWhoWinInt(table.getWhatToPaintOverBig());
                    if (table.getWhatToPaintOverBig() == Table.ID_BLUE) {
                        table.setWhoWin(getString(R.string.Win_blue));
                    } else if (table.getWhatToPaintOverBig() == Table.ID_RED) {
                        table.setWhoWin(getString(R.string.Win_red));
                    } else if (table.getWhatToPaintOverBig() == Table.ID_ORANGE) {
                        table.setWhoWin(getString(R.string.Win_orange));
                    } else {
                        table.setWhoWin(getString(R.string.Draw));
                    }
                    setWhoWin(View.VISIBLE);
                    for (int l = 0; l < 9; l++) {
                        for (int n = 0; n < 3; n++) {
                            bigLine[l][n].setVisibility(View.INVISIBLE);
                        }
                    }
                    winSmallSong();
                }
            }
            table.setWhereToPoke(Table.whereDrawFrame(i, j));
            if (!table.isFinished()) {
                checkFrame();
            } else {
                cancel.setBackgroundResource(R.drawable.button_pressed);
            }
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    void nextMove() {
        setNumberStroke();
    }

    protected void winSmallSong() {
        if (!isWin) {
            isWin = true;
            Continue.conservations.get(index).setFinished(true);
            Intent intent = new Intent(this, Statistics.class);
            intent.putExtra(Conservation.NUMBER_FIELD, 1);
            intent.putExtra(Conservation.NUMBER_PLAYER, 1);
            intent.putExtra(Conservation.TAG, conservation);
            if (table.getWhoWinInt() == Table.ID_BLUE) {
                intent.putExtra(WIN_ONE, 10);
                intent.putExtra(WIN_TWO, 0);
            } else if (table.getWhoWinInt() == Table.ID_RED) {
                intent.putExtra(WIN_ONE, 0);
                intent.putExtra(WIN_TWO, 10);
            } else {
                intent.putExtra(WIN_ONE, 5);
                intent.putExtra(WIN_TWO, 5);
            }
            intent.putExtra(IS_LOCAL, true);
            intent.putExtra(getString(R.string.table) + 1, table);
            intent.putExtra(Table.APP_SAVE_WHO_WIN, table.getWhoWin());
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
    }

    protected void check(int i, int j, View view) {
        checkMethod(i, j, view);
    }

    protected void setWhoWin(int visibility) {
        whoWin.setVisibility(visibility);
        whoWin2.setVisibility(visibility);
        whoWin3.setVisibility(visibility);
        whoWin4.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            whoWin.setText(table.getWhoWin());
            whoWin2.setText(table.getWhoWin());
            whoWin3.setText(table.getWhoWin());
            whoWin4.setText(table.getWhoWin());
        }
    }

    protected void checkFrame() {
        if (!table.isMakeHisMove()) {
            if (table.getBigSquareInt(table.getWhereToPoke()) == Table.ID_EMPTY) {
                bigSquare[table.getWhereToPoke()].setForeground(myResources.getDrawable(R.drawable.gold_frame));
            } else {
                frame.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickShow(View view) {
        if (table.getHowShow() != 0 && !isPressed) {
            isPressed = true;
            MainActivity.startPlay(buttonSong);
            int visibility = table.isShow() ? View.INVISIBLE : View.VISIBLE;
            if (table.isFinished()) {
                setWhoWin(!table.isShow() ? View.INVISIBLE : View.VISIBLE);
            }
            buttonCheck();
            for (int l = 0; l < 9; l++) {
                show(l, visibility);
            }
            new Handler().postDelayed(() -> isPressed = false, 100);
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    protected void show(int i, int visibility) {
        if (table.getBigSquareInt(i) != Table.ID_EMPTY || table.isFinished()) {
            for (int n = 0; n < 3; n++) {
                bigLine[i][n].setVisibility(visibility);
            }
        }
    }

    protected void buttonCheck() {
        table.invertShow();
        show.setText(table.isShow() ? R.string.Hide : R.string.Show);
    }

    protected void paintOver(int i, int j, View view) {
        table.setSmallSquare(i, j, table.isFirst() ? Table.ID_BLUE : Table.ID_RED);
        table.getPrevious().setForeground(myResources.getDrawable(R.drawable.green_thin));
        view.setBackgroundResource(getBackgroundResourceThin(table.isFirst ? 1 : 2));
        motion.setBackgroundResource(getBackgroundResourceThin(!table.isFirst ? 1 : 2));
        motionText.setText(getString(R.string.motion_text, String.valueOf(numberStroke), table.isFirst ? conservation.getNameTwoPlayer() : conservation.getNameOnePlayer()));
        table.invertFirst();
    }

    public static int getBackgroundResourceThin(int t) {
        if (t == Table.ID_BLUE) {
            return squareDesigns[numberDesign].getDesignSmallBlue();
        } else if (t == Table.ID_RED) {
            return squareDesigns[numberDesign].getDesignSmallRed();
        } else if (t == Table.ID_ORANGE) {
            return squareDesigns[numberDesign].getDesignSmallOrange();
        } else if (t == Table.ID_BIG_SQUARE_DRAW) {
            return squareDesigns[numberDesign].getDesignSmallViolet();
        } else {
            return squareDesigns[numberDesign].getDesignWhite();
        }
    }

    public static int getBackgroundResourceThick(int t) {
        if (t == Table.ID_BLUE) {
            return squareDesigns[numberDesign].getDesignBigBlue();
        } else if (t == Table.ID_RED) {
            return squareDesigns[numberDesign].getDesignBigRed();
        } else if (t == Table.ID_ORANGE) {
            return squareDesigns[numberDesign].getDesignBigOrange();
        } else if (t == Table.ID_BIG_SQUARE_DRAW) {
            return squareDesigns[numberDesign].getDesignBigViolet();
        } else {
            return squareDesigns[numberDesign].getDesignTransperent();
        }
    }

    public void onClickSquare(View view) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (smallSquareView[i][j].getId() == view.getId()) {
                    check(i, j, view);
                    return;
                }
            }
        }
    }

    protected void fillPaint() {
        for (int i = 0; i < 9; i++) {
            bigSquare[i].setBackgroundResource(getBackgroundResourceThick(table.getBigSquareInt(i)));
            for (int j = 0; j < 9; j++) {
                smallSquareView[i][j].setBackgroundResource(getBackgroundResourceThin(table.getSmallSquare(i, j)));
            }
            show(i, View.INVISIBLE);
        }
        table.setShow(false);
        show.setText(R.string.Show);
        if (table.getHowShow() != 0) {
            show.setBackgroundResource(R.drawable.button);
        }
        if (table.getPrevious() != null) {
            table.getPrevious().setForeground(myResources.getDrawable(R.drawable.green_thin));
        }
        if (table.isCancel() && !table.isFinished()) {
            cancel.setBackgroundResource(R.drawable.button);
        }
        if (table.isFinished()) {
            setWhoWin(View.VISIBLE);
        } else {
            checkFrame();
            if (!table.isMakeHisMove()) {
                motion.setVisibility(View.VISIBLE);
                motionText.setVisibility(View.VISIBLE);
            }
        }
        showMotion();
    }

    protected void showMotion() {
        showMotion(motion, motionText, table);
    }

    protected static void showMotion(View motion, TextView motionText, Table table) {
        if (ThreePlayer.isThreePlayer) {
            motion.setBackgroundResource(getBackgroundResourceThin(ThreePlayer.idNow));
            motionText.setText(myResources.getString(R.string.motion_text, String.valueOf(numberStroke), ThreePlayer.idNow == 1 ? conservation.getNameOnePlayer() : ThreePlayer.idNow == 2 ? conservation.getNameTwoPlayer() : conservation.getNameThreePlayer()));
        } else {
            motion.setBackgroundResource(getBackgroundResourceThin(table.isFirst() ? 1 : 2));
            motionText.setText(myResources.getString(R.string.motion_text, String.valueOf(numberStroke), table.isFirst() ? conservation.getNameOnePlayer() : conservation.getNameTwoPlayer()));
        }
    }

    public void onClickHelp(View view) {
        if (!isPressed) {
            isPressed = true;
            MainActivity.startPlay(buttonSong);
            Intent intent = new Intent(this, Help.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
            new Handler().postDelayed(() -> isPressed = false, 100);
        }
    }

    protected void sendNotification() {

    }

    protected void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.emailpassword_status_name, displayName);
            String description = getString(R.string.anonim);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(TAG, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
        }
    }

    @SuppressLint("StringFormatMatches")
    protected void ShowNotification(String text) {

        Intent intent = new Intent(this, Continue.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        createNotificationChannel();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, TAG)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(getString(R.string.Rejoice))
                .setContentText(getString(R.string.your_turn, numberStroke, text))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        try {
            notificationManager.notify(idNotification, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void showMessage(String textInMessage) {
        Toast.makeText(getApplicationContext(), textInMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        numberStroke = 1;
    }
}
