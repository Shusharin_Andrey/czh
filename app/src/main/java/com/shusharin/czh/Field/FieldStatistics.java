package com.shusharin.czh.Field;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.czh.R;

public class FieldStatistics extends Field {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        table = getIntent().getParcelableExtra(getString(R.string.table));
        bigSquare[4].setForeground(null);
        Button menu = findViewById(R.id.menu);
        menu.setText(R.string.back);
        motion.setVisibility(View.GONE);
        motionText.setVisibility(View.GONE);
        cancel.setBackgroundResource(R.drawable.button_pressed);
        show.setBackgroundResource(R.drawable.button);
        table.setShow(true);
        table.setHowShow(1);
    }
    protected void isFinishedContinue(){
    }
}
