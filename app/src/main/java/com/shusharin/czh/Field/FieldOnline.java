package com.shusharin.czh.Field;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.czh.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.MainActivity;

import static com.shusharin.czh.MainActivity.displayName;
import static com.shusharin.czh.MainActivity.myResources;
import static com.shusharin.czh.MainActivity.saveName;

public class FieldOnline extends Field {
    private static final String TAG = "FieldOnline";

    public static String APP_SAVE_ID_PLAYER = "ID_PLAYER";
    public static String APP_SAVE_HAS_SENT_NOTIFICATION = "HAS_SENT_NOTIFICATION";
    public static String APP_SAVE_COUNTER_STROKE = "COUNTER_STROKE";

    private DatabaseReference DATABASE_HAS_SENT_NOTIFICATION;
    private DatabaseReference DATABASE_COUNTER_STROKE;
    private DatabaseReference DATABASE_IS_PRESENT_FIRST_PLAYER;
    private DatabaseReference DATABASE_IS_PRESENT_SECOND_PLAYER;

    private DatabaseReference reference;

    private ValueEventListener LISTENER_HAS_SENT_NOTIFICATION;
    private ValueEventListener LISTENER_IS_FIND;
    private ValueEventListener LISTENER_IS_PRESENT_FIRST_PLAYER;
    private ValueEventListener LISTENER_IS_PRESENT_SECOND_PLAYER;
    private ValueEventListener LISTENER_COUNTER_STROKE;

    private static final int NOTIFY_ID = 2;

    protected void isFinishedContinue() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (conservation.isFinished()) {
            winSmallSong();
        }
        reference = FirebaseDatabase.getInstance().getReference(getString(R.string.Games));
        DatabaseReference databaseUser = reference.child(saveName).child(getString(R.string.Field)).child(displayName);
        LISTENER_IS_FIND = isFind(databaseUser, APP_SAVE_ID_PLAYER);

        DATABASE_COUNTER_STROKE = databaseUser.child(APP_SAVE_COUNTER_STROKE);
        LISTENER_COUNTER_STROKE = update(DATABASE_COUNTER_STROKE,3);

        DatabaseReference referenceTable = reference.child(saveName).child(getString(R.string.Field));
        DATABASE_HAS_SENT_NOTIFICATION = referenceTable.child(APP_SAVE_HAS_SENT_NOTIFICATION);
        LISTENER_HAS_SENT_NOTIFICATION = update(DATABASE_HAS_SENT_NOTIFICATION, 0);
    }

    private ValueEventListener update(DatabaseReference databaseReference, int number) {
        return databaseReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("StringFormatMatches")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (dataSnapshot.getValue() != null) {
                    switch (number) {
                        case 0:
                            if (dataSnapshot.getValue(boolean.class)) {
                                if ((table.isFirst() && idPlayer == 1) || (!table.isFirst() && idPlayer == 2)) {
                                    setNumberStroke();
                                    showMessage(getString(R.string.notification_of_your_turn, numberStroke));
                                    MainActivity.startPlay(MainActivity.notification);
                                    DATABASE_HAS_SENT_NOTIFICATION.setValue(false);
                                    ShowNotification(conservation.getName());
                                }
                                showMotion();
                                if (isFinished) {
                                    winSmallSong();
                                }
                            }
                            break;
                        case 1:
                        case 2:
                            String numberPlayer;
                            String isJoned;
                            if (number != idPlayer) {
                                if (number == 1) {
                                    numberPlayer = conservation.getNameOnePlayer();
                                } else{
                                    numberPlayer = conservation.getNameTwoPlayer();
                                }
                                if (dataSnapshot.getValue(boolean.class)) {
                                    isJoned = myResources.getString(R.string.joined);
                                } else {
                                    isJoned = getString(R.string.came_out);
                                }
                                showMessage(numberPlayer + isJoned);
                                MainActivity.startPlay(MainActivity.notification);
                            }
                            break;
                        case 3:
                            numberStroke = dataSnapshot.getValue(Integer.class);
                            showMotion();
                            DATABASE_COUNTER_STROKE.removeEventListener(LISTENER_COUNTER_STROKE);
                            break;

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, getString(R.string.fail_read), error.toException());
            }
        });
    }

    void nextMove(){}

    private void findUsers() {
        if (reference != null) {
            if (LISTENER_IS_FIND != null) {
                reference.removeEventListener(LISTENER_IS_FIND);
            }
            DatabaseReference database = reference.child(saveName).child(getString(R.string.Field));
            DATABASE_IS_PRESENT_FIRST_PLAYER = database.child("isPresent" + Conservation.ONE_PLAYER);
            DATABASE_IS_PRESENT_SECOND_PLAYER = database.child("isPresent" + Conservation.TWO_PLAYER);
            LISTENER_IS_PRESENT_FIRST_PLAYER = update(DATABASE_IS_PRESENT_FIRST_PLAYER, 1);
            LISTENER_IS_PRESENT_SECOND_PLAYER = update(DATABASE_IS_PRESENT_SECOND_PLAYER, 2);
        }
    }

    private ValueEventListener isFind(DatabaseReference databaseReference, String children) {
        return databaseReference.child(children).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Integer.class) != null) {
                    idPlayer = dataSnapshot.getValue(Integer.class);
                    sendPresent(true);
                    findUsers();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, getString(R.string.fail_read), error.toException());
            }
        });
    }

    private void sendPresent(boolean present) {
        String numberPlayer;
        if (idPlayer == 1) {
            numberPlayer = Conservation.ONE_PLAYER;
        } else if (idPlayer == 2) {
            numberPlayer = Conservation.TWO_PLAYER;
        } else {
            numberPlayer = Conservation.THREE_PLAYER;
        }
        reference.child(saveName).child(getString(R.string.Field)).child("isPresent" + numberPlayer).setValue(present);
    }

    protected void setNumberStroke() {
        super.setNumberStroke();
        DATABASE_COUNTER_STROKE.setValue(numberStroke);
    }

    protected void winSmallSong() {
        if (!isWin) {
            isWin = true;
            FirebaseDatabase.getInstance().getReference(getString(R.string.name_game)).child(conservation.getName()).child(getString(R.string.finished)).setValue(true);
            Intent intent = new Intent(this, Statistics.class);
            intent.putExtra(Conservation.NUMBER_FIELD, 1);
            intent.putExtra(Conservation.NUMBER_PLAYER, 2);
            intent.putExtra(Field.IS_LOCAL, false);
            intent.putExtra(Conservation.TAG, conservation);
            saveName = conservation.getName();
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
    }

    @Override
    protected void createTable() {
        table = new TableOnline(getString(R.string.table) + 0, 0, this);
    }

    protected void checkMethod(int i, int j, View view) {
        if (idPlayer == (table.isFirst() ? 1 : 2))
            if (table.getSmallSquare(i, j) == Table.ID_EMPTY && (table.getWhereToPoke() == Table.whereDidPokeIt(i, j) || table.getBigSquareInt(table.getWhereToPoke()) != Table.ID_EMPTY) && !table.isFinished()) {
                super.checkMethod(i, j, view);
                sendNotification();
            }
    }

    void getData() {

    }

    protected void saveData() {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void sendNotification() {
        setHasSentNotification();
    }

    public void setHasSentNotification() {
        DATABASE_HAS_SENT_NOTIFICATION.setValue(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sendPresent(false);
        table.stop();
        if (reference != null) {
            if (LISTENER_HAS_SENT_NOTIFICATION != null) {
                DATABASE_HAS_SENT_NOTIFICATION.removeEventListener(LISTENER_HAS_SENT_NOTIFICATION);
            }
            if (LISTENER_IS_PRESENT_FIRST_PLAYER != null) {
                DATABASE_IS_PRESENT_FIRST_PLAYER.removeEventListener(LISTENER_IS_PRESENT_FIRST_PLAYER);
            }
            if (LISTENER_IS_PRESENT_SECOND_PLAYER != null) {
                DATABASE_IS_PRESENT_SECOND_PLAYER.removeEventListener(LISTENER_IS_PRESENT_SECOND_PLAYER);
            }
        }
    }
}
