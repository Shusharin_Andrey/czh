package com.shusharin.czh.Field;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.shusharin.czh.Conservation;
import com.shusharin.czh.Neural.NeuralNetwork;

import java.util.ArrayList;

import static com.shusharin.czh.Field.FieldThree.random;

public class OnePlayer extends Field {
    static NeuralNetwork network = new NeuralNetwork(new int[]{82, 9, 81}, 82);
    static NeuralNetwork network1 = new NeuralNetwork(new int[]{82, 9, 81}, 82);
    private static boolean isFirst;
    private static boolean isAnimate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        network.readFile("active/neuralNetworkFirst0.txt", this);
        network1.readFile("active/neuralNetworkSecond0.txt", this);
        isFirst = getIntent().getBooleanExtra(Conservation.IS_FIRST, true);
        if (!isFirst && table.isFirst()) {
            MakeTurnNetwork(network1);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    protected void check(int i1, int j1, View view) {
        if (!isAnimate) {
            if (table.getSmallSquare(i1, j1) == Table.ID_EMPTY && (table.getWhereToPoke() == Table.whereDidPokeIt(i1, j1) || table.getBigSquareInt(table.getWhereToPoke()) != Table.ID_EMPTY) && !table.isFinished()) {
                isAnimate = true;
                checkMethod(i1, j1, view);
                new Handler().postDelayed(() -> {
                    if (isFirst) {
                        MakeTurnNetwork(network);
                    } else {
                        MakeTurnNetwork(network1);
                    }
                    isAnimate = false;
                }, 250);
            }
        }
    }

    private void MakeTurnNetwork(NeuralNetwork network) {
        double[] list = new double[82];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                list[Table.get_number(i, j)] = table.getSmallSquare(i, j);
            }
        }
        if (table.getBigSquareInt(table.getWhereToPoke()) == Table.ID_EMPTY) {
            list[81] = table.getWhereToPoke();
        } else {
            list[81] = 10;
        }
        int[] ij = network.getAction(list);
        checkMethod(ij[0], ij[1], smallSquareView[ij[0]][ij[1]]);
    }

    private void butlTwoNetworks(NeuralNetwork neuralNetwork1, NeuralNetwork neuralNetwork2) {
        double[] list = new double[82];
        if (!table.isFinished()) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    list[Table.get_number(i, j)] = table.getSmallSquare(i, j);
                }
            }
            if (table.getBigSquareInt(table.getWhereToPoke()) == 0) {
                list[81] = table.getWhereToPoke();
            } else {
                list[81] = 10;
            }
//            if (table.isFirst()) {
//                checkMethod(neuralNetwork1.getAction(list));
//            } else {
//                checkMethod(neuralNetwork2.getAction(list));
//            }
            if (!table.isFirst()) {
                checkMethod(neuralNetwork1.getAction(list));
            } else {
                checkMethod(randomMotion(table));
            }
        }
        new Handler().postDelayed(() -> {
            butlTwoNetworks(neuralNetwork1, neuralNetwork2);
        }, 500);
    }

    private void checkMethod(int[] ij) {
        int i = ij[0];
        int j = ij[1];
        checkMethod(i, j, smallSquareView[i][j]);
    }

    protected int[] randomMotion(Table table) {
        ArrayList<Integer> arrayListI = new ArrayList<>();
        ArrayList<Integer> arrayListJ = new ArrayList<>();
        if (table.getBigSquareInt(table.getWhereToPoke()) != Table.ID_EMPTY) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (table.getSmallSquare(i, j) == Table.ID_EMPTY) {
                        arrayListI.add(i);
                        arrayListJ.add(j);
                    }
                }
            }
        } else {
            int pokeIt = table.getWhereToPoke();
            int x = pokeIt % 3;
            int y = pokeIt / 3;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (table.getSmallSquare(i + 3 * y, j + 3 * x) == Table.ID_EMPTY) {
                        arrayListI.add(i + 3 * y);
                        arrayListJ.add(j + 3 * x);
                    }
                }
            }
        }
        int q = random.nextInt(arrayListI.size());
        return new int[]{arrayListI.get(q), arrayListJ.get(q)};
    }
}


