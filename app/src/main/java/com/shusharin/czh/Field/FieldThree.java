package com.shusharin.czh.Field;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.czh.R;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.MainActivity;
import com.shusharin.czh.SelectionGames.Continue;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;
import java.util.Random;

import static com.shusharin.czh.MainActivity.buttonCloseSong;
import static com.shusharin.czh.MainActivity.buttonSong;
import static com.shusharin.czh.MainActivity.myResources;
import static com.shusharin.czh.MainActivity.saveName;

public class FieldThree extends Field {

    private static final String TAG = "FieldThree";

    public static final String WHO_OPEN = "WHO_OPEN";
    public static final String APP_SAVE_HOW_MAKE_HIS_MOVE = "HOW_MAKE_HIS_MOVE";
    public static final String RANDOM_I = "RANDOM_I";
    public static final String RANDOM_J = "RANDOM_J";
    public static final String APP_SAVE_HOW_MANE_FIELD = "HOW_MANE_FIELD";
    public static final String COUNTER = "COUNTER";

    protected static Random random = new Random();

    protected Table table1;
    protected Table table2;
    protected Table table3;

    protected int howMakeHisMove;
    protected int randomI;
    protected int randomJ;
    protected int howManyFields = 3;
    protected int counter;
    protected int winOne;
    protected int winTwo;

    public void setHowMakeHisMove(int howMakeHisMove) {
        this.howMakeHisMove = howMakeHisMove;
    }

    public void setHowManyFields(int howManyFields) {
        this.howManyFields = howManyFields;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public void setWinOne(int winOne) {
        this.winOne = winOne;
    }

    public void setWinTwo(int winTwo) {
        this.winTwo = winTwo;
    }

    void nextMove() {
    }

    void nextMoveThreeField() {
        super.nextMove();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Two.setVisibility(View.VISIBLE);
        Three.setVisibility(View.VISIBLE);
        One.setVisibility(View.VISIBLE);
        One.setSelected(true);
        One.setOnClickListener(v -> onClickButton(One, 1));
        Two.setOnClickListener(v -> onClickButton(Two, 2));
        Three.setOnClickListener(v -> onClickButton(Three, 3));
        if (savedInstanceState != null) {
            table1 = savedInstanceState.getParcelable(getString(R.string.table) + 1);
            table2 = savedInstanceState.getParcelable(getString(R.string.table) + 2);
            table3 = savedInstanceState.getParcelable(getString(R.string.table) + 3);
            whoOpen = savedInstanceState.getInt(WHO_OPEN);
            howMakeHisMove = savedInstanceState.getInt(APP_SAVE_HOW_MAKE_HIS_MOVE);
            randomI = savedInstanceState.getInt(RANDOM_I);
            randomJ = savedInstanceState.getInt(RANDOM_J);
            howManyFields = savedInstanceState.getInt(APP_SAVE_HOW_MANE_FIELD);
            counter = savedInstanceState.getInt(COUNTER);
            winOne = savedInstanceState.getInt(WIN_ONE);
            winTwo = savedInstanceState.getInt(WIN_TWO);
            bigSquare[4].setForeground(null);
            changeTable();
            checkFrameTable();
        }
    }

    @Override
    protected void saveData() {
        super.saveData();
        SharedPreferences.Editor editor = preferences.edit();
        saveDataTable(editor, table1, 1);
        saveDataTable(editor, table2, 2);
        saveDataTable(editor, table3, 3);
        editor.putInt(WHO_OPEN, whoOpen);
        editor.putInt(APP_SAVE_HOW_MAKE_HIS_MOVE, howMakeHisMove);
        editor.putInt(RANDOM_I, randomI);
        editor.putInt(RANDOM_J, randomJ);
        editor.putInt(APP_SAVE_HOW_MANE_FIELD, howManyFields);
        editor.putInt(COUNTER, counter);
        editor.putInt(WIN_ONE, winOne);
        editor.putInt(WIN_TWO, winTwo);
        editor.apply();
    }

    @Override
    void getData() {
        if (preferences.contains(saveName)) {
            super.getData();
            getDataTable(table1, 1);
            getDataTable(table2, 2);
            getDataTable(table3, 3);
            whoOpen = preferences.getInt(WHO_OPEN, whoOpen);
            howMakeHisMove = preferences.getInt(APP_SAVE_HOW_MAKE_HIS_MOVE, howMakeHisMove);
            randomI = preferences.getInt(RANDOM_I, randomI);
            randomJ = preferences.getInt(RANDOM_J, randomJ);
            howManyFields = preferences.getInt(APP_SAVE_HOW_MANE_FIELD, howManyFields);
            counter = preferences.getInt(COUNTER, counter);
            winOne = preferences.getInt(WIN_ONE, winOne);
            winTwo = preferences.getInt(WIN_TWO, winTwo);
            activitySave();
        }
    }

    @Override
    protected void activitySave() {
        super.activitySave();
        bigSquare[4].setForeground(null);
        changeTable();
        One.setSelected(false);
        if (whoOpen == 1) {
            One.setSelected(true);
            Button = One;
        } else if (whoOpen == 2) {
            Two.setSelected(true);
            Button = Two;
        } else {
            Three.setSelected(true);
            Button = Three;
        }
        checkFrameTable();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(getString(R.string.table) + 1, table1);
        outState.putParcelable(getString(R.string.table) + 2, table2);
        outState.putParcelable(getString(R.string.table) + 3, table3);
        outState.putInt(WHO_OPEN, whoOpen);
        outState.putInt(APP_SAVE_HOW_MAKE_HIS_MOVE, howMakeHisMove);
        outState.putInt(RANDOM_I, randomI);
        outState.putInt(RANDOM_J, randomJ);
        outState.putInt(APP_SAVE_HOW_MANE_FIELD, howManyFields);
        outState.putInt(COUNTER, counter);
        outState.putInt(WIN_ONE, winOne);
        outState.putInt(WIN_TWO, winTwo);
    }

    @Override
    public void Cancel(View view) {
        if (!table.isFinished() && table.isCancel() && !isPressed) {
            super.Cancel(view);
            if (howManyFields != 1 && howMakeHisMove > 0) {
                setHowMakeHisMove(howMakeHisMove - 1);
            }
            if (table.isMakeHisMove()) {
                table.setMakeHisMove(false);
                checkFrame();
            } else {
                if (whoOpen == 1) {
                    cancelTableThree(table2);
                    cancelTableThree(table3);
                } else if (whoOpen == 2) {
                    cancelTableThree(table1);
                    cancelTableThree(table3);
                } else {
                    cancelTableThree(table2);
                    cancelTableThree(table1);
                }
            }
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    protected void cancelTableThree(Table table) {
        if (!table.isFinished() && table.isCancel()) {
            table.eraseOrFillBigSquare(table.getCancelI(), table.getCancelJ(), false);
        }
    }

    @Override
    protected void check(int i, int j, View view) {
        if (!table.isMakeHisMove() && !isPressed) {
            if (table.getSmallSquare(i, j) == Table.ID_EMPTY && (table.getWhereToPoke() == Table.whereDidPokeIt(i, j) || table.getBigSquareInt(table.getWhereToPoke()) != Table.ID_EMPTY) && !table.isFinished()) {
                super.check(i, j, view);
                if (howManyFields == 3) {
                    if (table.getBigSquareInt(table.getWhereToPoke()) == Table.ID_EMPTY) {
                        bigSquare[table.getWhereToPoke()].setForeground(null);
                    } else {
                        frame.setVisibility(View.GONE);
                    }
                }
                if (howManyFields != 1) {
                    setHowMakeHisMove(howMakeHisMove + 1);
                    table.setMakeHisMove(true);
                }
                if (table.isFinished()) {
                    CheckIsFinishedAll();
                }
                if (howManyFields == 1) {
                    sendNotification();
                }
                if ((howMakeHisMove == 2 && howManyFields == 3) || (howMakeHisMove == 1 && howManyFields == 2)) {
                    setHowManyFields(howManyFields - counter);
                    setCounter(0);
                    setHowMakeHisMove(0);
                    if (!table1.isMakeHisMove() && !table1.isFinished()) {
                        randomMotion(table1);
                        whoOpen = 1;
                        if (!table2.isFinished()) table2.setMakeHisMove(false);
                        if (!table3.isFinished()) table3.setMakeHisMove(false);
                    } else if (!table2.isMakeHisMove() && !table2.isFinished()) {
                        if (!table1.isFinished()) table1.setMakeHisMove(false);
                        randomMotion(table2);
                        whoOpen = 2;
                        if (!table3.isFinished()) table3.setMakeHisMove(false);
                    } else {
                        if (!table1.isFinished()) table1.setMakeHisMove(false);
                        if (!table2.isFinished()) table2.setMakeHisMove(false);
                        randomMotion(table3);
                        whoOpen = 3;
                    }
                    isPressed = true;
                    new Handler().postDelayed(() -> {
                        changeTable();
                        if(whoOpen==1){
                            ChangePressed(One);
                        }else if(whoOpen==2){
                            ChangePressed(Two);
                        }else{
                            ChangePressed(Three);
                        }
                        new Handler().postDelayed(() -> {
                            super.check(randomI, randomJ, smallSquareView[randomI][randomJ]);
                            if (table.isFinished()) {
                                CheckIsFinishedAll();
                            }
                            checkFrameTable();
                            sendNotification();
                            isPressed = false;
                        }, 250);
                    }, 250);
                }
                checkFrameTable();
            }
        }
    }

    protected void sendNotification() {
        nextMoveThreeField();
        showMotion();
    }

    protected void CheckIsFinishedAll() {
        table1.setCancel(false);
        table2.setCancel(false);
        table3.setCancel(false);
        table.setWhoWin("");
        setWhoWin(View.GONE);
        setCounter(counter + 1);
        if (table.getWhoWinInt() == Table.ID_BLUE) {
            setWinOne(winOne + 10);
        } else if (table.getWhoWinInt() == Table.ID_RED) {
            setWinTwo(winTwo + 10);
        } else {
            setWinOne(winOne + 5);
            setWinTwo(winTwo + 5);
        }
        if (winOne + winTwo == 30 || winOne == 20 || winTwo == 20) {
            WinText();
        }
    }

    protected void WinText() {
        if (!isWin) {
            isWin = true;
            Continue.conservations.get(index).setFinished(true);
            table1.setFinished();
            table2.setFinished();
            table3.setFinished();
            setWhoWin(View.VISIBLE);
            Intent intent = new Intent(this, Statistics.class);
            intent.putExtra(Conservation.NUMBER_FIELD, 3);
            intent.putExtra(Conservation.NUMBER_PLAYER, 2);
            intent.putExtra(WIN_ONE, winOne);
            intent.putExtra(WIN_TWO, winTwo);
            intent.putExtra(getString(R.string.table) + 1, table1);
            intent.putExtra(getString(R.string.table) + 2, table2);
            intent.putExtra(getString(R.string.table) + 3, table3);
            intent.putExtra(Conservation.TAG, conservation);
            intent.putExtra(IS_LOCAL, true);
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
    }

    protected void randomMotion(Table table) {
        ArrayList<Integer> arrayListI = new ArrayList<>();
        ArrayList<Integer> arrayListJ = new ArrayList<>();
        if (table.getBigSquareInt(table.getWhereToPoke()) != Table.ID_EMPTY) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (table.getSmallSquare(i, j) == Table.ID_EMPTY) {
                        arrayListI.add(i);
                        arrayListJ.add(j);
                    }
                }
            }
        } else {
            int pokeIt = table.getWhereToPoke();
            int x = pokeIt % 3;
            int y = pokeIt / 3;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (table.getSmallSquare(i + 3 * y, j + 3 * x) == Table.ID_EMPTY) {
                        arrayListI.add(i + 3 * y);
                        arrayListJ.add(j + 3 * x);
                    }
                }
            }
        }
        int q = random.nextInt(arrayListI.size());
        randomI = arrayListI.get(q);
        randomJ = arrayListJ.get(q);
    }

    protected void createTable() {
        table1 = new Table();
        table2 = new Table();
        table3 = new Table();
        table = table1;
        whoOpen = 1;
    }

    protected void ChangePressed(View view) {
        Button.setSelected(false);
        view.setSelected(true);
        Button = view;
    }

    public void onClickButton(View view, int whoNeedOpen) {
        if (whoOpen != whoNeedOpen && !isPressed) {
            whoOpen = whoNeedOpen;
            isPressed = true;
            MainActivity.startPlay(buttonSong);
            ChangePressed(view);
            changeTable();
            new Handler().postDelayed(() -> isPressed = false, 100);
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    @Override
    protected void winSmallSong() {
        MainActivity.startPlay(winSmallSong);
    }


    @Contract(pure = true)
    protected Drawable getFrameId(int i) {
        if (i == Table.ID_BLUE) {
            return myResources.getDrawable(R.drawable.blue_button);
        } else if (i == Table.ID_RED) {
            return myResources.getDrawable(R.drawable.red_button);
        } else if (i == Table.ID_BIG_SQUARE_DRAW) {
            return myResources.getDrawable(R.drawable.violet_button);
        } else {
            return myResources.getDrawable(R.drawable.green_button);
        }
    }

    private void getView(View view, int i) {
        if (i != Table.ID_EMPTY) {
            view.setForeground(getFrameId(i));
        }
    }

    protected void changeTable() {
        bigSquare[table.getWhereToPoke()].setForeground(null);
        if (table.getPrevious() != null) {
            table.getPrevious().setForeground(null);
        }
        for (int l = 0; l < 9; l++) {
            show(l, View.VISIBLE);
        }
        if (table.isFinished()) {
            setWhoWin(View.GONE);
            motion.setVisibility(View.GONE);
            motionText.setVisibility(View.GONE);
        }
        if (table.getHowShow() != 0) {
            show.setBackgroundResource(R.drawable.button_pressed);
        }
        if (table.isCancel()) {
            cancel.setBackgroundResource(R.drawable.button_pressed);
        }
        frame.setVisibility(View.GONE);
        if (whoOpen == 1) {
            table = table1;
        } else if (whoOpen == 2) {
            table = table2;
        } else {
            table = table3;
        }
        fillPaint();
    }

    protected void checkFrameTable() {
        One.setForeground(null);
        Three.setForeground(null);
        Two.setForeground(null);
        int[] m = new int[3];// 1 2 3
        //1,2,3 синий красный фиолетовый 4 зелёный
        if (table1.isMakeHisMove()) {
            m[0] = 4;
        } else if (table2.isMakeHisMove()) {
            m[1] = 4;
        } else if (table3.isMakeHisMove()) {
            m[2] = 4;
        }
        if (table1.isFinished()) {
            m[0] = table1.getWhoWinInt();
        }
        if (table2.isFinished()) {
            m[1] = table2.getWhoWinInt();
        }
        if (table3.isFinished()) {
            m[2] = table3.getWhoWinInt();
        }
        getView(One, m[0]);
        getView(Two, m[1]);
        getView(Three, m[2]);
    }

    @Override
    protected void fillPaint() {
        super.fillPaint();
        cancel.setBackgroundResource(R.drawable.button_pressed);
        if (table.isCancel() && !table.isFinished() && (howManyFields < 3 || table.isMakeHisMove() || (!table1.isMakeHisMove() && !table2.isMakeHisMove() && !table3.isMakeHisMove()))) {
            cancel.setBackgroundResource(R.drawable.button);
        }
    }

    protected void isFinishedContinue() {
        if (isFinished) {
            if (winOne + winTwo == 30 || winOne == 20 || winTwo == 20) {
                WinText();
            }
        }
    }
}
