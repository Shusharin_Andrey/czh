package com.shusharin.czh.Field;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.czh.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.Interface.Help;
import com.shusharin.czh.MainActivity;

import java.util.Objects;

import static com.shusharin.czh.Field.Field.winSong;
import static com.shusharin.czh.MainActivity.buttonSong;
import static com.shusharin.czh.MainActivity.saveName;

public class Statistics extends AppCompatActivity {
    private static final String TAG = "Statistics";
    protected Table table1;
    protected Table table2;
    protected Table table3;
    protected int numberField;
    protected int numberPlayer;
    protected int winOne=0;
    protected int winTwo=0;
    protected int winThree=0;
    protected TextView[][] statistics = new TextView[3][6];
    protected int[][] statisticsInt = new int[3][6];
    protected Conservation conservation;
    protected boolean isLocal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_statistics);
        MainActivity.isNeedHideNavigationBar = true;
        Help.hideNavigationBar(getWindow(), Objects.requireNonNull(getSupportActionBar()));
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        TextView text = findViewById(R.id.tvTitle);
        text.setText("");

        isLocal = getIntent().getBooleanExtra(Field.IS_LOCAL, true);

        numberField = getIntent().getIntExtra(Conservation.NUMBER_FIELD, 1);
        numberPlayer = getIntent().getIntExtra(Conservation.NUMBER_PLAYER, 2);
        int time;
        if (isLocal) {
            winOne = getIntent().getIntExtra(Field.WIN_ONE, 0);
            winTwo = getIntent().getIntExtra(Field.WIN_TWO, 0);
            winThree = getIntent().getIntExtra(ThreePlayer.APP_SAVE_WIN_THREE, 0);
            table1 = getIntent().getParcelableExtra(getString(R.string.table) + 1);
            time = 0;
        } else {
            Field.whoOpen = 4;
            if (numberField == 1) {
                table1 = new TableOnline(getString(R.string.table) + 0, 0, this,true);
            } else {
                table1 = new TableOnline(getString(R.string.table) + 1, 1, this,true);
                table2 = new TableOnline(getString(R.string.table) + 2, 2, this,true);
                table3 = new TableOnline(getString(R.string.table) + 3, 3, this,true);
            }
            time = 2500;
            DatabaseReference databaseTable = FirebaseDatabase.getInstance().getReference(getString(R.string.Games)).child(saveName).child(getString(R.string.Field));

            DatabaseReference WIN_ONE = databaseTable.child(Field.WIN_ONE);
            DatabaseReference WIN_TWO = databaseTable.child(Field.WIN_TWO);
            DatabaseReference WIN_THREE = databaseTable.child(ThreePlayer.APP_SAVE_WIN_THREE);
            update(WIN_ONE, 0);
            update(WIN_TWO, 1);
            update(WIN_THREE, 2);
        }


        if (numberPlayer < 3) {
            findViewById(R.id.orange).setVisibility(View.GONE);
            findViewById(R.id.orange0).setVisibility(View.GONE);
            findViewById(R.id.orange1).setVisibility(View.GONE);
            findViewById(R.id.orange2).setVisibility(View.GONE);
            findViewById(R.id.orange3).setVisibility(View.GONE);
            findViewById(R.id.orange4).setVisibility(View.GONE);
            findViewById(R.id.orange5).setVisibility(View.GONE);
        }

        View[] SquareView0 = new View[9];
        View[] SquareView1 = new View[9];
        View[] SquareView2 = new View[9];

        SquareView0[0] = findViewById(R.id.square00);
        SquareView0[1] = findViewById(R.id.square01);
        SquareView0[2] = findViewById(R.id.square02);
        SquareView0[3] = findViewById(R.id.square10);
        SquareView0[4] = findViewById(R.id.square11);
        SquareView0[5] = findViewById(R.id.square12);
        SquareView0[6] = findViewById(R.id.square20);
        SquareView0[7] = findViewById(R.id.square21);
        SquareView0[8] = findViewById(R.id.square22);

        SquareView1[0] = findViewById(R.id.square03);
        SquareView1[1] = findViewById(R.id.square04);
        SquareView1[2] = findViewById(R.id.square05);
        SquareView1[3] = findViewById(R.id.square13);
        SquareView1[4] = findViewById(R.id.square14);
        SquareView1[5] = findViewById(R.id.square15);
        SquareView1[6] = findViewById(R.id.square23);
        SquareView1[7] = findViewById(R.id.square24);
        SquareView1[8] = findViewById(R.id.square25);

        SquareView2[0] = findViewById(R.id.square06);
        SquareView2[1] = findViewById(R.id.square07);
        SquareView2[2] = findViewById(R.id.square08);
        SquareView2[3] = findViewById(R.id.square16);
        SquareView2[4] = findViewById(R.id.square17);
        SquareView2[5] = findViewById(R.id.square18);
        SquareView2[6] = findViewById(R.id.square26);
        SquareView2[7] = findViewById(R.id.square27);
        SquareView2[8] = findViewById(R.id.square28);

        statistics[0][0] = findViewById(R.id.statistics00);
        statistics[0][1] = findViewById(R.id.statistics10);
        statistics[0][2] = findViewById(R.id.statistics20);
        statistics[0][3] = findViewById(R.id.statistics30);
        statistics[0][4] = findViewById(R.id.statistics40);
        statistics[0][5] = findViewById(R.id.statistics50);

        statistics[1][0] = findViewById(R.id.statistics01);
        statistics[1][1] = findViewById(R.id.statistics11);
        statistics[1][2] = findViewById(R.id.statistics21);
        statistics[1][3] = findViewById(R.id.statistics31);
        statistics[1][4] = findViewById(R.id.statistics41);
        statistics[1][5] = findViewById(R.id.statistics51);

        statistics[2][0] = findViewById(R.id.statistics02);
        statistics[2][1] = findViewById(R.id.statistics12);
        statistics[2][2] = findViewById(R.id.statistics22);
        statistics[2][3] = findViewById(R.id.statistics32);
        statistics[2][4] = findViewById(R.id.statistics42);
        statistics[2][5] = findViewById(R.id.statistics52);
        
        View table_0 = findViewById(R.id.table0);
        View table_2 = findViewById(R.id.table2);
        View table_3 = findViewById(R.id.table3);
        View table_4 = findViewById(R.id.table4);
        View table_5 = findViewById(R.id.table5);
        View table_6 = findViewById(R.id.table6);
        View table_7 = findViewById(R.id.table7);
        View table_8 = findViewById(R.id.table8);

        table_5.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
        table_6.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
        if (numberField == 1) {
            table_0.setVisibility(View.GONE);
            table_2.setVisibility(View.GONE);
            table_3.setVisibility(View.GONE);
            table_4.setVisibility(View.GONE);
            table_7.setVisibility(View.GONE);
            table_8.setVisibility(View.GONE);
        } else {
            if (isLocal) {
                table2 = getIntent().getParcelableExtra(getString(R.string.table) + 2);
                table3 = getIntent().getParcelableExtra(getString(R.string.table) + 3);
            }
            counting(table2);
            counting(table3);
            statistics[2][0].setText(String.valueOf(winThree * 1.0 / 10.0));
            table_3.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
            table_8.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            if (numberPlayer == 3) {
                table_4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
                table_7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
            }else{
                table_4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
                table_7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            }
        }

        new Handler().postDelayed(() -> {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            MainActivity.startPlay(winSong);
            if (numberField == 3) {
                for (int i = 0; i < 9; i++) {
                    SquareView0[i].setBackgroundResource(Field.getBackgroundResourceThin(table1.getBigSquareInt(i)));
                    SquareView1[i].setBackgroundResource(Field.getBackgroundResourceThin(table2.getBigSquareInt(i)));
                    SquareView2[i].setBackgroundResource(Field.getBackgroundResourceThin(table3.getBigSquareInt(i)));
                }
            } else {
                for (int i = 0; i < 9; i++) {
                    SquareView1[i].setBackgroundResource(Field.getBackgroundResourceThin(table1.getBigSquareInt(i)));
                }
            }
            conservation = getIntent().getParcelableExtra(Conservation.TAG);

            statistics[0][0].setText(String.valueOf(winOne * 1.0 / 10.0));
            statistics[1][0].setText(String.valueOf(winTwo * 1.0 / 10.0));
            counting(table1);

            View table_1 = findViewById(R.id.table1);
            table_0.setOnClickListener(arg0 -> {
                Intent intent = new Intent(this, FieldStatistics.class);
                intent.putExtra(getString(R.string.table), table1);
                intent.putExtra(Conservation.TAG, conservation);
                startActivity(intent);
                overridePendingTransition(0, 0);
            });
            table_1.setOnClickListener(arg0 -> {
                Intent intent = new Intent(this, FieldStatistics.class);
                if (numberField == 1) {
                    intent.putExtra(getString(R.string.table), table1);
                } else {
                    intent.putExtra(getString(R.string.table), table2);
                }
                intent.putExtra(Conservation.TAG, conservation);
                startActivity(intent);
                overridePendingTransition(0, 0);
            });
            table_2.setOnClickListener(arg0 -> {
                Intent intent = new Intent(this, FieldStatistics.class);
                intent.putExtra(getString(R.string.table), table3);
                intent.putExtra(Conservation.TAG, conservation);
                startActivity(intent);
                overridePendingTransition(0, 0);
            });

            TextView whoWin = findViewById(R.id.whoWin);
            String WinText;
            switch (numberPlayer) {
                case 1:
                case 2:
                    if (winOne > winTwo) {
                        WinText = getResources().getString(R.string.Win_blue_stroke);
                    } else if (winTwo > winOne) {
                        WinText = getResources().getString(R.string.Win_red_stroke);
                    } else {
                        WinText = getResources().getString(R.string.Draw);
                    }
                    break;
                default:
                    if (winOne > winTwo && winOne > winThree) {
                        WinText = getResources().getString(R.string.Win_blue_stroke);
                    } else if (winTwo > winOne && winTwo > winThree) {
                        WinText = getResources().getString(R.string.Win_red_stroke);
                    } else if (winThree > winOne && winThree > winTwo) {
                        WinText = getResources().getString(R.string.Win_orange_stroke);
                    } else
                        WinText = getResources().getString(R.string.Draw);
                    break;
            }
            whoWin.setText(WinText);
            for (int i = 1; i < 6; i++) {
                for (int j = 0; j < (Math.max(2, numberPlayer)); j++) {
                    statistics[j][i].setText(String.valueOf(statisticsInt[j][i]));
                }
            }
        }, time);
    }

    private void update(DatabaseReference databaseReference, int number) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (dataSnapshot.getValue() != null) {
                    switch (number) {
                        case 0:
                            winOne = dataSnapshot.getValue(Integer.class);
                            break;
                        case 1:
                            winTwo = dataSnapshot.getValue(Integer.class);
                            break;
                        case 2:
                            winThree = dataSnapshot.getValue(Integer.class);
                            break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, getString(R.string.fail_read), error.toException());
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MainActivity.startPlay(buttonSong);
            finish();
            overridePendingTransition(0, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private int convertNumberInId(int number) {
        switch (number) {
            case 0:
                return Table.ID_BLUE;
            case 1:
                return Table.ID_RED;
            case 2:
                return Table.ID_ORANGE;
            default:
                return -1;
        }
    }

    private void counting(Table table) {
        for (int number = 0; number < (Math.max(2, numberPlayer)); number++) {
            for (int i = 0; i < 9; i++) {
                if (table.getBigSquareInt(i) == convertNumberInId(number)) {
                    statisticsInt[number][1]++;
                }
                for (int j = 0; j < 9; j++) {
                    if (table.getSmallSquare(i, j) == convertNumberInId(number)) {
                        statisticsInt[number][2]++;
                    }
                    if (table.getBigSquareInt(Table.whereDidPokeIt(i, j)) == convertNumberInId(number)) {
                        if (table.getSmallSquare(i, j) != Table.ID_SMALL_SQUARE_DRAW &&
                                table.getSmallSquare(i, j) != convertNumberInId(number) &&
                                table.getSmallSquare(i, j) != Table.ID_EMPTY) {
                            statisticsInt[number][3]++;
                        }
                    } else if (table.getBigSquareInt(Table.whereDidPokeIt(i, j)) == Table.ID_BIG_SQUARE_DRAW) {
                        if (table.getSmallSquare(i, j) == convertNumberInId(number)) {
                            statisticsInt[number][5]++;
                        }
                    } else if (table.getBigSquareInt(Table.whereDidPokeIt(i, j)) != Table.ID_EMPTY) {
                        if (table.getSmallSquare(i, j) == convertNumberInId(number)) {
                            statisticsInt[number][4]++;
                        }
                    }
                }
            }
        }
    }
}
