package com.shusharin.czh.Field;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

public class Table implements Parcelable {
    static final int ID_EMPTY = 0;
    static final int ID_BLUE = 1;
    static final int ID_RED = 2;
    static final int ID_BIG_SQUARE_DRAW = 3;
    static final int ID_SMALL_SQUARE_DRAW = 4;
    static final int ID_ORANGE = 5;
    static final String APP_SAVE_IS_FIRST = "IS_FIRST";
    static final String APP_SAVE_IS_SHOW = "IS_SHOW";
    static final String APP_SAVE_IS_FINISHED = "IS_FINISHED";
    static final String APP_SAVE_IS_CANCEL = "IS_CANCEL";
    static final String APP_SAVE_SMALL_SQUARE = "SMALL_SQUARE";
    public static final String APP_SAVE_BIG_SQUARE_INT = "BIG_SQUARE_INT";
    static final String APP_SAVE_CANCEL_I = "CANCEL_I";
    static final String APP_SAVE_CANCEL_J = "CANCEL_J";
    static final String APP_SAVE_HOW_SHOW = "HOW_SHOW";
    static final String APP_SAVE_WHERE_TO_POKE = "WHERE_TO_POKE";
    static final String APP_SAVE_POKE_IT = "POKE_IT";
    static final String APP_SAVE_CANCEL_WHERE_TO_POKE = "CANCEL_WHERE_TO_POKE";
    static final String APP_SAVE_WHAT_TO_PAINT_OVER_BIG = "WHAT_TO_PAINT_OVER_BIG";
    static final String APP_SAVE_WHO_WIN_INT = "WHO_WIN_INT";
    static final String APP_SAVE_WHO_WIN = "WHO_WIN";
    static final String APP_SAVE_IS_MAKE_HIS_MOVE = "IS_MAKE_HIS_MOVE";
    static final String APP_SAVE_PREVIOUS = "PREVIOUS";
    static final String APP_SAVE_CANCEL_BIG_SQUARE = "CANCEL_BIG_SQUARE";
    static final String APP_SAVE_CANCEL_PREVIOUS = "CANCEL_PREVIOUS";
    protected boolean isFirst = true;
    protected boolean isShow;
    protected boolean isFinished;
    protected boolean isCancel;
    protected int[][] smallSquare = new int[9][9];
    protected int[] bigSquareInt = new int[9];
    protected int cancelI;
    protected int cancelJ;
    protected int howShow;
    protected int whereToPoke = 4;
    protected int pokeIt;
    protected int cancelWhereToPoke;
    protected int whatToPaintOverBig;
    int whoWinInt;
    View previous;
    View cancelBigSquare;
    View cancelPrevious;
    String whoWin;
    boolean isMakeHisMove;

    Table() {
    }

    private Table(Parcel in) {
        isFirst = in.readByte() != 0;
        isShow = in.readByte() != 0;
        isFinished = in.readByte() != 0;
        isCancel = in.readByte() != 0;
        bigSquareInt = in.createIntArray();
        for (int i = 0; i < 9; i++) {
            smallSquare[i] = in.createIntArray();
        }
        cancelI = in.readInt();
        cancelJ = in.readInt();
        howShow = in.readInt();
        whereToPoke = in.readInt();
        pokeIt = in.readInt();
        cancelWhereToPoke = in.readInt();
        whatToPaintOverBig = in.readInt();
        whoWinInt = in.readInt();
        whoWin = in.readString();
        isMakeHisMove = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isFirst ? 1 : 0));
        dest.writeByte((byte) (isShow ? 1 : 0));
        dest.writeByte((byte) (isFinished ? 1 : 0));
        dest.writeByte((byte) (isCancel ? 1 : 0));
        dest.writeIntArray(bigSquareInt);
        for (int i = 0; i < 9; i++) {
            dest.writeIntArray(smallSquare[i]);
        }
        dest.writeInt(cancelI);
        dest.writeInt(cancelJ);
        dest.writeInt(howShow);
        dest.writeInt(whereToPoke);
        dest.writeInt(pokeIt);
        dest.writeInt(cancelWhereToPoke);
        dest.writeInt(whatToPaintOverBig);
        dest.writeInt(whoWinInt);
        dest.writeString(whoWin);
        dest.writeByte((byte) (isMakeHisMove ? 1 : 0));
    }

    public static final Creator<Table> CREATOR = new Creator<Table>() {
        @Override
        public Table createFromParcel(Parcel in) {
            return new Table(in);
        }

        @Override
        public Table[] newArray(int size) {
            return new Table[size];
        }
    };


    View getPrevious() {
        return previous;
    }

    void setPrevious(View previous) {
        this.previous = previous;
    }

    View getCancelBigSquare() {
        return cancelBigSquare;
    }

    void setCancelBigSquare(View cancelBigSquare) {
        this.cancelBigSquare = cancelBigSquare;
    }

    View getCancelPrevious() {
        return cancelPrevious;
    }

    void setCancelPrevious(View cancelPrevious) {
        this.cancelPrevious = cancelPrevious;
    }

    String getWhoWin() {
        if (whoWin != null) {
            return whoWin;
        } else {
            return "";
        }
    }

    void setWhoWin(String whoWin) {
        this.whoWin = whoWin;
    }

    boolean isMakeHisMove() {
        return isMakeHisMove;
    }

    void setMakeHisMove(boolean makeHisMove) {
        isMakeHisMove = makeHisMove;
    }

    int getWhoWinInt() {
        return whoWinInt;
    }

    void setWhoWinInt(int whoWinInt) {
        this.whoWinInt = whoWinInt;
    }

    private boolean isHaveBoth(int one, int two, int three) {
        int[] m = new int[6];
        m[one]++;
        m[two]++;
        m[three]++;
        int k = 0;
        if (m[ID_BLUE] != 0) {
            k++;
        }
        if (m[ID_RED] != 0) {
            k++;
        }
        if (m[ID_BIG_SQUARE_DRAW] != 0) {
            k += 2;
        }
        if (m[ID_ORANGE] != 0) {
            k++;
        }
        return k >= 2;
    }

    private int checkMiddleCell(int[] m, boolean isBig) {
        //0 1 2
        //3 4 5
        //6 7 8
        if (m[0] != ID_EMPTY && m[0] != ID_BIG_SQUARE_DRAW) {
            if (m[0] == m[1] && m[0] == m[2]) {
                return m[0];
            } else if (m[0] == m[3] && m[0] == m[6]) {
                return m[0];
            } else if (m[0] == m[4] && m[0] == m[8]) {
                return m[0];
            }
        }
        if (m[4] != ID_EMPTY && m[4] != ID_BIG_SQUARE_DRAW) {
            if (m[3] == m[4] && m[3] == m[5]) {
                return m[3];
            } else if (m[1] == m[4] && m[1] == m[7]) {
                return m[1];
            } else if (m[2] == m[4] && m[2] == m[6]) {
                return m[2];
            }
        }
        if (m[8] != ID_EMPTY && m[8] != ID_BIG_SQUARE_DRAW) {
            if (m[6] == m[7] && m[6] == m[8]) {
                return m[6];
            } else if (m[2] == m[5] && m[2] == m[8]) {
                return m[2];
            }
        }
        if (m[0] * m[1] * m[2] * m[3] * m[4] * m[5] * m[6] * m[7] * m[8] != ID_EMPTY ||
                (isHaveBoth(m[0], m[1], m[2]) &&
                        isHaveBoth(m[3], m[4], m[5]) &&
                        isHaveBoth(m[6], m[7], m[8]) &&
                        isHaveBoth(m[0], m[3], m[6]) &&
                        isHaveBoth(m[1], m[4], m[7]) &&
                        isHaveBoth(m[2], m[5], m[8]) &&
                        isHaveBoth(m[0], m[4], m[8]) &&
                        isHaveBoth(m[2], m[4], m[6]) &&
                        isBig)
        ) {
            return ID_BIG_SQUARE_DRAW;
        }
        return 0;
    }

    static int whereDrawFrame(int i, int j) {
        i %= 3;
        j %= 3;
        return getK(i, j);
    }

    static int whereDidPokeIt(int i, int j) {
        i /= 3;
        j /= 3;
        return getK(i, j);
    }

    private static int getK(int i, int j) {
        int k;
        if (i == 0 && j == 0) {
            k = 0;
        } else if (i == 0 && j == 1) {
            k = 1;
        } else if (i == 0) {
            k = 2;
        } else if (i == 1 && j == 0) {
            k = 3;
        } else if (i == 1 && j == 1) {
            k = 4;
        } else if (i == 1) {
            k = 5;
        } else if (j == 0) {
            k = 6;
        } else if (j == 1) {
            k = 7;
        } else {
            k = 8;
        }
        return k;
    }

    boolean eraseOrFillBigSquare(int i, int j, boolean isFill) {
        setPokeIt(whereDidPokeIt(i, j));
        int x = pokeIt % 3;
        int y = pokeIt / 3;
        int[] m = {smallSquare[y * 3][x * 3],
                smallSquare[y * 3][1 + x * 3],
                smallSquare[y * 3][2 + x * 3],
                smallSquare[1 + y * 3][x * 3],
                smallSquare[1 + y * 3][1 + x * 3],
                smallSquare[1 + y * 3][2 + x * 3],
                smallSquare[2 + y * 3][x * 3],
                smallSquare[2 + y * 3][1 + x * 3],
                smallSquare[2 + y * 3][2 + x * 3]};
        setWhatToPaintOverBig(checkMiddleCell(m, false));
        int q;
        int w;
        if (!isFill) {
            invertFirst();
            setWhereToPoke(cancelWhereToPoke);
            setPrevious(cancelPrevious);
            setCancel(false);
            setSmallSquare(cancelI, cancelJ, ID_EMPTY);
        }
        if (whatToPaintOverBig != 0) {
            if (isFill) {
                setHowShow(howShow + 1);
                setBigSquareInt(pokeIt, whatToPaintOverBig);
                q = ID_EMPTY;
                w = ID_SMALL_SQUARE_DRAW;
            } else {
                setHowShow(howShow - 1);
                setBigSquareInt(pokeIt, ID_EMPTY);
                q = ID_SMALL_SQUARE_DRAW;
                w = ID_EMPTY;
            }
            for (int l = 0; l < 3; l++) {
                for (int n = 0; n < 3; n++) {
                    if (smallSquare[l + y * 3][n + x * 3] == q) {
                        setSmallSquare(l + y * 3, n + x * 3, w);
                    }
                }
            }
            return true;
        }
        return false;
    }

    int getWhatToPaintOverBig() {
        return whatToPaintOverBig;
    }

    boolean isFirst() {
        return isFirst;
    }

    void invertFirst() {
        setFirst(!isFirst);
    }

    void setFirst(boolean First) {
        isFirst = First;
    }

    boolean isShow() {
        return isShow;
    }

    void setShow(boolean show) {
        isShow = show;
    }

    void invertShow() {
        setShow(!isShow);
    }

    boolean isFinished() {
        return isFinished;
    }

    void setFinished() {
        setFinished(true);
    }

    void setFinished(Boolean Finished) {
        isFinished = Finished;
    }

    boolean isCancel() {
        return isCancel;
    }

    void setCancel(boolean cancel) {
        isCancel = cancel;
    }

    int getSmallSquare(int i, int j) {
        return smallSquare[i][j];
    }

    void setSmallSquare(int i, int j, int k) {
        this.smallSquare[i][j] = k;
    }

    int getBigSquareInt(int i) {
        return bigSquareInt[i];
    }

    void setBigSquareInt(int i, int k) {
        bigSquareInt[i] = k;
    }

    int getCancelI() {
        return cancelI;
    }

    void setCancelI(int cancelI) {
        this.cancelI = cancelI;
    }

    int getCancelJ() {
        return cancelJ;
    }

    void setCancelJ(int cancelJ) {
        this.cancelJ = cancelJ;
    }

    int getHowShow() {
        return howShow;
    }

    int getWhereToPoke() {
        return whereToPoke;
    }

    boolean beFinal() {
        return (checkMiddleCell(bigSquareInt, true) != 0);
    }

    void setWhereToPoke(int whereToPoke) {
        this.whereToPoke = whereToPoke;
    }

    int getPokeIt() {
        return pokeIt;
    }


    int getCancelWhereToPoke() {
        return cancelWhereToPoke;
    }

    void setCancelWhereToPoke(int cancelWhereToPoke) {
        this.cancelWhereToPoke = cancelWhereToPoke;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    void setPokeIt(int anInt) {
        pokeIt = anInt;
    }

    void setHowShow(int anInt) {
        howShow = anInt;
    }

    void setWhatToPaintOverBig(int anInt) {
        whatToPaintOverBig = anInt;
    }

    public static int getK(int[] ij) {
        return whereDidPokeIt(ij[0], ij[1]);
    }

    public static int[] get_i_j(int number) {
        return new int[]{number / 9, number % 9};
    }

    static int get_number(int i, int j) {
        return i * 9 + j;
    }

    void stop() {
    }
}
