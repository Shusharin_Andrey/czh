package com.shusharin.czh.Field;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.czh.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.MainActivity;

import static com.shusharin.czh.Field.FieldOnline.APP_SAVE_HAS_SENT_NOTIFICATION;
import static com.shusharin.czh.MainActivity.displayName;
import static com.shusharin.czh.MainActivity.myResources;
import static com.shusharin.czh.MainActivity.saveName;

public class FieldThreeOnline extends FieldThree {
    private static final String TAG = "FieldThreeOnline";
    public static String APP_SAVE_ID_PLAYER = "ID_PLAYER";
    // Идентификатор уведомления
    private static final int NOTIFY_ID = 1;
    private DatabaseReference DATABASE_HOW_MAKE_HIS_MOVE;
    private DatabaseReference DATABASE_HOW_MANE_FIELD;
    private DatabaseReference DATABASE_COUNTER;
    private DatabaseReference DATABASE_WIN_ONE;
    private DatabaseReference DATABASE_WIN_TWO;
    private DatabaseReference DATABASE_HAS_SENT_NOTIFICATION;
    private DatabaseReference DATABASE_COUNTER_STROKE;
    private DatabaseReference DATABASE_IS_PRESENT_FIRST_PLAYER;
    private DatabaseReference DATABASE_IS_PRESENT_SECOND_PLAYER;

    private DatabaseReference reference;

    private ValueEventListener LISTENER_HAS_SENT_NOTIFICATION;
    private ValueEventListener LISTENER_IS_FIND;
    private ValueEventListener LISTENER_IS_PRESENT_FIRST_PLAYER;
    private ValueEventListener LISTENER_IS_PRESENT_SECOND_PLAYER;
    private ValueEventListener LISTENER_HOW_MAKE_HIS_MOVE;
    private ValueEventListener LISTENER_HOW_MANE_FIELD;
    private ValueEventListener LISTENER_COUNTER;
    private ValueEventListener LISTENER_WIN_ONE;
    private ValueEventListener LISTENER_WIN_TWO;
    private ValueEventListener LISTENER_FRAME;
    private ValueEventListener LISTENER_COUNTER_STROKE;

    protected void isFinishedContinue() {

    }

    public void setHowMakeHisMove(int howMakeHisMove) {
        this.howMakeHisMove = howMakeHisMove;
        DATABASE_HOW_MAKE_HIS_MOVE.setValue(howMakeHisMove);
    }

    public void setHowManyFields(int howManyFields) {
        this.howManyFields = howManyFields;
        DATABASE_HOW_MANE_FIELD.setValue(howManyFields);
    }

    public void setCounter(int counter) {
        this.counter = counter;
        DATABASE_COUNTER.setValue(counter);
    }

    public void setWinOne(int winOne) {
        this.winOne = winOne;
        DATABASE_WIN_ONE.setValue(winOne);
    }

    public void setWinTwo(int winTwo) {
        this.winTwo = winTwo;
        DATABASE_WIN_TWO.setValue(winTwo);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (conservation.isFinished()) {
            WinText();
        }
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference(getString(R.string.Games));
        DatabaseReference databaseUser = reference.child(saveName).child(getString(R.string.Field)).child(displayName);
        LISTENER_IS_FIND = isFind(databaseUser, APP_SAVE_ID_PLAYER);

        DATABASE_COUNTER_STROKE = databaseUser.child(FieldOnline.APP_SAVE_COUNTER_STROKE);
        LISTENER_COUNTER_STROKE = update(DATABASE_COUNTER_STROKE,10);

        DatabaseReference databaseTable = reference.child(saveName).child(getString(R.string.Field));

        DATABASE_HOW_MAKE_HIS_MOVE = databaseTable.child(APP_SAVE_HOW_MAKE_HIS_MOVE);
        LISTENER_HOW_MAKE_HIS_MOVE = update(DATABASE_HOW_MAKE_HIS_MOVE, 0);

        DATABASE_HOW_MANE_FIELD = databaseTable.child(APP_SAVE_HOW_MANE_FIELD);
        LISTENER_HOW_MANE_FIELD = update(DATABASE_HOW_MANE_FIELD, 7);

        DATABASE_COUNTER = databaseTable.child(COUNTER);
        LISTENER_COUNTER = update(DATABASE_COUNTER, 8);

        DATABASE_WIN_ONE = databaseTable.child(WIN_ONE);
        LISTENER_WIN_ONE = update(DATABASE_WIN_ONE, 3);

        DATABASE_WIN_TWO = databaseTable.child(WIN_TWO);
        LISTENER_WIN_TWO = update(DATABASE_WIN_TWO, 4);

        LISTENER_FRAME = update(databaseTable, 9);

        DATABASE_HAS_SENT_NOTIFICATION = databaseTable.child(APP_SAVE_HAS_SENT_NOTIFICATION);
        LISTENER_HAS_SENT_NOTIFICATION = update(DATABASE_HAS_SENT_NOTIFICATION, 6);

        activitySave();
    }

    private ValueEventListener update(DatabaseReference databaseReference, int number) {
        return databaseReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("StringFormatMatches")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (dataSnapshot.getValue() != null) {
                    switch (number) {
                        case 0:
                            howMakeHisMove = dataSnapshot.getValue(Integer.class);
                            break;
                        case 7:
                            howManyFields = dataSnapshot.getValue(Integer.class);
                            break;
                        case 8:
                            counter = dataSnapshot.getValue(Integer.class);
                            break;
                        case 3:
                            winOne = dataSnapshot.getValue(Integer.class);
                            break;
                        case 4:
                            winTwo = dataSnapshot.getValue(Integer.class);
                            break;
                        case 9:
                            checkFrameTable();
                            break;
                        case 6:
                            if (dataSnapshot.getValue(boolean.class)) {
                                if ((table.isFirst() && idPlayer == 1) || (!table.isFirst() && idPlayer == 2)) {
                                    setNumberStroke();
                                    showMessage(getString(R.string.notification_of_your_turn, numberStroke));
                                    MainActivity.startPlay(MainActivity.notification);
                                    DATABASE_HAS_SENT_NOTIFICATION.setValue(false);
                                    ShowNotification(conservation.getName());
                                }
                                showMotion();
                            }
                            break;
                        case 1:
                        case 2:
                            String numberPlayer;
                            String isJoned;
                            if (number != idPlayer) {
                                if (number == 1) {
                                    numberPlayer = conservation.getNameOnePlayer();
                                } else {
                                    numberPlayer = conservation.getNameTwoPlayer();
                                }
                                if (dataSnapshot.getValue(boolean.class)) {
                                    isJoned = myResources.getString(R.string.joined);
                                } else {
                                    isJoned = getString(R.string.came_out);
                                }
                                Toast.makeText(getBaseContext(), numberPlayer + isJoned, Toast.LENGTH_SHORT).show();
                                MainActivity.startPlay(MainActivity.notification);
                            }
                            break;
                        case 10:
                            numberStroke = dataSnapshot.getValue(Integer.class);
                            showMotion();
                            DATABASE_COUNTER_STROKE.removeEventListener(LISTENER_COUNTER_STROKE);
                            break;
                    }
                } else {
                    if (number == 9) {
                        checkFrameTable();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, getString(R.string.fail_read), error.toException());
            }
        });
    }

    protected void setNumberStroke() {
        super.setNumberStroke();
        DATABASE_COUNTER_STROKE.setValue(numberStroke);
    }

    private ValueEventListener isFind(DatabaseReference databaseReference, String children) {
        return databaseReference.child(children).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Integer.class) != null) {
                    idPlayer = dataSnapshot.getValue(Integer.class);
                    sendPresent(true);
                    findUsers();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, getString(R.string.fail_read), error.toException());
            }
        });
    }

    private void findUsers() {
        if (reference != null) {
            if (LISTENER_IS_FIND != null) {
                reference.removeEventListener(LISTENER_IS_FIND);
            }
            DatabaseReference database = reference.child(saveName).child(getString(R.string.Field));
            DATABASE_IS_PRESENT_FIRST_PLAYER = database.child("isPresent" + Conservation.ONE_PLAYER);
            DATABASE_IS_PRESENT_SECOND_PLAYER = database.child("isPresent" + Conservation.TWO_PLAYER);
            LISTENER_IS_PRESENT_FIRST_PLAYER = update(DATABASE_IS_PRESENT_FIRST_PLAYER, 1);
            LISTENER_IS_PRESENT_SECOND_PLAYER = update(DATABASE_IS_PRESENT_SECOND_PLAYER, 2);
        }
    }

    void nextMove() {
    }

    private void sendPresent(boolean present) {
        String numberPlayer;
        if (idPlayer == 1) {
            numberPlayer = Conservation.ONE_PLAYER;
        } else if (idPlayer == 2) {
            numberPlayer = Conservation.TWO_PLAYER;
        } else {
            numberPlayer = Conservation.THREE_PLAYER;
        }
        reference.child(saveName).child(getString(R.string.Field)).child("isPresent" + numberPlayer).setValue(present);
    }

    protected void WinText() {
        if (!isWin) {
            isWin = true;
            FirebaseDatabase.getInstance().getReference(getString(R.string.name_game)).child(conservation.getName()).child(getString(R.string.finished)).setValue(true);
            table1.setFinished();
            table2.setFinished();
            table3.setFinished();
            setWhoWin(View.VISIBLE);
            Intent intent = new Intent(this, Statistics.class);
            intent.putExtra(Conservation.NUMBER_FIELD, 3);
            intent.putExtra(Conservation.NUMBER_PLAYER, 2);
            intent.putExtra(WIN_ONE, winOne);
            intent.putExtra(WIN_TWO, winTwo);
            intent.putExtra(IS_LOCAL, false);
            intent.putExtra(Conservation.TAG, conservation);
            saveName = conservation.getName();
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        }
    }

    @Override
    protected void createTable() {
        whoOpen = 1;
        table1 = new TableOnline(getString(R.string.table) + 1, 1, this);
        table2 = new TableOnline(getString(R.string.table) + 2, 2, this);
        table3 = new TableOnline(getString(R.string.table) + 3, 3, this);
        table = table1;
    }

    protected void check(int i, int j, View view) {
        if (idPlayer == (table.isFirst() ? 1 : 2)) {
            if (table.getSmallSquare(i, j) == Table.ID_EMPTY
                    && (table.getWhereToPoke() == Table.whereDidPokeIt(i, j)
                    || table.getBigSquareInt(table.getWhereToPoke()) != Table.ID_EMPTY)
                    && !table.isFinished()) {
                super.check(i, j, view);
            }
        }
    }

    void getData() {

    }

    protected void saveData() {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void sendNotification() {
        setHasSentNotification();
    }

    public void setHasSentNotification() {
        DATABASE_HAS_SENT_NOTIFICATION.setValue(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sendPresent(false);
        table.stop();
        table1.stop();
        table2.stop();
        table3.stop();
        if (reference != null) {
            if (LISTENER_HAS_SENT_NOTIFICATION != null) {
                DATABASE_HAS_SENT_NOTIFICATION.removeEventListener(LISTENER_HAS_SENT_NOTIFICATION);
            }
            if (LISTENER_IS_PRESENT_FIRST_PLAYER != null) {
                reference.removeEventListener(LISTENER_IS_PRESENT_FIRST_PLAYER);
            }
            if (LISTENER_IS_PRESENT_FIRST_PLAYER != null) {
                DATABASE_IS_PRESENT_FIRST_PLAYER.removeEventListener(LISTENER_IS_PRESENT_FIRST_PLAYER);
            }
            if (LISTENER_IS_PRESENT_SECOND_PLAYER != null) {
                DATABASE_IS_PRESENT_SECOND_PLAYER.removeEventListener(LISTENER_IS_PRESENT_SECOND_PLAYER);
            }
            if (LISTENER_HOW_MAKE_HIS_MOVE != null) {
                DATABASE_HOW_MAKE_HIS_MOVE.removeEventListener(LISTENER_HOW_MAKE_HIS_MOVE);
            }
            if (LISTENER_HOW_MANE_FIELD != null) {
                DATABASE_HOW_MANE_FIELD.removeEventListener(LISTENER_HOW_MANE_FIELD);
            }
            if (LISTENER_COUNTER != null) {
                DATABASE_COUNTER.removeEventListener(LISTENER_COUNTER);
            }
            if (LISTENER_WIN_ONE != null) {
                DATABASE_WIN_ONE.removeEventListener(LISTENER_WIN_ONE);
            }
            if (LISTENER_WIN_TWO != null) {
                DATABASE_WIN_TWO.removeEventListener(LISTENER_WIN_TWO);
            }
            if (LISTENER_FRAME != null) {
                reference.removeEventListener(LISTENER_FRAME);
            }
        }
    }
}
