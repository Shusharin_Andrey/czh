package com.shusharin.czh;

public class SquareDesign {
    private final String name;
    private final int designSmallBlue;
    private final int designSmallRed;
    private final int designSmallOrange;
    private final int designSmallViolet;
    private final int designBigBlue;
    private final int designBigRed;
    private final int designBigOrange;
    private final int designBigViolet;
    private final int designWhite;
    private final int designTransperent;

    public SquareDesign(String name, int designSmallBlue, int designSmallRed, int designSmallOrange, int designSmallViolet, int designBigBlue, int designBigRed, int designBigOrange, int designBigViolet, int designWhite, int designTransperent) {
        this.name = name;
        this.designSmallBlue = designSmallBlue;
        this.designSmallRed = designSmallRed;
        this.designSmallOrange = designSmallOrange;
        this.designSmallViolet = designSmallViolet;
        this.designBigBlue = designBigBlue;
        this.designBigRed = designBigRed;
        this.designBigOrange = designBigOrange;
        this.designBigViolet = designBigViolet;
        this.designWhite = designWhite;
        this.designTransperent = designTransperent;
    }

    public String getName() {
        return name;
    }

    public int getDesignSmallBlue() {
        return designSmallBlue;
    }

    public int getDesignSmallRed() {
        return designSmallRed;
    }

    public int getDesignSmallOrange() {
        return designSmallOrange;
    }

    public int getDesignSmallViolet() {
        return designSmallViolet;
    }

    public int getDesignBigBlue() {
        return designBigBlue;
    }

    public int getDesignBigRed() {
        return designBigRed;
    }

    public int getDesignBigOrange() {
        return designBigOrange;
    }

    public int getDesignBigViolet() {
        return designBigViolet;
    }

    public int getDesignWhite() {
        return designWhite;
    }

    public int getDesignTransperent() {
        return designTransperent;
    }
}
