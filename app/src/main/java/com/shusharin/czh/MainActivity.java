package com.shusharin.czh;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.czh.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.shusharin.czh.Field.Field;
import com.shusharin.czh.Interface.EmailPasswordActivity;
import com.shusharin.czh.Interface.Help;
import com.shusharin.czh.Interface.SettingsActivity;
import com.shusharin.czh.SelectionGames.ChoiceContinue;
import com.shusharin.czh.SelectionGames.NewGame;

import java.util.Objects;

import static com.shusharin.czh.SelectionGames.Continue.conservations;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener {
    public static final String NUMBER_DESIGN = "NUMBER_DESIGN";
    public static boolean isContinue;
    public static MediaPlayer buttonSong;
    public static MediaPlayer buttonCloseSong;
    public static MediaPlayer notification;
    protected Button play;
    protected Button continueB;
    protected View help;
    protected boolean isPressed;
    public static String saveName = "";
    public static boolean isNeedHideNavigationBar = true;
    protected TextView hello;
    protected View label;
    protected View background2;
    public static FirebaseUser currentUser;
    public static String displayName;
    private static boolean isFirstStart = true;
    private boolean isAnimate;
    public static Resources myResources;
    public static SquareDesign[] squareDesigns = new SquareDesign[3];
    public static int numberDesign = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isAnimate = true;
        play = findViewById(R.id.play);
        continueB = findViewById(R.id.continueB);
        help = findViewById(R.id.help);
        hello = findViewById(R.id.view);
        label = findViewById(R.id.label);
        background2 = findViewById(R.id.background2);
        play.setOnClickListener(this);
        continueB.setOnClickListener(this);
        help.setOnClickListener(this);
        findViewById(R.id.Profile).setOnClickListener(this);
        findViewById(R.id.Settings).setOnClickListener(this);
        hideNavigationBar(getWindow());
        conservations.clear();
        buttonSong = MediaPlayer.create(this, R.raw.button);
        buttonCloseSong = MediaPlayer.create(this, R.raw.close);
        notification = MediaPlayer.create(this, R.raw.notification);
        buttonSong.setOnCompletionListener(mp -> stopPlay(buttonSong));
        buttonCloseSong.setOnCompletionListener(mp -> stopPlay(buttonCloseSong));
        notification.setOnCompletionListener(mp -> stopPlay(notification));
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences.contains(Conservation.SIZE)) {
            for (int i = 0; i < sharedPreferences.getInt(Conservation.SIZE, conservations.size()); i++) {
                conservations.add(new Conservation(
                        sharedPreferences.getInt(Conservation.NUMBER_PLAYER + i, 0),
                        sharedPreferences.getInt(Conservation.NUMBER_FIELD + i, 0),
                        sharedPreferences.getString(getString(R.string.Game) + i, ""),
                        sharedPreferences.getInt(Conservation.NUMBER_NAME + i, 0))
                );
                conservations.get(i).setFinished(sharedPreferences.getBoolean(Conservation.IS_FINISHED + i, false));
                conservations.get(i).setFirst(sharedPreferences.getBoolean(Conservation.IS_FIRST + i, false));
                conservations.get(i).setNameOnePlayer(sharedPreferences.getString(Conservation.ONE_PLAYER + i, conservations.get(i).getNameOnePlayer()));
                conservations.get(i).setNameTwoPlayer(sharedPreferences.getString(Conservation.TWO_PLAYER + i, conservations.get(i).getNameTwoPlayer()));
                conservations.get(i).setNameThreePlayer(sharedPreferences.getString(Conservation.THREE_PLAYER + i, conservations.get(i).getNameThreePlayer()));
            }
        }
        if (sharedPreferences.contains(NUMBER_DESIGN)) {
            numberDesign= sharedPreferences.getInt(NUMBER_DESIGN,0);
            SettingsActivity.isSong= sharedPreferences.getBoolean(SettingsActivity.IS_SONG,true);
        }
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            displayName = Objects.requireNonNull(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getDisplayName());
        } else {
            displayName = getResources().getString(R.string.person);
        }
        squareDesigns[0] = new SquareDesign(getResources().getString(R.string.nameBase), R.drawable.blue_thin, R.drawable.red_thin, R.drawable.color_three_thin, R.drawable.violet_thin,
                R.drawable.blue_thick, R.drawable.red_thick, R.drawable.color_three_thick, R.drawable.violet_thick,
                R.drawable.white_thin, R.drawable.transperent_thick);
        squareDesigns[1] = new SquareDesign(getResources().getString(R.string.name_cross_zero), R.drawable.cross, R.drawable.zero, R.drawable.three, R.drawable.violet,
                R.drawable.cross, R.drawable.zero, R.drawable.three, R.drawable.violet,
                R.drawable.white, R.drawable.transperent);
        squareDesigns[2] = new SquareDesign(getResources().getString(R.string.nameAnimal), R.drawable.rabbit, R.drawable.cat, R.drawable.fox, R.drawable.monster,
                R.drawable.rabbit_big, R.drawable.cat_big, R.drawable.fox_big, R.drawable.dog_big,
                R.drawable.white, R.drawable.transperent_big);
        myResources = getResources();
        if (isFirstStart) {
            isFirstStart = false;
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.disappearance);
            new Handler().postDelayed(() -> {
                label.startAnimation(animation);
                background2.startAnimation(animation);
                new Handler().postDelayed(() -> {
                    label.setVisibility(View.GONE);
                    background2.setVisibility(View.GONE);
                    isAnimate = false;
                }, getResources().getInteger(R.integer.time_disappearance));
            }, 1000);
        } else {
            label.setVisibility(View.GONE);
            background2.setVisibility(View.GONE);
        }
    }

    private void stopPlay(MediaPlayer mediaPlayer) {
        mediaPlayer.stop();
        try {
            mediaPlayer.prepare();
            mediaPlayer.seekTo(0);
        } catch (Throwable t) {
            Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public static void startPlay(MediaPlayer mediaPlayer) {
        if (SettingsActivity.isSong) {
            mediaPlayer.start();
        }
    }
    public static void hideNavigationBar(final Window window) {
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        if (isNeedHideNavigationBar) {
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            View decorView = window.getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener
                    (visibility -> {
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            new Handler().postDelayed(() -> {
                                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
                            }, 3500);
                        }
                    });
        } else {
            NohideNavigationBar(window);
        }
    }

    public static void NohideNavigationBar(final Window window) {
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isNeedHideNavigationBar = true;
        play.setVisibility(View.VISIBLE);
        continueB.setVisibility(View.VISIBLE);
        help.setVisibility(View.VISIBLE);
        hello.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(Conservation.SIZE, conservations.size());
        edit.putInt(NUMBER_DESIGN, numberDesign);
        for (int i = 0; i < conservations.size(); i++) {
            edit.putString(getString(R.string.Game) + i, conservations.get(i).getNameNoNumber());
            edit.putInt(Conservation.NUMBER_NAME + i, conservations.get(i).getNumberName());
            edit.putInt(Conservation.NUMBER_PLAYER + i, conservations.get(i).getNumberPlayer());
            edit.putString(Conservation.ONE_PLAYER + i, conservations.get(i).getNameOnePlayer());
            edit.putString(Conservation.TWO_PLAYER + i, conservations.get(i).getNameTwoPlayer());
            edit.putString(Conservation.THREE_PLAYER + i, conservations.get(i).getNameThreePlayer());
            edit.putInt(Conservation.NUMBER_FIELD + i, conservations.get(i).getNumberField());
            edit.putBoolean(Conservation.IS_FINISHED + i, conservations.get(i).isFinished());
            edit.putBoolean(Conservation.IS_FIRST + i, conservations.get(i).isFirst());
        }
        edit.apply();
    }

    private void showMessage(String textInMessage) {
        Toast.makeText(getApplicationContext(), textInMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        if (!isPressed && !isAnimate) {
            startPlay(buttonSong);
            isPressed = true;
            Intent intent;
            switch (v.getId()) {
                case R.id.Profile:
                    intent = new Intent(MainActivity.this, EmailPasswordActivity.class);
                    break;
                case R.id.play:
                    isContinue = false;
                    intent = new Intent(MainActivity.this, NewGame.class);
                    break;
                case R.id.continueB:
                    isContinue = true;
                    intent = new Intent(MainActivity.this, ChoiceContinue.class);
                    intent.putExtra(Field.IS_LOAD, true);
                    break;
                case R.id.Settings:
                    intent = new Intent(MainActivity.this, SettingsActivity.class);
                    break;
                default:
                    intent = new Intent(MainActivity.this, Help.class);
                    break;
            }
            startActivity(intent);
            overridePendingTransition(0, 0);
            new Handler().postDelayed(() -> isPressed = false, 250);
        } else {
            startPlay(buttonCloseSong);
        }
    }
}
