package com.shusharin.czh.Interface;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.czh.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.shusharin.czh.SelectionGames.Continue;

public class Notification extends FirebaseMessagingService {
    private static final String TAG = "Notification";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        ShowNotification(remoteMessage.getData().get("NAME_GAME"));

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }


    @Override
    public void onDeletedMessages() {
        // In some situations, FCM may not deliver a message. This occurs when there are too many messages (>100) pending for your app on a particular device
        // at the time it connects or if the device hasn't connected to FCM in more than one month. In these cases, you may receive a callback
        // to FirebaseMessagingService.onDeletedMessages() When the app instance receives this callback, it should perform a full sync with your app server.
        // If you haven't sent a message to the app on that device within the last 4 weeks, FCM won't call onDeletedMessages().
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.emailpassword_status_name);
            String description = getString(R.string.anonim);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    void ShowNotification(String text) {

        Intent intent = new Intent(this, Continue.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        createNotificationChannel();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "CHANNEL_ID")
                .setSmallIcon(R.drawable.violet_thin)
                .setContentTitle("Напоминание")
                .setContentText("Ваш ход в партии \"" + text + "\"")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        int mId = 1001;
        try {
            notificationManager.notify(mId, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
