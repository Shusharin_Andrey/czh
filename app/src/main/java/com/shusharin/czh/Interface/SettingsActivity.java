package com.shusharin.czh.Interface;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.czh.R;
import com.shusharin.czh.MainActivity;
import com.shusharin.czh.SelectionGames.AdapterSpiner;

import java.util.Objects;

public class SettingsActivity extends AppCompatActivity implements
        View.OnClickListener {
    public static final String IS_SONG = "IS_SONG";
    public static boolean isSong = true;
    Switch song;
    protected Spinner spinner;
    protected AdapterSpiner adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        song = findViewById(R.id.song);
        song.setOnClickListener(this);
        song.setChecked(isSong);
        spinner = findViewById(R.id.spinner2);
        adapter = new AdapterSpiner(this);
        spinner.setAdapter(adapter);
        spinner.setSelection(MainActivity.numberDesign);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View itemSelected, int selectedItemPosition, long selectedId) {
                MainActivity.numberDesign = selectedItemPosition;
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
        openOptionsMenu();
        Help.hideNavigationBar(getWindow(), Objects.requireNonNull(getSupportActionBar()));
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        TextView text = findViewById(R.id.tvTitle);
        text.setText(R.string.title_activity_settings);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.song:
                isSong = !isSong;
        }
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MainActivity.startPlay(MainActivity.buttonSong);
            finish();
            overridePendingTransition(0, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(MainActivity.NUMBER_DESIGN, MainActivity.numberDesign);
        edit.putBoolean(IS_SONG, isSong);
        edit.apply();
    }
}