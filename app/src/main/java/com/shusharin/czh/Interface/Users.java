package com.shusharin.czh.Interface;

class Users {
    private String ID;
    private String email;
    private String name;

    Users(String ID, String email, String name) {
        this.ID = ID;
        this.email = email;
        this.name = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
