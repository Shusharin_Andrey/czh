package com.shusharin.czh.Interface;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.czh.R;
import com.shusharin.czh.MainActivity;

import java.text.MessageFormat;
import java.util.Objects;

public class Help extends AppCompatActivity {

    private TextView title;
    private TextView text;

    private static final String TITLE = "TITLE";
    private static final String TEXT = "TEXT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        openOptionsMenu();
        hideNavigationBar(getWindow(), Objects.requireNonNull(getSupportActionBar()));
        title = findViewById(R.id.title);
        text = findViewById(R.id.text);
        if (savedInstanceState != null) {
            title.setText(savedInstanceState.getCharSequence(TITLE));
            text.setText(savedInstanceState.getCharSequence(TEXT));
        }
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        TextView text = findViewById(R.id.tvTitle);
        text.setText(R.string.title_activity_help);
    }

    public static void hideNavigationBar(@NonNull final Window window, @NonNull ActionBar actionBar) {
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        View decorView = window.getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (visibility -> {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        new Handler().postDelayed(() -> window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_IMMERSIVE
                                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE), 3500);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.help, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Move_rules:
                MainActivity.startPlay(MainActivity.buttonSong);
                title.setText(R.string.Move_rules);
                text.setText(R.string.Move_rules_Text);
                return true;
            case R.id.Condition_of_victory:
                MainActivity.startPlay(MainActivity.buttonSong);
                title.setText(R.string.Condition_of_victory);
                text.setText(MessageFormat.format("{0}\n\n{1}\n\n{2}\n\n{3}\n\n{4}\n\n{5}",
                        getText(R.string.Condition_of_victory_in_the_subfield),
                        getString(R.string.Condition_of_victory_in_the_subfield_Text),
                        getString(R.string.Condition_of_victory_in_the_field),
                        getString(R.string.Condition_of_victory_in_the_field_Text),
                        getString(R.string.Condition_of_victory_in_the_three_field),
                        getString(R.string.Condition_of_victory_in_the_three_field_Text)));
                return true;
            case R.id.Two_player:
                MainActivity.startPlay(MainActivity.buttonSong);
                title.setText(R.string.Two_player);
                text.setText(R.string.Two_player_text);
                return true;
            case R.id.Three_player:
                MainActivity.startPlay(MainActivity.buttonSong);
                title.setText(R.string.Three_player);
                text.setText(R.string.Three_player_text);
                return true;
            case android.R.id.home:
                MainActivity.startPlay(MainActivity.buttonSong);
                finish();
                overridePendingTransition(0, 0);
                return true;
            default:
                MainActivity.startPlay(MainActivity.buttonSong);
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence(TITLE, title.getText());
        outState.putCharSequence(TEXT, text.getText());
    }
}
