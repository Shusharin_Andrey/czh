package com.shusharin.czh.Interface;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.czh.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthMultiFactorException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.MultiFactorResolver;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.shusharin.czh.MainActivity;

import java.util.Objects;

public class EmailPasswordActivity extends BaseActivity implements
        View.OnClickListener {

    public static final int RESULT_NEEDS_MFA_SIGN_IN = 42;

    private static final String TAG = "EmailPassword";
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String displayName = "";
    private static int ID_NOW = 0;
    private static final int ID_SIGN = 0;
    private static final int ID_SIGNED = 2;
    private static final int ID_CREATE = 1;

    private TextView titleText;
    private TextView changeText;
    private TextView userName;
    private TextView userEmail;

    private Button signButton;

    private EditText mEmailField;
    private EditText mPasswordField;
    private EditText mDisplayNameField;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        setProgressBar(R.id.progressBar);
        showProgressBar();

        // Views
        titleText = findViewById(R.id.title_text);
        mEmailField = findViewById(R.id.fieldEmail);
        mPasswordField = findViewById(R.id.fieldPassword);
        mDisplayNameField = findViewById(R.id.fieldDispalayName);
        userName = findViewById(R.id.user_name);
        userEmail = findViewById(R.id.user_email);

        // Buttons
        signButton = findViewById(R.id.sign_button);
        signButton.setOnClickListener(this);
        changeText = findViewById(R.id.change_text);
        changeText.setOnClickListener(this);

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]
        Help.hideNavigationBar(getWindow(), Objects.requireNonNull(getSupportActionBar()));
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                if (!displayName.isEmpty()) {
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(displayName).build();
                    user.updateProfile(profileUpdates).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Log.d("Display name: ", displayName);
                            sendEmailVerification();
                            updateUI(user);
                        }
                    });
                }
                displayName = "";
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MainActivity.startPlay(MainActivity.buttonSong);
            finish();
            overridePendingTransition(0, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        mAuth.addAuthStateListener(mAuthListener);
        updateUI(currentUser);
    }
    // [END on_start_check_user]


    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void createAccount(String name, String email, String password) {
        Log.d(TAG, "createAccount: " + email);
        if (!validateForm()) {
            return;
        }
        if (TextUtils.isEmpty(name)) {
            mDisplayNameField.setError("Required.");
            return;
        } else {
            mDisplayNameField.setError(null);
        }
        displayName = name;
        showProgressBar();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                .setDisplayName(name)
                                .build();
                        if (user != null) {
                            user.updateProfile(profileUpdate);
                        }
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(EmailPasswordActivity.this, R.string.auth_failed,
                                Toast.LENGTH_SHORT).show();
                        updateUI(null);
                    }

                    // [START_EXCLUDE]
                    hideProgressBar();
                    // [END_EXCLUDE]
                });
        // [END create_user_with_email]
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressBar();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(EmailPasswordActivity.this, R.string.auth_failed,
                                Toast.LENGTH_SHORT).show();
                        updateUI(null);
                        // [START_EXCLUDE]
                        checkForMultiFactorFailure(task.getException());
                        // [END_EXCLUDE]
                    }

                    // [START_EXCLUDE]
                    if (!task.isSuccessful()) {
                        titleText.setText(R.string.auth_failed);
                    }
                    hideProgressBar();
                    // [END_EXCLUDE]
                });
        // [END sign_in_with_email]
    }

    private void signOut() {
        mAuth.signOut();
        updateUI(null);
    }

    private void sendEmailVerification() {
        // Disable button

        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(this, task -> {
                        // [START_EXCLUDE]
                        // Re-enable button
                        if (task.isSuccessful()) {
                            Toast.makeText(EmailPasswordActivity.this,
                                    "Verification email sent to " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(EmailPasswordActivity.this,
                                    R.string.fail_send_email_vertification,
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [END_EXCLUDE]
                    });
            // [END send_email_verification]
        } else {
            Log.e(TAG, "sendEmailVerification Error: user == null");
            Toast.makeText(EmailPasswordActivity.this,
                    R.string.fail_send_email_vertification,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void reload() {
        if (!Objects.requireNonNull(mAuth.getCurrentUser()).isEmailVerified()) {
            sendEmailVerification();
        }
        mAuth.getCurrentUser().reload().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                updateUI(mAuth.getCurrentUser());
                Toast.makeText(EmailPasswordActivity.this,
                        R.string.reload_successful,
                        Toast.LENGTH_SHORT).show();
            } else {
                Log.e(TAG, "reload", task.getException());
                Toast.makeText(EmailPasswordActivity.this,
                        R.string.reload_fail,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    @SuppressLint("StringFormatMatches")
    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
            MainActivity.currentUser = currentUser;
            MainActivity.displayName = currentUser.getDisplayName();
            userName.setText(getString(R.string.emailpassword_status_name, currentUser.getDisplayName()));
            String isVertifed;
            if (currentUser.isEmailVerified()) {
                isVertifed = getResources().getString(R.string.have);
            } else {
                isVertifed = getResources().getString(R.string.not_have);
            }
            userEmail.setText(getString(R.string.emailpassword_status_email,
                    currentUser.getEmail(), isVertifed));
            mDisplayNameField.setVisibility(View.GONE);
            mEmailField.setVisibility(View.GONE);
            mPasswordField.setVisibility(View.GONE);
            userName.setVisibility(View.VISIBLE);
            userEmail.setVisibility(View.VISIBLE);
            new Handler().postDelayed(() -> {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference databaseUser = database.getReference("users").child(MainActivity.displayName);
                Users user = new Users(currentUser.getUid(), currentUser.getEmail(), MainActivity.displayName);
                databaseUser.setValue(user);
                hideProgressBar();
                setIdNow(ID_SIGNED);
            }, 2500);
        } else {
            MainActivity.displayName = getString(R.string.person);
            mDisplayNameField.setVisibility(View.VISIBLE);
            mEmailField.setVisibility(View.VISIBLE);
            mPasswordField.setVisibility(View.VISIBLE);
            userName.setVisibility(View.GONE);
            userEmail.setVisibility(View.GONE);
            setIdNow(ID_SIGN);
        }
        hideProgressBar();
    }

    private void checkForMultiFactorFailure(Exception e) {
        // Multi-factor authentication with SMS is currently only available for
        // Google Cloud Identity Platform projects. For more information:
        // https://cloud.google.com/identity-platform/docs/android/mfa
        if (e instanceof FirebaseAuthMultiFactorException) {
            Log.w(TAG, "multiFactorFailure", e);
            Intent intent = new Intent();
            MultiFactorResolver resolver = ((FirebaseAuthMultiFactorException) e).getResolver();
            intent.putExtra("EXTRA_MFA_RESOLVER", resolver);
            setResult(RESULT_NEEDS_MFA_SIGN_IN, intent);
            finish();
        }
    }

    private void updateActivity() {
        if (ID_NOW == ID_CREATE) {
            mDisplayNameField.setVisibility(View.VISIBLE);
            signButton.setText(R.string.create);
            titleText.setText(R.string.create_account);
            changeText.setText(R.string.sign_in_account);
        } else if (ID_NOW == ID_SIGN) {
            mDisplayNameField.setVisibility(View.GONE);
            signButton.setText(R.string.sign_in);
            titleText.setText(R.string.sign_in_account);
            changeText.setText(R.string.create_account);
        } else if (ID_NOW == ID_SIGNED) {
            signButton.setText(R.string.sign_out);
            titleText.setText(R.string.signed_in);
            changeText.setText(R.string.reload);
        }
    }

    private void setIdNow(int idNow) {
        ID_NOW = idNow;
        updateActivity();
    }

    @Override
    public void onClick(View v) {
        if (!isProgressBar) {
            MainActivity.startPlay(MainActivity.buttonSong);
            int i = v.getId();
            if (i == R.id.sign_button) {
                if (ID_NOW == ID_CREATE) {
                    createAccount(mDisplayNameField.getText().toString(), mEmailField.getText().toString(), mPasswordField.getText().toString());
                } else if (ID_NOW == ID_SIGN) {
                    signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
                } else if (ID_NOW == ID_SIGNED) {
                    signOut();
                }
            } else if (i == R.id.change_text) {
                if (ID_NOW == ID_SIGN) {
                    setIdNow(ID_CREATE);
                } else if (ID_NOW == ID_CREATE) {
                    setIdNow(ID_SIGN);
                } else if (ID_NOW == ID_SIGNED) {
                    reload();
                }
            }
        }else{
            MainActivity.startPlay((MainActivity.buttonCloseSong));
        }
    }
}