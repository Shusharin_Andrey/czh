package com.shusharin.czh.SelectionGames;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.czh.R;
import com.shusharin.czh.MainActivity;
import com.shusharin.czh.SquareDesign;

public class AdapterSpiner extends ArrayAdapter<SquareDesign> {

    public AdapterSpiner(Context context) {
        super(context, R.layout.spinner_square, MainActivity.squareDesigns);
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @SuppressLint("InflateParams")
    @NonNull
    public View getCustomView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SquareDesign squareDesign = getItem(position);
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_square, null);
        ((TextView) convertView.findViewById(R.id.textView2)).setText(squareDesign.getName());
        convertView.findViewById(R.id.blue).setBackgroundResource(squareDesign.getDesignSmallBlue());
        convertView.findViewById(R.id.red).setBackgroundResource(squareDesign.getDesignSmallRed());
        convertView.findViewById(R.id.orange).setBackgroundResource(squareDesign.getDesignSmallOrange());
        convertView.findViewById(R.id.violet).setBackgroundResource(squareDesign.getDesignSmallViolet());
        return convertView;
    }
}

