package com.shusharin.czh.SelectionGames;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.czh.R;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.Field.Field;
import com.shusharin.czh.Field.FieldThree;
import com.shusharin.czh.Field.OnePlayer;
import com.shusharin.czh.Field.Table;
import com.shusharin.czh.Field.ThreePlayer;
import com.shusharin.czh.Interface.Help;
import com.shusharin.czh.MainActivity;

import java.util.ArrayList;
import java.util.Objects;

public class Continue extends AppCompatActivity {
    private static final String TAG = "Continue";
    public static ArrayList<Conservation> conservations = new ArrayList<>();
    protected static ArrayList<Conservation> conservationsOnline = new ArrayList<>();
    protected static Conservation conservation;
    protected static AlertDialog dialog;
    protected static RecyclerView listConservation;
    protected static Intent intent;
    protected static boolean isDialogShow = false;
    protected static boolean isOpen = false;
    protected static boolean isLocal;
    protected boolean isPressed;
    protected MyAdapter adapterConservationsTwoPlayerOneField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_continue);
        openOptionsMenu();
        Help.hideNavigationBar(getWindow(), Objects.requireNonNull(getSupportActionBar()));
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        TextView text = findViewById(R.id.tvTitle);
        text.setText(R.string.title_continue);
        isLocal = true;
        onCreateDialog();
        listConservation = findViewById(R.id.List);
        start();

    }

    protected void start() {
        adapterConservationsTwoPlayerOneField = new MyAdapter(this, conservations);
        listConservation.setAdapter(adapterConservationsTwoPlayerOneField);
        listConservation.setVisibility(View.VISIBLE);
        adapterConservationsTwoPlayerOneField.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MainActivity.startPlay(MainActivity.buttonSong);
            finish();
            overridePendingTransition(0, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isDialogShow) {
            showDialog();
        }
    }

    private static void showDialog() {
        isDialogShow = true;
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(Conservation.SIZE, conservations.size());
        for (int i = 0; i < conservations.size(); i++) {
            edit.putString(getString(R.string.Game) + i, conservations.get(i).getNameNoNumber());
            edit.putInt(Conservation.NUMBER_NAME + i, conservations.get(i).getNumberName());
            edit.putString(Conservation.ONE_PLAYER + i, conservations.get(i).getNameOnePlayer());
            edit.putString(Conservation.TWO_PLAYER + i, conservations.get(i).getNameTwoPlayer());
            edit.putString(Conservation.THREE_PLAYER + i, conservations.get(i).getNameThreePlayer());
            edit.putInt(Conservation.NUMBER_PLAYER + i, conservations.get(i).getNumberPlayer());
            edit.putInt(Conservation.NUMBER_FIELD + i, conservations.get(i).getNumberField());
            edit.putBoolean(Conservation.IS_FINISHED + i, conservations.get(i).isFinished());
            edit.putBoolean(Conservation.IS_FIRST + i, conservations.get(i).isFirst());
        }
        edit.apply();
    }

    public static void toConservation(Conservation conservation) {
        Continue.conservation = conservation;
        showDialog();
    }

    @SuppressLint({"InflateParams"})
    protected void onCreateDialog() {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View viewDialog = inflater.inflate(R.layout.dialog_game_name, null);
        View[] SquareView0 = new View[9];
        View[] SquareView1 = new View[9];
        View[] SquareView2 = new View[9];
        SquareView0[0] = viewDialog.findViewById(R.id.square00);
        SquareView0[1] = viewDialog.findViewById(R.id.square01);
        SquareView0[2] = viewDialog.findViewById(R.id.square02);
        SquareView0[3] = viewDialog.findViewById(R.id.square10);
        SquareView0[4] = viewDialog.findViewById(R.id.square11);
        SquareView0[5] = viewDialog.findViewById(R.id.square12);
        SquareView0[6] = viewDialog.findViewById(R.id.square20);
        SquareView0[7] = viewDialog.findViewById(R.id.square21);
        SquareView0[8] = viewDialog.findViewById(R.id.square22);

        SquareView1[0] = viewDialog.findViewById(R.id.square03);
        SquareView1[1] = viewDialog.findViewById(R.id.square04);
        SquareView1[2] = viewDialog.findViewById(R.id.square05);
        SquareView1[3] = viewDialog.findViewById(R.id.square13);
        SquareView1[4] = viewDialog.findViewById(R.id.square14);
        SquareView1[5] = viewDialog.findViewById(R.id.square15);
        SquareView1[6] = viewDialog.findViewById(R.id.square23);
        SquareView1[7] = viewDialog.findViewById(R.id.square24);
        SquareView1[8] = viewDialog.findViewById(R.id.square25);

        SquareView2[0] = viewDialog.findViewById(R.id.square06);
        SquareView2[1] = viewDialog.findViewById(R.id.square07);
        SquareView2[2] = viewDialog.findViewById(R.id.square08);
        SquareView2[3] = viewDialog.findViewById(R.id.square16);
        SquareView2[4] = viewDialog.findViewById(R.id.square17);
        SquareView2[5] = viewDialog.findViewById(R.id.square18);
        SquareView2[6] = viewDialog.findViewById(R.id.square26);
        SquareView2[7] = viewDialog.findViewById(R.id.square27);
        SquareView2[8] = viewDialog.findViewById(R.id.square28);

        viewDialog.setBackgroundResource(R.drawable.grey);
        builder.setView(viewDialog);
        Button positive = viewDialog.findViewById(R.id.positive);
        positiveSetName(positive);
        positive.setOnClickListener(arg0 -> {
            if (!isPressed) {
                isPressed = true;
                MainActivity.startPlay(MainActivity.buttonSong);
                MainActivity.saveName = conservation.getName();
                positiveSetOnClickListener();
                new Handler().postDelayed(() -> isPressed = false, 250);
            } else {
                MainActivity.startPlay(MainActivity.buttonCloseSong);
            }
        });
        dialog = builder.create();
        viewDialog.findViewById(R.id.negative).setOnClickListener(arg0 -> {
            if (!isPressed) {
                isPressed = true;
                closeDialog(viewDialog);
                MainActivity.startPlay(MainActivity.buttonSong);
                dialog.dismiss();
                new Handler().postDelayed(() -> isPressed = false, 100);
            }
        });
        dialog.setOnShowListener(arg0 -> {
            TextView nameGameText = viewDialog.findViewById(R.id.nameGame);
            TextView numberFieldText = viewDialog.findViewById(R.id.field);
            TextView numberPlayerText = viewDialog.findViewById(R.id.player);
            nameGameText.setText(String.format(getString(R.string.Party) + getString(R.string.enter_stoke), conservation.getName()));
            numberFieldText.setText(conservation.getNumberFieldString());
            numberPlayerText.setText(conservation.getNumberPlayerString());

            View table3 = viewDialog.findViewById(R.id.table3);
            View table4 = viewDialog.findViewById(R.id.table4);
            View table5 = viewDialog.findViewById(R.id.table5);
            View table6 = viewDialog.findViewById(R.id.table6);
            View table7 = viewDialog.findViewById(R.id.table7);
            View table8 = viewDialog.findViewById(R.id.table8);
            table5.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            table6.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
            if (conservation.getNumberField() == 1) {
                View table0 = viewDialog.findViewById(R.id.table0);
                View table2 = viewDialog.findViewById(R.id.table2);
                table0.setVisibility(View.GONE);
                table2.setVisibility(View.GONE);
                table3.setVisibility(View.GONE);
                table4.setVisibility(View.GONE);
                table7.setVisibility(View.GONE);
                table8.setVisibility(View.GONE);
                for (int i = 0; i < 9; i++) {
                    setBackgroundOneField(SquareView1[i], i);
                }
            } else  {
                table3.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
                table8.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
                if (conservation.getNumberPlayer() == 3) {
                    table4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
                    table7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
                }else{
                    table4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
                    table7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
                }
                for (int i = 0; i < 9; i++) {
                    setBackgroundThreeField(SquareView0[i], SquareView1[i], SquareView2[i], i);
                }
            }
            TextView nameOnePlayer = viewDialog.findViewById(R.id.OnePlayer);
            TextView nameTwoPlayer = viewDialog.findViewById(R.id.TwoPlayer);
            TextView nameThreePlayer = viewDialog.findViewById(R.id.ThreePlayer);
            nameOnePlayer.setText(conservation.getNameOnePlayer());
            nameTwoPlayer.setText(conservation.getNameTwoPlayer());
            viewDialog.findViewById(R.id.ColorOne).setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
            viewDialog.findViewById(R.id.ColorTwo).setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            switch (conservation.getNumberPlayer()) {
                case 1:
                case 2:
                    nameThreePlayer.setVisibility(View.GONE);
                    viewDialog.findViewById(R.id.ColorThree).setVisibility(View.GONE);
                    break;
                case 3:
                    nameThreePlayer.setText(conservation.getNameThreePlayer());
                    nameThreePlayer.setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.ColorThree).setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.ColorThree).setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
                    break;
            }
            select(nameOnePlayer, viewDialog, 1);
            select(nameTwoPlayer, viewDialog, 2);
            select(nameThreePlayer, viewDialog, 5);
        });
        dialog.setOnCancelListener(arg0 -> closeDialog(viewDialog));
        dialog.setOnDismissListener(arg0 -> closeDialog(viewDialog));
    }

    protected void select(TextView textView, View viewDialog, int id) {
    }

    protected void positiveSetName(Button positive) {
        positive.setText(R.string.open);
    }

    protected void positiveSetOnClickListener() {
        if (conservation.getNumberPlayer() == 1) {
            intent = new Intent(this, OnePlayer.class);
            intent.putExtra(Conservation.IS_FIRST, conservation.isFirst());
        } else if (conservation.getNumberPlayer() == 2) {
            if (conservation.getNumberField() == 1) {
                intent = new Intent(this, Field.class);
            } else {
                intent = new Intent(this, FieldThree.class);
            }
        } else {
            intent = new Intent(this, ThreePlayer.class);
        }
        intent.putExtra(Conservation.NUMBER_CONSERVATION, Continue.conservations.indexOf(conservation));
        intent.putExtra(Conservation.TAG, conservation);
        startActivity(intent);
        overridePendingTransition(0, 0);
        isDialogShow = false;
        dialog.dismiss();
    }

    protected void setBackgroundOneField(@NonNull View view, int number) {
        SharedPreferences preferences = getSharedPreferences(conservation.getName(), Context.MODE_PRIVATE);
        view.setBackgroundResource(Field.getBackgroundResourceThin(preferences.getInt(Table.APP_SAVE_BIG_SQUARE_INT + 0 + number, 0)));
    }

    protected void setBackgroundThreeField(@NonNull View view1, @NonNull View view2, @NonNull View view3, int number) {
        SharedPreferences preferences = getSharedPreferences(conservation.getName(), Context.MODE_PRIVATE);
        view1.setBackgroundResource(Field.getBackgroundResourceThin(preferences.getInt(Table.APP_SAVE_BIG_SQUARE_INT + 1 + number, 0)));
        view2.setBackgroundResource(Field.getBackgroundResourceThin(preferences.getInt(Table.APP_SAVE_BIG_SQUARE_INT + 2 + number, 0)));
        view3.setBackgroundResource(Field.getBackgroundResourceThin(preferences.getInt(Table.APP_SAVE_BIG_SQUARE_INT + 3 + number, 0)));
    }

    protected void closeDialog(@NonNull View viewDialog) {
        View table3 = viewDialog.findViewById(R.id.table3);
        View table4 = viewDialog.findViewById(R.id.table4);
        View table7 = viewDialog.findViewById(R.id.table7);
        View table8 = viewDialog.findViewById(R.id.table8);
        if (conservation.getNumberField() == 1) {
            View table0 = viewDialog.findViewById(R.id.table0);
            View table2 = viewDialog.findViewById(R.id.table2);
            table0.setVisibility(View.VISIBLE);
            table2.setVisibility(View.VISIBLE);
            table3.setVisibility(View.VISIBLE);
            table4.setVisibility(View.VISIBLE);
            table7.setVisibility(View.VISIBLE);
            table8.setVisibility(View.VISIBLE);
        } else if (conservation.getNumberPlayer() == 3) {
            table4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            table7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
        }
        isDialogShow = false;
    }
}
