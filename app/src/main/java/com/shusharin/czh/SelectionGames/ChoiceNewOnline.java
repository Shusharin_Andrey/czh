package com.shusharin.czh.SelectionGames;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.czh.R;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.Field.FieldOnline;
import com.shusharin.czh.Field.FieldThreeOnline;
import com.shusharin.czh.Field.ThreePlayerOnline;
import com.shusharin.czh.MainActivity;

import static com.shusharin.czh.MainActivity.displayName;

public class ChoiceNewOnline extends ContinueOnline {
    private static final String TAG = "ChoiceNewOnline";
    private static int idPlayer = -1;

    @Override
    protected void closeDialog(@NonNull View viewDialog) {
        super.closeDialog(viewDialog);
        idPlayer = -1;
        TextView text = findViewById(R.id.tvTitle);
        text.setText(R.string.Connection);
    }

    protected void positiveSetName(Button positive) {
        positive.setText(R.string.Connection);
    }

    @Override
    protected void positiveSetOnClickListener() {
        if (idPlayer != -1) {
            if (conservation.getNumberPlayer() == 2) {
                if (conservation.getNumberField() == 1) {
                    intent = new Intent(this, FieldOnline.class);
                } else {
                    intent = new Intent(this, FieldThreeOnline.class);
                }
            } else {
                intent = new Intent(this, ThreePlayerOnline.class);
            }
            reference.child(MainActivity.saveName).child((idPlayer == 2) ? "nameTwoPlayer" : ((idPlayer == 1) ? "nameOnePlayer" : "nameThreePlayer")).setValue(displayName);
            database.child(MainActivity.saveName).child(getString(R.string.Field)).child(displayName).child(FieldOnline.APP_SAVE_ID_PLAYER).setValue(idPlayer);
            if (idPlayer == 1) {
                conservation.setNameOnePlayer(displayName);
            } else if (idPlayer == 2) {
                conservation.setNameTwoPlayer(displayName);
            } else {
                conservation.setNameThreePlayer(displayName);
            }
            intent.putExtra(Conservation.NUMBER_CONSERVATION, Continue.conservations.indexOf(conservation));
            if (!conservation.getNameOnePlayer().isEmpty() &&
                    !conservation.getNameTwoPlayer().isEmpty() &&
                    (!conservation.getNameThreePlayer().isEmpty() || conservation.getNumberPlayer() == 2)) {
                reference.child(MainActivity.saveName).child("loose").setValue(false);
            }
            intent.putExtra(Conservation.TAG, conservation);
            isDialogShow = false;
            isPressed = false;
            startActivity(intent);
            overridePendingTransition(0, 0);
            finish();
        } else {
            MainActivity.startPlay(MainActivity.buttonCloseSong);
            Toast.makeText(ChoiceNewOnline.this,
                    R.string.not_selected_side,
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void select(TextView textView, View viewDialog, int id) {
        textView.setOnClickListener(arg0 -> {
            if (!isPressed) {
                if (textView.getText().equals("")) {
                    TextView nameOnePlayer = viewDialog.findViewById(R.id.OnePlayer);
                    TextView nameTwoPlayer = viewDialog.findViewById(R.id.TwoPlayer);
                    TextView nameThreePlayer = viewDialog.findViewById(R.id.ThreePlayer);
                    nameOnePlayer.setText(conservation.getNameOnePlayer());
                    nameTwoPlayer.setText(conservation.getNameTwoPlayer());
                    switch (conservation.getNumberPlayer()) {
                        case 2:
                            nameThreePlayer.setVisibility(View.GONE);
                            viewDialog.findViewById(R.id.ColorThree).setVisibility(View.GONE);
                            break;
                        case 3:
                            nameThreePlayer.setText(conservation.getNameThreePlayer());
                            nameThreePlayer.setVisibility(View.VISIBLE);
                            viewDialog.findViewById(R.id.ColorThree).setVisibility(View.VISIBLE);
                            break;
                    }
                    MainActivity.startPlay(MainActivity.buttonSong);
                    idPlayer = id;
                    textView.setText(displayName);
                } else {
                    MainActivity.startPlay(MainActivity.buttonCloseSong);
                }
                new Handler().postDelayed(() -> isPressed = false, 100);
            }
        });
    }

    protected void addConversationIF(@NonNull Conservation conservation) {
        if (conservation.isLoose() &&
                (!displayName.equals(conservation.getNameOnePlayer()) &&
                        !displayName.equals(conservation.getNameTwoPlayer()) &&
                        !displayName.equals(conservation.getNameThreePlayer()))) {
            conservationsOnline.add(conservation);
        }
    }
}
