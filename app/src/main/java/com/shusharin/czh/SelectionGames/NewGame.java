package com.shusharin.czh.SelectionGames;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.czh.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.Field.Field;
import com.shusharin.czh.Field.FieldOnline;
import com.shusharin.czh.Field.FieldThree;
import com.shusharin.czh.Field.FieldThreeOnline;
import com.shusharin.czh.Field.OnePlayer;
import com.shusharin.czh.Field.ThreePlayer;
import com.shusharin.czh.Field.ThreePlayerOnline;
import com.shusharin.czh.Interface.EmailPasswordActivity;
import com.shusharin.czh.Interface.Help;
import com.shusharin.czh.Interface.SettingsActivity;
import com.shusharin.czh.MainActivity;

public class NewGame extends MainActivity implements
        View.OnClickListener {
    private static final String TAG = "NewGame";
    protected Button onePlayer;
    protected Button twoPlayer;
    protected Button threePlayer;
    protected Button onePole;
    protected Button threePole;
    protected Button online;
    protected Button local;
    protected Button load;
    protected Button create;
    protected Button back;
    private AlertDialog dialog;
    private Intent intent;
    private static Conservation conservation;
    private static boolean isDialogShow = false;
    protected EditText editNameGame;
    protected static int whatOpen = 0;
    protected static int ID;
    protected static boolean isLocal;
    protected static boolean isFirst;
    private static boolean isSelected = false;
    static int Number = 0;
    static String name;

    private static DatabaseReference reference;
    private DatabaseReference database;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        isNeedHideNavigationBar = false;
        super.onCreate(savedInstanceState);
        onCreateDialog();
        onePlayer = findViewById(R.id.onePlayer);
        twoPlayer = findViewById(R.id.twoPlayer);
        threePlayer = findViewById(R.id.threePlayer);
        onePole = findViewById(R.id.onePole);
        threePole = findViewById(R.id.threePole);
        online = findViewById(R.id.Online);
        local = findViewById(R.id.Local);
        load = findViewById(R.id.Load);
        create = findViewById(R.id.Create);
        back = findViewById(R.id.back);
        editNameGame = findViewById(R.id.editTextName);
        onePlayer.setOnClickListener(this);
        twoPlayer.setOnClickListener(this);
        threePlayer.setOnClickListener(this);
        onePole.setOnClickListener(this);
        threePole.setOnClickListener(this);
        back.setOnClickListener(this);
        local.setOnClickListener(this);
        online.setOnClickListener(this);
        load.setOnClickListener(this);
        create.setOnClickListener(this);
        findViewById(R.id.help2).setOnClickListener(this);
        if (currentUser == null || !currentUser.isEmailVerified()) {
            online.setBackgroundResource(R.drawable.button_pressed);
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            return cm.getActiveNetworkInfo() != null;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (!isPressed) {
            isPressed = true;
            MainActivity.startPlay(buttonSong);
            switch (v.getId()) {
                case R.id.Local:
                    onePlayer.setText(R.string.onePlayer);
                    isLocal = true;
                    whatOpen = 1;
                    updateActivity();
                    break;
                case R.id.Online:
                    if (currentUser != null && currentUser.isEmailVerified() && isOnline()) {
                        database = FirebaseDatabase.getInstance().getReference(getString(R.string.Games));
                        reference = FirebaseDatabase.getInstance().getReference().child("NameGame");
                        isLocal = false;
                        isSelected = false;
                        whatOpen = 4;
                        onePlayer.setText(R.string.Two_player);
                        updateActivity();
                    } else {
                        isPressed = false;
                        MainActivity.startPlay(buttonCloseSong);
                        String text;
                        if (isOnline()) {
                            text = getString(R.string.сonfirmation_error);
                        } else {
                            text = getString(R.string.no_internet);
                        }
                        Toast.makeText(NewGame.this, text,
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.onePlayer:
                    isSelected = false;
                    if (isLocal) {
                        intent = new Intent(NewGame.this, OnePlayer.class);
                        setName(1, 1);
                    } else {
                        whatOpen = 2;
                        updateActivity();
                    }
                    break;
                case R.id.twoPlayer:
                    whatOpen = 2;
                    updateActivity();
                    break;
                case R.id.threePlayer:
                    if (isLocal) {
                        isSelected = true;
                        intent = new Intent(NewGame.this, ThreePlayer.class);
                    } else {
                        intent = new Intent(NewGame.this, ThreePlayerOnline.class);
                    }
                    setName(3, 3);
                    break;
                case R.id.onePole:
                    isSelected = true;
                    if (isLocal) {
                        intent = new Intent(NewGame.this, Field.class);
                    } else {
                        intent = new Intent(NewGame.this, FieldOnline.class);
                    }
                    setName(2, 1);
                    break;
                case R.id.threePole:
                    isSelected = true;
                    if (isLocal) {
                        intent = new Intent(NewGame.this, FieldThree.class);
                    } else {
                        intent = new Intent(NewGame.this, FieldThreeOnline.class);
                    }
                    setName(2, 3);
                    break;
                case R.id.Load:
                    isContinue = true;
                    Intent intent = new Intent(NewGame.this, ChoiceNewOnline.class);
                    intent.putExtra("IS_LOAD", false);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    break;
                case R.id.Create:
                    whatOpen = 5;
                    updateActivity();
                    break;
                case R.id.back:
                    switch (whatOpen) {
                        case 2:
                            if (!isLocal) {
                                whatOpen = 5;
                                break;
                            }
                        case 3:
                            whatOpen = 1;
                            break;
                        case 1:
                        case 4:
                            whatOpen = 0;
                            break;
                        case 5:
                            whatOpen = 4;
                            break;
                        default:
                            finish();
                            overridePendingTransition(0, 0);
                    }
                    updateActivity();
                    break;
                case R.id.Profile:
                    Intent intent1 = new Intent(NewGame.this, EmailPasswordActivity.class);
                    startActivity(intent1);
                    overridePendingTransition(0, 0);
                    break;
                case R.id.help2:
                    Intent intent2 = new Intent(NewGame.this, Help.class);
                    startActivity(intent2);
                    overridePendingTransition(0, 0);
                    break;
                case R.id.Settings:
                    Intent intent3 = new Intent(this, SettingsActivity.class);
                    startActivity(intent3);
                    overridePendingTransition(0, 0);
                    break;
            }
            new Handler().postDelayed(() -> isPressed = false, 100);
        } else {
            MainActivity.startPlay(buttonCloseSong);
        }
    }

    protected void updateActivity() {
        onePlayer.setVisibility(View.GONE);
        twoPlayer.setVisibility(View.GONE);
        threePlayer.setVisibility(View.GONE);
        onePole.setVisibility(View.GONE);
        threePole.setVisibility(View.GONE);
        local.setVisibility(View.GONE);
        online.setVisibility(View.GONE);
        load.setVisibility(View.GONE);
        create.setVisibility(View.GONE);
        switch (whatOpen) {
            case 0:
                local.setVisibility(View.VISIBLE);
                online.setVisibility(View.VISIBLE);
                break;
            case 1:
                twoPlayer.setVisibility(View.VISIBLE);
            case 5:
                onePlayer.setVisibility(View.VISIBLE);
                threePlayer.setVisibility(View.VISIBLE);
                break;
            case 2:
                onePole.setVisibility(View.VISIBLE);
                threePole.setVisibility(View.VISIBLE);
                break;
            case 4:
                load.setVisibility(View.VISIBLE);
                create.setVisibility(View.VISIBLE);
                break;
        }
        new Handler().postDelayed(() -> isPressed = false, 100);
    }

    protected void onStart() {
        isNeedHideNavigationBar = false;
        super.onStart();
        hello.setVisibility(View.INVISIBLE);
        findViewById(R.id.view1).setVisibility(View.GONE);
        findViewById(R.id.view2).setVisibility(View.GONE);
        findViewById(R.id.view3).setVisibility(View.GONE);
        hello.setVisibility(View.INVISIBLE);
        hello.setVisibility(View.INVISIBLE);
        editNameGame.setVisibility(View.VISIBLE);
        play.setVisibility(View.INVISIBLE);
        continueB.setVisibility(View.INVISIBLE);
        help.setVisibility(View.INVISIBLE);
        findViewById(R.id.help2).setVisibility(View.VISIBLE);
        updateActivity();
        back.setVisibility(View.VISIBLE);
        if (isDialogShow) {
            showDialog();
        }
        label.setVisibility(View.GONE);
        background2.setVisibility(View.GONE);
    }

    @SuppressLint({"InflateParams"})
    private void onCreateDialog() {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View viewDialog = inflater.inflate(R.layout.dialog_game_name, null);
        viewDialog.setBackgroundResource(R.drawable.grey);
        builder.setView(viewDialog);
        viewDialog.findViewById(R.id.positive).setOnClickListener(arg0 -> {
            if (isSelected) {
                MainActivity.startPlay(buttonSong);
                saveName = conservation.getName();
                if (!isLocal && currentUser!=null) {
                    DatabaseReference databaseUser = database.child(saveName).child(getString(R.string.Field)).child(displayName);
                    databaseUser.child(FieldOnline.APP_SAVE_ID_PLAYER).setValue(ID);
                    if (ID == 1) {
                        conservation.setNameOnePlayer(displayName);
                    } else if (ID == 2) {
                        conservation.setNameTwoPlayer(displayName);
                    } else {
                        conservation.setNameThreePlayer(displayName);
                    }
                    reference.child(saveName).setValue(conservation);
                } else {
                    if (conservation.getNumberPlayer() == 1) {
                        conservation.setFirst(isFirst);
                        intent.putExtra(Conservation.IS_FIRST, isFirst);
                        if (conservation.isFirst()) {
                            conservation.setNameOnePlayer(displayName);
                            conservation.setNameTwoPlayer(getResources().getString(R.string.comp));
                        } else {
                            conservation.setNameOnePlayer(getResources().getString(R.string.comp));
                            conservation.setNameTwoPlayer(displayName);
                        }
                    }
                    Continue.conservations.add(conservation);
                }
                intent.putExtra(Conservation.NUMBER_CONSERVATION, Continue.conservations.indexOf(conservation));
                intent.putExtra(Conservation.TAG, conservation);
                whatOpen = 0;
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
                isDialogShow = false;
                new Handler().postDelayed(() -> isPressed = false, 100);
            } else {
                MainActivity.startPlay(buttonCloseSong);
                Toast.makeText(this,
                        R.string.not_selected_side,
                        Toast.LENGTH_SHORT).show();
            }
        });
        dialog = builder.create();
        viewDialog.findViewById(R.id.negative).setOnClickListener(arg0 -> {
            MainActivity.startPlay(buttonSong);
            closeDialog(viewDialog);
            dialog.dismiss();
            new Handler().postDelayed(() -> isPressed = false, 100);
        });
        dialog.setOnShowListener(arg0 -> {
            TextView nameGameText = viewDialog.findViewById(R.id.nameGame);
            TextView numberFieldText = viewDialog.findViewById(R.id.field);
            TextView numberPlayerText = viewDialog.findViewById(R.id.player);
            if (conservation.getNumberName() != 0) {
                nameGameText.setText(String.format(getString(R.string.party_name_will_be_changed_to) + getString(R.string.enter_stoke), conservation.getName()));
            } else {
                nameGameText.setText(String.format(getString(R.string.Party) + getString(R.string.enter_stoke), conservation.getName()));
            }
            numberFieldText.setText(conservation.getNumberFieldString());
            numberPlayerText.setText(conservation.getNumberPlayerString());
            View table3 = viewDialog.findViewById(R.id.table3);
            View table4 = viewDialog.findViewById(R.id.table4);
            View table5 = viewDialog.findViewById(R.id.table5);
            View table6 = viewDialog.findViewById(R.id.table6);
            View table7 = viewDialog.findViewById(R.id.table7);
            View table8 = viewDialog.findViewById(R.id.table8);
            table5.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            table6.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
            if (conservation.getNumberField() == 1) {
                View table0 = viewDialog.findViewById(R.id.table0);
                View table2 = viewDialog.findViewById(R.id.table2);
                table0.setVisibility(View.GONE);
                table2.setVisibility(View.GONE);
                table3.setVisibility(View.GONE);
                table4.setVisibility(View.GONE);
                table7.setVisibility(View.GONE);
                table8.setVisibility(View.GONE);
            } else {
                table3.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
                table8.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
                if (conservation.getNumberPlayer() == 3) {
                    table4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
                    table7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
                } else {
                    table4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
                    table7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
                }
            }
            TextView nameOnePlayer = viewDialog.findViewById(R.id.OnePlayer);
            TextView nameTwoPlayer = viewDialog.findViewById(R.id.TwoPlayer);
            TextView nameThreePlayer = viewDialog.findViewById(R.id.ThreePlayer);
            nameOnePlayer.setText(conservation.getNameOnePlayer());
            nameTwoPlayer.setText(conservation.getNameTwoPlayer());
            viewDialog.findViewById(R.id.ColorOne).setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
            viewDialog.findViewById(R.id.ColorTwo).setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            switch (conservation.getNumberPlayer()) {
                case 1:
                case 2:
                    nameThreePlayer.setVisibility(View.GONE);
                    viewDialog.findViewById(R.id.ColorThree).setVisibility(View.GONE);
                    break;
                case 3:
                    nameThreePlayer.setText(conservation.getNameThreePlayer());
                    nameThreePlayer.setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.ColorThree).setVisibility(View.VISIBLE);
                    viewDialog.findViewById(R.id.ColorThree).setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallOrange());
                    break;
            }
            if (!isLocal) {
                select(nameOnePlayer, viewDialog, 1);
                select(nameTwoPlayer, viewDialog, 2);
                select(nameThreePlayer, viewDialog, 5);
            } else {
                selectLocal(nameOnePlayer, viewDialog, 1);
                selectLocal(nameTwoPlayer, viewDialog, 2);
            }
        });
        dialog.setOnCancelListener(arg0 -> closeDialog(viewDialog));
        dialog.setOnDismissListener(arg0 -> closeDialog(viewDialog));
    }

    private void select(TextView textView, View viewDialog, int id) {
        textView.setOnClickListener(arg0 -> {
            if (!isPressed) {
                if (textView.getText() == "") {
                    TextView nameOnePlayer = viewDialog.findViewById(R.id.OnePlayer);
                    TextView nameTwoPlayer = viewDialog.findViewById(R.id.TwoPlayer);
                    TextView nameThreePlayer = viewDialog.findViewById(R.id.ThreePlayer);
                    nameOnePlayer.setText(conservation.getNameOnePlayer());
                    nameTwoPlayer.setText(conservation.getNameTwoPlayer());
                    switch (conservation.getNumberPlayer()) {
                        case 2:
                            nameThreePlayer.setVisibility(View.GONE);
                            viewDialog.findViewById(R.id.ColorThree).setVisibility(View.GONE);
                            break;
                        case 3:
                            nameThreePlayer.setText(conservation.getNameThreePlayer());
                            nameThreePlayer.setVisibility(View.VISIBLE);
                            viewDialog.findViewById(R.id.ColorThree).setVisibility(View.VISIBLE);
                            break;
                    }
                    MainActivity.startPlay(buttonSong);
                    ID = id;
                    isSelected = true;
                    textView.setText(displayName);
                } else {
                    MainActivity.startPlay(buttonCloseSong);
                }
                new Handler().postDelayed(() -> isPressed = false, 100);
            }
        });
    }

    private void selectLocal(TextView textView, View viewDialog, int id) {
        textView.setOnClickListener(arg0 -> {
            if (conservation.getNumberPlayer() == 1) {
                if (!isPressed) {
                    TextView nameOnePlayer = viewDialog.findViewById(R.id.OnePlayer);
                    TextView nameTwoPlayer = viewDialog.findViewById(R.id.TwoPlayer);
                    if (id == 1) {
                        nameOnePlayer.setText(getResources().getString(R.string.person));
                        nameTwoPlayer.setText(getResources().getString(R.string.comp));
                        isFirst = true;
                    } else {
                        nameOnePlayer.setText(getResources().getString(R.string.comp));
                        nameTwoPlayer.setText(getResources().getString(R.string.person));
                        isFirst = false;
                    }
                    MainActivity.startPlay(buttonSong);
                    isSelected = true;
                    textView.setText(displayName);
                    new Handler().postDelayed(() -> isPressed = false, 100);
                }
            }
        });
    }

    private void closeDialog(View viewDialog) {
        View table3 = viewDialog.findViewById(R.id.table3);
        View table4 = viewDialog.findViewById(R.id.table4);
        View table7 = viewDialog.findViewById(R.id.table7);
        View table8 = viewDialog.findViewById(R.id.table8);
        if (conservation.getNumberField() == 1) {
            View table0 = viewDialog.findViewById(R.id.table0);
            View table2 = viewDialog.findViewById(R.id.table2);
            table0.setVisibility(View.VISIBLE);
            table2.setVisibility(View.VISIBLE);
            table3.setVisibility(View.VISIBLE);
            table4.setVisibility(View.VISIBLE);
            table7.setVisibility(View.VISIBLE);
            table8.setVisibility(View.VISIBLE);
        } else if (conservation.getNumberPlayer() == 3) {
            table4.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallRed());
            table7.setBackgroundResource(MainActivity.squareDesigns[MainActivity.numberDesign].getDesignSmallBlue());
        }
        ID = -1;
        isSelected = false;
        isDialogShow = false;
        new Handler().postDelayed(() -> isPressed = false, 100);
    }

    public void showDialog() {
        isDialogShow = true;
        dialog.show();
    }

    private void setName(int numberPlayer, int numberField) {
        String saveName = !editNameGame.getText().toString().equals("") ? editNameGame.getText().toString() : editNameGame.getHint().toString();
        int number = -1;
        if (isLocal) {
            for (int i = 0; i < Continue.conservations.size(); i++) {
                if (saveName.equals(Continue.conservations.get(i).getNameNoNumber())) {
                    if (number < Continue.conservations.get(i).getNumberName()) {
                        number = Continue.conservations.get(i).getNumberName();
                    }
                }
            }
            conservation = new Conservation(numberPlayer, numberField, saveName, number == -1 ? 0 : number + 1);
            if (conservation.getNumberPlayer() != 1) {
                conservation.setNameOnePlayer(getResources().getString(R.string.person) + 1);
                conservation.setNameTwoPlayer(getResources().getString(R.string.person) + 2);
                if (conservation.getNumberPlayer() == 3) {
                    conservation.setNameThreePlayer(getResources().getString(R.string.person) + 3);
                }
            }
            showDialog();
        } else {
            Number = 0;
            name = saveName;
            if (conservation != null) {
                conservation = null;
            }
            conservation = new Conservation(numberPlayer, numberField, saveName);
            findName(saveName);
        }
    }

    private void findName(String saveName) {
        reference.child(saveName).runTransaction(new Transaction.Handler() {
            @NonNull
            @Override
            public Transaction.Result doTransaction(@NonNull MutableData mutableData) {
                if (mutableData.getValue() == null) {
                    return Transaction.success(mutableData);
                }
                return Transaction.abort();
            }

            @Override
            public void onComplete(@Nullable DatabaseError databaseError, boolean b, @Nullable DataSnapshot dataSnapshot) {
                if (b) {
                    conservation.setNumberName(Number);
                    showDialog();
                } else {
                    Number++;
                    findName(name + Number);
                }
            }
        });
    }
}
