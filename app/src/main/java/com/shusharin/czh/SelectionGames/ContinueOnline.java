package com.shusharin.czh.SelectionGames;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.example.czh.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shusharin.czh.Conservation;
import com.shusharin.czh.Field.Field;
import com.shusharin.czh.Field.FieldOnline;
import com.shusharin.czh.Field.FieldThreeOnline;
import com.shusharin.czh.Field.Table;
import com.shusharin.czh.Field.ThreePlayerOnline;

import static com.shusharin.czh.MainActivity.displayName;

public class ContinueOnline extends Continue {
    private static final String TAG = "ContinueOnline";
    protected DatabaseReference database;
    protected static DatabaseReference reference;

    private static ValueEventListener LISTENER_CONSERVATION;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isLocal = false;
        database = FirebaseDatabase.getInstance().getReference("Games");
        reference = FirebaseDatabase.getInstance().getReference().child(getString(R.string.name_game));
        LISTENER_CONSERVATION = update(this);
    }

    @Override
    protected void start() {

    }

    @Override
    protected void positiveSetOnClickListener() {
        if (conservation.getNumberPlayer() == 2) {
            if (conservation.getNumberField() == 1) {
                intent = new Intent(this, FieldOnline.class);
            } else {
                intent = new Intent(this, FieldThreeOnline.class);
            }
        } else {
            intent = new Intent(this, ThreePlayerOnline.class);
        }
        intent.putExtra(Conservation.NUMBER_CONSERVATION, Continue.conservations.indexOf(conservation));
        intent.putExtra(Conservation.TAG, conservation);
        startActivity(intent);
        overridePendingTransition(0, 0);
        isDialogShow = false;
        isPressed = false;
        dialog.dismiss();
    }

    @Override
    protected void setBackgroundOneField(@NonNull View view, int number) {
        DatabaseReference reference1 = database.child(conservation.getName()).child(getString(R.string.Field)).child(getString(R.string.table) + 0).child(Table.APP_SAVE_BIG_SQUARE_INT).child(String.valueOf(number));
        ValueEventListener eventListener = update(reference1, view);
        new Handler().postDelayed(() -> reference1.removeEventListener(eventListener), 1000);
    }

    @Override
    protected void setBackgroundThreeField(@NonNull View view1, @NonNull View view2, @NonNull View view3, int number) {
        DatabaseReference reference1 = database.child(conservation.getName()).child(getString(R.string.Field)).child(getString(R.string.table) + 1).child(Table.APP_SAVE_BIG_SQUARE_INT).child(String.valueOf(number));
        DatabaseReference reference2 = database.child(conservation.getName()).child(getString(R.string.Field)).child(getString(R.string.table) + 2).child(Table.APP_SAVE_BIG_SQUARE_INT).child(String.valueOf(number));
        DatabaseReference reference3 = database.child(conservation.getName()).child(getString(R.string.Field)).child(getString(R.string.table) + 3).child(Table.APP_SAVE_BIG_SQUARE_INT).child(String.valueOf(number));
        ValueEventListener eventListener1 = update(reference1, view1);
        ValueEventListener eventListener2 = update(reference2, view2);
        ValueEventListener eventListener3 = update(reference3, view3);
        new Handler().postDelayed(() -> reference1.removeEventListener(eventListener1), 1000);
        new Handler().postDelayed(() -> reference2.removeEventListener(eventListener2), 1000);
        new Handler().postDelayed(() -> reference3.removeEventListener(eventListener3), 1000);
    }

    private ValueEventListener update(Activity activity) {
        return reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                conservationsOnline.clear();
                if (dataSnapshot.getValue() != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Conservation conservation = postSnapshot.getValue(Conservation.class);
                        if (conservation != null) {
                            addConversationIF(conservation);
                        }
                    }
                    listConservation.setVisibility(View.VISIBLE);
                    adapterConservationsTwoPlayerOneField = new MyAdapter(activity, conservationsOnline);
                    listConservation.setAdapter(adapterConservationsTwoPlayerOneField);
                    adapterConservationsTwoPlayerOneField.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    protected void addConversationIF(@NonNull Conservation conservation) {
        if (displayName.equals(conservation.getNameOnePlayer()) ||
                displayName.equals(conservation.getNameTwoPlayer()) ||
                displayName.equals(conservation.getNameThreePlayer())) {
            conservationsOnline.add(conservation);
        }
    }

    private ValueEventListener update(DatabaseReference databaseReference, View view) {
        return databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Integer.class) != null) {
                    view.setBackgroundResource(Field.getBackgroundResourceThin(dataSnapshot.getValue(Integer.class)));
                } else {
                    view.setBackgroundResource(Field.getBackgroundResourceThin(0));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (reference != null) {
            if (LISTENER_CONSERVATION != null) {
                reference.removeEventListener(LISTENER_CONSERVATION);
            }
        }
    }
}
