package com.shusharin.czh.SelectionGames;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.shusharin.czh.Conservation;
import com.example.czh.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.shusharin.czh.MainActivity;

import java.util.List;
import java.util.Objects;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<Conservation> conservations;
    private Activity context;

    MyAdapter(Activity context, List<Conservation> conservations) {
        this.conservations = conservations;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInGroup = inflater.inflate(R.layout.list_item, parent, false);
        ViewHolder holder = new ViewHolder(viewInGroup);
        View Delete = holder.delete;

        viewInGroup.setOnClickListener(view1 -> {
            MainActivity.startPlay(MainActivity.buttonSong);
            int position = holder.getAdapterPosition();
            Conservation conservation = conservations.get(position);
            Continue.toConservation(conservation);
        });

        Delete.setOnClickListener(Delete1 -> {
            MainActivity.startPlay(MainActivity.buttonSong);
            if(Continue.isLocal) {
                Conservation conservation = conservations.get(holder.getAdapterPosition());
                SharedPreferences preferences = context.getSharedPreferences(conservation.getName(), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.apply();
                Continue.conservations.remove(conservation);
                conservations = Continue.conservations;
            }else{
                Conservation conservation = conservations.get(holder.getAdapterPosition());
                String name = Objects.requireNonNull(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getDisplayName());
                if (name.equals(conservation.getNameOnePlayer()) ||
                        name.equals(conservation.getNameTwoPlayer()) ||
                        name.equals(conservation.getNameThreePlayer())) {
                    FirebaseDatabase.getInstance().getReference().child("NameGame").child(conservation.getName()).removeValue();
                    FirebaseDatabase.getInstance().getReference("Games").child(conservation.getName()).removeValue();
                    Continue.conservationsOnline.remove(conservation);
                }else{
                    MainActivity.startPlay(MainActivity.buttonCloseSong);
                }
            }
            notifyDataSetChanged();
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        Conservation conservation = conservations.get(position);
        holder.nameView.setText(conservation.getName());
        holder.playerView.setText(conservation.getNumberPlayerString());
        holder.fieldView.setText(conservation.getNumberFieldString());
    }

    @Override
    public int getItemCount() {
        return conservations.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final TextView nameView;
        final TextView playerView;
        final TextView fieldView;
        final View delete;
        final View layout;

        ViewHolder(View view) {
            super(view);
            nameView = view.findViewById(R.id.NameGame);
            playerView = view.findViewById(R.id.NumberPlayer);
            fieldView = view.findViewById(R.id.NumberField);
            delete = view.findViewById(R.id.remote);
            layout = view.findViewById(R.id.All_item);
        }
    }
}

