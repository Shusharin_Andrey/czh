package com.shusharin.czh.SelectionGames;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.czh.R;
import com.shusharin.czh.Interface.Help;
import com.shusharin.czh.MainActivity;

import java.util.Objects;

import static com.shusharin.czh.MainActivity.currentUser;

public class ChoiceContinue extends AppCompatActivity implements
        View.OnClickListener {
    protected Button online;
    protected Button local;
    protected boolean isPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_continue);
        Help.hideNavigationBar(getWindow(), Objects.requireNonNull(getSupportActionBar()));
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        TextView text = findViewById(R.id.tvTitle);
        text.setText(R.string.title_continue);
        online = findViewById(R.id.Online);
        local = findViewById(R.id.Local);
        online.setOnClickListener(this);
        local.setOnClickListener(this);
        if (currentUser == null || !currentUser.isEmailVerified() && !isOnline()) {
            online.setBackgroundResource(R.drawable.button_pressed);
        }
    }

    @Override
    public void onClick(View v) {
        if(!isPressed) {
            isPressed = true;
            MainActivity.startPlay(MainActivity.buttonSong);
            Intent intent;
            switch (v.getId()) {
                case R.id.Local:
                    intent = new Intent(this, Continue.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    break;
                case R.id.Online:
                    if (currentUser != null && currentUser.isEmailVerified() && isOnline()) {
                        intent = new Intent(this, ContinueOnline.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                    } else {
                        MainActivity.startPlay(MainActivity.buttonCloseSong);
                        String text;
                        if (isOnline()) {
                            text = getString(R.string.сonfirmation_error);
                        } else {
                            text = getString(R.string.no_internet);
                        }
                        Toast.makeText(ChoiceContinue.this, text,
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
            new Handler().postDelayed(() -> isPressed = false, 250);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            MainActivity.startPlay(MainActivity.buttonSong);
            finish();
            overridePendingTransition(0, 0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            return cm.getActiveNetworkInfo() != null;
        }
        return false;
    }
}
