package com.shusharin.czh.Neural;

import android.content.Context;

import com.shusharin.czh.Field.Table;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NeuralNetwork {
    private final Perceptron[][] perceptrons;

    public NeuralNetwork(int[] perceptronNumber, int inputCount) {
        if (perceptronNumber.length == 0) {
            throw new IllegalArgumentException("wrong number of layers");
        }
        if (inputCount <= 0) {
            throw new IllegalArgumentException("wrong number of inputs");
        }
        for (int value : perceptronNumber) {
            if (value <= 0) {
                throw new IllegalArgumentException("wrong number of perceptrons in a layer");
            }
        }
        perceptrons = new Perceptron[perceptronNumber.length][];
        for (int i = 0; i < perceptrons.length; i++) {
            perceptrons[i] = new Perceptron[perceptronNumber[i]];
            int num = (i == 0) ? inputCount : perceptrons[i - 1].length;
            for (int j = 0; j < perceptrons[i].length; j++) {
                perceptrons[i][j] = new Perceptron(num);
            }
        }
    }


    private double[][] calculateOutputs(double[] inputs) {
        final double[][] result = new double[perceptrons.length][];
        for (int i = 0; i < perceptrons.length; i++) {
            result[i] = new double[perceptrons[i].length];
            for (int j = 0; j < perceptrons[i].length; j++) {
                final double[] in = (i == 0) ?
                        inputs : result[i - 1];
                result[i][j] = perceptrons[i][j].getOutput(in);
            }
        }
        return result;
    }

    public int[] getAction(double[] inputs) {
        double[] output = calculateOutputs(inputs)[perceptrons.length - 1];
        int i_max = 0;
        double max = -1.7E+308;
        for (int i = 0; i < output.length; i++) {
            if ((Table.getK(Table.get_i_j(i)) == inputs[81] || inputs[81] == 10) && inputs[i] == 0) {
                if (output[i] >= max) {
                    i_max = i;
                    max = output[i];
                }
            }
        }
        return Table.get_i_j(i_max);
    }

    public void readFile(String fileName, Context context) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName)));
            try {
                for (Perceptron[] perceptron : perceptrons) {
                    for (Perceptron value : perceptron) {
                        for (int k = 0; k < value.getInputCount(); k++) {
                            value.setWeight(k, Double.parseDouble(bufferedReader.readLine()));
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

