package com.shusharin.czh.Neural;

import androidx.annotation.NonNull;

class Perceptron {
    private double[] weights;

    Perceptron(int inputCount) {
        weights = new double[inputCount];
        for (int i = 0; i < weights.length; i++) {
            weights[i] = Math.random();
        }
    }

    int getInputCount() {
        return weights.length;
    }

    void setWeight(int i, double p) {
        weights[i] = p;
    }

    double getOutput(@NonNull double[] input) {
        double sum = 0;
        for (int i = 0; i < input.length; i++) {
            sum += weights[i] * input[i];
        }
        return 1.0 / (1.0 + Math.exp(-sum));
    }
}


